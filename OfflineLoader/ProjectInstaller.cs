﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;


namespace OfflineLoader
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            var inst = FindEventLogInstaller(Installers);
            if (inst != null)
                inst.Log = "Evo Offline Loader";
        }

        private EventLogInstaller FindEventLogInstaller(InstallerCollection installers)
        {
            foreach (Installer inst in installers)
            {
                if (inst is EventLogInstaller)
                    return (EventLogInstaller)inst;

                if (inst.Installers != null)
                {
                    EventLogInstaller instLog = FindEventLogInstaller(inst.Installers);
                    if (instLog != null)
                        return instLog;
                };
            };

            return null;
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
            ServiceController controller = new ServiceController("SelectOfflineLoader");
            try
            {
                controller.Start();
            }
            catch (Exception ex)
            {
                String source = "SelectOfflineLoader";
                String log = "Evo Offline Loader";
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, log);
                };

                EventLog eLog = new EventLog();
                eLog.Source = source;

                eLog.WriteEntry(@"The service could not be started. Please start the service manually. Error: " + ex.Message, EventLogEntryType.Error);
            }
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        private void serviceProcessInstaller_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
