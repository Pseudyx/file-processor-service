﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OfflineLoader.Core.System;

namespace OfflineLoader.Core.Data
{
    public class Poke : BaseData
    {

        public Poke(XSystem system) : base(system) { }

        public bool Do()
        {
            if (!Check("DB Connection Check"))
                return false;

            try
            {
                db.Poke();
                return true;
            }
            catch
            {
                Close();
                return false;
            }
        }
    }
}
