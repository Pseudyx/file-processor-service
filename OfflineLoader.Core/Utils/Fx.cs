﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace OfflineLoader.Core.Utils
{
    public class Fx
    {
        public static DataSet ImportCSVFile(Stream stream, string tableName, char delimiter, bool headersInFirstRow, int colNumber)
        {
            DataSet ds = null;
            int lineCnt = 0;
            string line = null;

            UtilsExt utilsExt = new UtilsExt();
            utilsExt.delimiter = delimiter;
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            while (sr.Peek() >= 0)
            {
                line = sr.ReadLine();
                lineCnt++;

                if (colNumber > 0)
                {
                    int rowColCnt = GetNumberOfFields(line, delimiter);

                    if (rowColCnt + 1 < colNumber)
                    {
                        do
                        {
                            line += sr.ReadLine();

                            rowColCnt = GetNumberOfFields(line, delimiter);
                        }
                        while (rowColCnt + 1 < colNumber);
                    }
                }

                if (lineCnt == 1)
                {
                    ds = utilsExt.CreateDataSet(line, tableName, headersInFirstRow);

                    if (colNumber <= 0)
                        colNumber = ds.Tables[tableName].Columns.Count;

                    if (headersInFirstRow == false)
                        utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
                else
                {
                    utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
            }

            return ds;
        }
        public static DataSet ImportCSVFile(Stream stream, string tableName, char delimiter, bool headersInFirstRow)
        {
            DataSet ds = null;
            int lineCnt = 0;
            string line = null;

            UtilsExt utilsExt = new UtilsExt();
            utilsExt.delimiter = delimiter;
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            while (sr.Peek() >= 0)
            {
                line = sr.ReadLine();
                lineCnt++;

                if (lineCnt == 1)
                {
                    ds = utilsExt.CreateDataSet(line, tableName, headersInFirstRow);

                    if (headersInFirstRow == false)
                        utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
                else
                    utilsExt.AddRowToDataSet(ds, line, lineCnt);
            }

            return ds;
        }
        private static int GetNumberOfFields(string line, char delimiter)
        {
            int result = 0;

            int pos = line.IndexOf(delimiter);

            if (pos >= 0)
            {
                do
                {
                    result++;
                    pos = line.IndexOf(delimiter, pos + 1);
                }
                while (pos >= 0);
            }

            return result;
        }

        public static bool IsEmpty(object obj)
        {
            bool result = false;

            if (obj == null || obj == DBNull.Value)
                result = true;
            else if (string.IsNullOrEmpty(((string)obj).Trim()))
                result = true;

            return result;
        }

        public static void ConvertUTCToGeog(int zone, bool southernHemisphere, double easting, double northing, out double latitude, out double longitude)
        {
            // GRS 80
            double a = 6378137; // Equatorial Radius,
            double f = 1.0 / 298.25722210;
            double b = a * (1 - f); //6356752.3; // Polar Radius
            double esq = 1 - (b / a) * (b / a);
            double e = Math.Sqrt(esq);
            double e2 = e * e;
            double e0sq = e * e / (1 - e * e);

            double zcm = 3 + 6 * (zone - 1) - 180;

            double k0 = 0.9996; // scale along central meridian of zone
            double M = northing / k0; // Meridional Arc
            if (southernHemisphere) M = (northing - 10000000); // for southern hemisphere

            double mu = M / (a * (1.0 - esq * (1.0 / 4.0 + esq * (3.0 / 64.0 + 5.0 * esq / 256.0))));

            double e1 = (1.0 - Math.Sqrt(1.0 - e2)) / (1.0 + Math.Sqrt(1 - e2));
            double J1 = (3.0 * e1 / 2.0 - 27 * Math.Pow(e1, 3.0) / 32.0);
            double J2 = e1 * e1 * (21.0 / 16.0 - 55 * e1 * e1 / 32);
            double J3 = Math.Pow(e1, 3.0) * (151.0 / 96.0);
            double J4 = (1097.0 * Math.Pow(e1, 4.0) / 512.0);
            double fp = mu + J1 * Math.Sin(2.0 * mu) + J2 * Math.Sin(4.0 * mu) + J3 * Math.Sin(6.0 * mu) + J4 * Math.Sin(8.0 * mu);

            double e0 = e / Math.Sqrt(1.0 - e2);
            double C1 = e0sq * Math.Pow(Math.Cos(fp), 2.0);
            double T1 = Math.Pow(Math.Tan(fp), 2.0);
            double N1 = a / Math.Sqrt(1 - e2 * Math.Pow(Math.Sin(fp), 2.0));
            double R1 = N1 * (1.0 - e2) / (1.0 - Math.Pow(e * Math.Sin(fp), 2.0));
            double D = (easting - 500000) / (N1 * k0);

            double Q1 = N1 * Math.Tan(fp) / R1;
            double Q2 = (D * D / 2);
            double Q3 = (5.0 + 3.0 * T1 + 10.0 * C1 - 4 * C1 * C1 - 9 * e0sq) * Math.Pow(D, 4.0) / 24;
            double Q4 = (61 + 90 * T1 + 298 * C1 + 45 * T1 * T1 - 3 * C1 * C1 - 252 * e0sq) * Math.Pow(D, 6.0) / 720;

            latitude = fp - Q1 * (Q2 - Q3 + Q4);
            latitude = Math.Floor(1000000 * latitude / (Math.PI / 180.0)) / 1000000;

            double Q5 = D;
            double Q6 = (1.0 + 2.0 * T1 + C1) * Math.Pow(D, 3.0) / 6.0;
            double Q7 = (5.0 - 2.0 * C1 + 28 * T1 - 3 * C1 * C1 + 8 * e0sq + 24 * T1 * T1) * Math.Pow(D, 5) / 120;

            longitude = (Q5 - Q6 + Q7) / Math.Cos(fp);
            longitude = zcm + longitude / (Math.PI / 180.0);
        }

        public static String PadString(string value, string PadWith, int PadTo)
        {
            if (value.Length < PadTo)
            {
                string pad = string.Empty;
                for (int i = 0; i < (PadTo - value.Length); i++)
                {
                    pad = PadWith + pad;
                }
                value = pad + value;
            }

            return value;
        }
    }

    internal class UtilsExt
    {
        private static int nextPosInLine = 0;
        private string colName = null;
        internal char delimiter = ',';

        internal DataSet CreateDataSet(string line, string tableName, bool headersInFirstRow)
        {
            Type typeString = Type.GetType("System.String");
            DataSet ds = new DataSet();
            ds.Tables.Add(tableName);

            string[] fields = line.Split(new char[] { delimiter });

            for (int i1 = 0; i1 < fields.Length; i1++)
            {
                if (headersInFirstRow)
                    colName = GetColValue(fields[i1]);
                else
                    colName = "_col" + i1.ToString();

                DataColumn dc = new DataColumn(colName, typeString);
                ds.Tables[0].Columns.Add(dc);
            }

            return ds;
        }

        private string GetColValue(string val)
        {
            string result = null;

            if (val != null && val.Length > 1 && val.StartsWith("\"") && val.EndsWith("\""))
            {
                if (val.Length == 2)
                    result = "";
                else
                    result = val.Substring(1, val.Length - 2);
            }
            else
                result = val;

            return result;
        }

        internal void AddRowToDataSet(DataSet ds, string line, int lineCnt)
        {
            string value = null;
            int filedCnt = 0;
            DataRow dr = ds.Tables[0].NewRow();
            Object obj = null;
            int colCnt = ds.Tables[0].Columns.Count;

            value = GetFirstToken(line);

            while (value != null)
            {
                if (string.IsNullOrEmpty(value))
                    obj = null;
                else
                    obj = GetColValue(value);

                if (filedCnt < colCnt)
                    dr[filedCnt] = obj;
                else
                    throw new ApplicationException(string.Format("CSV file has invalid structure line no {0}", lineCnt));

                value = GetNextToken(line);
                filedCnt++;
            }

            ds.Tables[0].Rows.Add(dr);
        }

        // extract first token from a line
        private string GetFirstToken(string line)
        {
            nextPosInLine = 0;

            return GetToken(line);
        }

        // extract next token from the line or null the end of the line has bee reached
        private string GetNextToken(string line)
        {
            return GetToken(line);
        }

        // extract token from the posintion stored in nextPosInLine field
        private string GetToken(string line)
        {
            string result = null;

            if (nextPosInLine < line.Length)
            {
                bool openquota = false;
                char ch;
                bool cont = true;
                result = "";

                while (cont)
                {
                    if (nextPosInLine < line.Length)
                    {
                        ch = line[nextPosInLine++];

                        if (ch == '"')
                            openquota = !openquota;

                        if (ch != delimiter || openquota)
                            result += ch;

                        // end of token
                        if (ch == delimiter && openquota == false)
                            cont = false;
                    }
                    else
                        cont = false;
                }
            }

            return result;
        }
    }

    public static class CsvExtensions
    {
        /// <summary>
        /// Internal helper class with CSV parsing implementation.
        /// </summary>
        /// <typeparam name="TItem">Target type</typeparam>
        static class Csv<TItem>
            where TItem : class, new()
        {
            static Type targetType;
            static PropertyInfo lineNoProperty;
            static PropertyInfo[] propertyList;
            static List<PropertyInfo> mandatoryList;

            static Csv()
            {
                targetType = typeof(TItem);
                propertyList = targetType.GetProperties();
                mandatoryList = new List<PropertyInfo>();
                lineNoProperty = null;
                foreach (var pi in propertyList)
                {
                    CsvColumnAttribute ca = (CsvColumnAttribute)(from att in pi.GetCustomAttributes(false)
                                                                 where att is CsvColumnAttribute
                                                                 select att).FirstOrDefault();
                    if (ca != null)
                    {
                        if (ca.Mandatory) mandatoryList.Add(pi);
                        if (ca.LineNo && pi.PropertyType == typeof(int)) lineNoProperty = pi;
                    }
                }
            }

            public static PropertyInfo[] PrepareHeaderMap(string[] items)
            {
                Dictionary<string, PropertyInfo> map = new Dictionary<string, PropertyInfo>();
                foreach (var pi in propertyList)
                {
                    CsvColumnAttribute ca = (CsvColumnAttribute)(from att in pi.GetCustomAttributes(false)
                                                                 where att is CsvColumnAttribute
                                                                 select att).FirstOrDefault();
                    if (ca == null || ca.LineNo) continue;
                    if (string.IsNullOrEmpty(ca.Name)) map[pi.Name] = pi;
                    else map[ca.Name] = pi;
                }
                return Array.ConvertAll<string, PropertyInfo>(items, (x) =>
                {
                    PropertyInfo pi;
                    if (map.TryGetValue(x, out pi)) return pi;
                    else return null;
                });
            }

            /// <summary>
            /// Actual parsing splitted CSV single line string into target type.
            /// </summary>
            /// <param name="items">Splitted CSV single line string</param>
            /// <param name="delimiter">Delimiter character</param>
            /// <param name="quote">Quote character</param>
            public static TItem Convert(int lineNo, string[] items, PropertyInfo[] map, char delimiter = ',', char quote = '"')
            {
                if (map == null) map = propertyList;
                TItem item = new TItem();
                int maxItems = Math.Min(items.Length, map.Length);
                if (lineNoProperty != null && lineNo >= 0) lineNoProperty.SetValue(item, lineNo, null);
                for (int i = 0; i < maxItems; i++)
                {
                    var pi = map[i];
                    if (pi == null) continue;
                    if (pi.PropertyType == typeof(string)) pi.SetValue(item, items[i], null);
                    else throw new InvalidOperationException("Unsupported property type in " + targetType.Name);
                }
                foreach (var pi in mandatoryList)
                    if (pi.GetValue(item, null) == null)
                        throw new MandatoryRuleException(string.Format("The '{0}' property value is missing", pi.Name));
                return item;
            }
        }

        /// <summary>
        /// Split single line into array of string with taking in considiration quoted fields.
        /// </summary>
        /// <param name="s">Single line string</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="quote">Quote character</param>
        public static string[] CsvSplit(this string s, char delimiter = ',', char quote = '"')
        {
            List<string> items = new List<string>();
            StringBuilder sb = new StringBuilder(1024);
            int startIndex = 0, endIndex;
            while (startIndex < s.Length)
            {
                if (s[startIndex] == quote)
                {
                    endIndex = s.IndexOf(quote, startIndex + 1);
                    if (endIndex < 0) sb.Append(s.Substring(startIndex + 1));
                    else sb.Append(s.Substring(startIndex + 1, endIndex - startIndex - 1));
                    startIndex = endIndex + 1;
                    if (startIndex < s.Length && s[startIndex] != delimiter) continue;
                    else startIndex++;
                }
                else
                {
                    endIndex = s.IndexOf(delimiter, startIndex);
                    if (endIndex < 0) endIndex = s.Length;
                    sb.Append(s.Substring(startIndex, endIndex - startIndex));
                    startIndex = endIndex + 1;
                }
                items.Add(sb.ToString());
                sb = new StringBuilder();
            }
            return items.ToArray();
        }

        /// <summary>
        /// Convert single line CSV string into TItem type.
        /// </summary>
        /// <typeparam name="TItem">Target class type</typeparam>
        /// <param name="s">Single line string</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="quote">Quote character</param>
        public static TItem CsvConvert<TItem>(this string s, char delimiter = ',', char quote = '"')
            where TItem : class, new()
        {
            return Csv<TItem>.Convert(-1, s.CsvSplit(delimiter, quote), null, delimiter, quote);
        }

        /// <summary>
        /// Parse and convert multi line CSV string into enumerable set of target type instances
        /// </summary>
        /// <typeparam name="TItem">Target type</typeparam>
        /// <param name="s">Multi line CSV string</param>
        /// <param name="exceptionHandler">Exception handler: 1st param - line no; 2nd param - comment; 3rd param - exception; return - true (allow to continue), false (halt)</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="quote">Quote character</param>
        public static List<TItem> CsvParse<TItem>(this string s, bool headerRowPresented = true, Func<int, string, Exception, bool> exceptionHandler = null, char delimiter = ',', char quote = '"')
            where TItem : class, new()
        {
            string[] lines = null;

            try
            {
                lines = s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception ex)
            {
                if (exceptionHandler == null) throw;
                else
                {
                    exceptionHandler(-1, "Exception in spliting string to lines", ex);
                    return null;
                }
            }

            try
            {
                int lineNo = 0;
                PropertyInfo[] map = null;
                Converter<string, TItem> converter = (x) =>
                {
                    try { return Csv<TItem>.Convert(lineNo, x.CsvSplit(delimiter, quote), map, delimiter, quote); }
                    catch (Exception ex)
                    {
                        if (exceptionHandler == null) throw;
                        else if (exceptionHandler(lineNo, string.Format("Error in parsing string '{0}'", x), ex)) return null;
                        else throw;
                    }
                    finally { lineNo++; }
                };
                if (headerRowPresented && lines.Length > 0)
                {
                    map = Csv<TItem>.PrepareHeaderMap(lines[0].CsvSplit(delimiter, quote));
                    lineNo++;
                }

                List<TItem> result = new List<TItem>(lines.Length - lineNo);
                for (int i = 1; i < lines.Length; i++)
                {
                    var item = converter(lines[i]);
                    if (item != null) result.Add(item);
                }
                return result;
            }
            catch
            {
                if (exceptionHandler == null) throw;
                else return null;
            }
        }

        /// <summary>
        /// Parse and convert CSV stream into enumerable set of target type instances
        /// </summary>
        /// <typeparam name="TItem">Target type</typeparam>
        /// <param name="s">CSV stream</param>
        /// <param name="exceptionHandler">Exception handler: 1st param - line no; 2nd param - comment; 3rd param - exception; return - true (allow to continue), false (halt)</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="quote">Quote character</param>
        public static List<TItem> CsvParse<TItem>(this Stream s, bool headerRowPresented = true, Func<int, string, Exception, bool> exceptionHandler = null, char delimiter = ',', char quote = '"')
            where TItem : class, new()
        {
            List<TItem> result = new List<TItem>();
            using (var sr = new StreamReader(s, Encoding.Default))
            {
                PropertyInfo[] map = null;
                int lineNo = 0;
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    try
                    {
                        if (headerRowPresented)
                        {
                            map = Csv<TItem>.PrepareHeaderMap(line.CsvSplit(delimiter, quote));
                            headerRowPresented = false;
                        }
                        else
                        {
                            TItem item = Csv<TItem>.Convert(lineNo, line.CsvSplit(delimiter, quote), map, delimiter, quote);
                            if (item != null) result.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (exceptionHandler == null) throw;
                        else if (exceptionHandler(lineNo, string.Format("Error in parsing string '{0}'", line), ex)) return null;
                        else throw;
                    }
                    lineNo++;
                }
                return result;
            }
        }
    }

    public class CsvColumnAttribute : Attribute
    {
        public bool LineNo { get; set; }
        public string Name { get; set; }
        public bool Mandatory { get; set; }

        public CsvColumnAttribute() { Name = null; Mandatory = false; LineNo = false; }
        public CsvColumnAttribute(string name) { Name = name; Mandatory = false; LineNo = false; }
        public CsvColumnAttribute(string name, bool mandatory) { Name = name; Mandatory = mandatory; LineNo = false; }
    }

    public class MandatoryRuleException : Exception
    {
        public MandatoryRuleException() { }

        public MandatoryRuleException(string message)
            : base(message) { }
    }
}
