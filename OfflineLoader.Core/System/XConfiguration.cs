﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using OfflineLoader.Core.Utils;

namespace OfflineLoader.Core.System
{
    internal class XConfiguration
    {
        public static string ConnectionString { get; private set; }
        public static string MessageConnection { get; private set; }

        static XConfiguration()
        {
            try
            {
                var cs = ConfigurationManager.ConnectionStrings["DaveEntities"];
                if (cs != null) ConnectionString = cs.ConnectionString;

                var mq = ConfigurationManager.ConnectionStrings["MessageStoreEntities"];
                if (mq != null) MessageConnection = mq.ConnectionString;
            }
            catch (Exception ex) { EventLogger.Post(ex, LogErrorID.Configuration, "Configuration error"); }
        }
    }

    public class LogErrorID
    {
        public const int Unexpected = 1;
        public const int Configuration = 10;
        public const int DataRetrieval = 20;
        public const int DataUpdate = 21;
        public const int DatabaseConnectivity = 22;
        public const int Smtp = 30;
        public const int Authentication = 40;
        public const int InvalidClientData = 41;
        public const int TriedTooManyTimes = 42;
        public const int Processor = 50;
        public const int Scheduler = 52;
        public const int Validation = 53;
    }
}
