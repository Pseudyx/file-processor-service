﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OfflineLoader.Core.Utils;

namespace OfflineLoader.Core.System
{
    public class XSystem : Disposable
    {
        public EventLogger Logger { get; private set; }
        public string ProcessName { get; set; }

        public XSystem()
        {
            Logger = new EventLogger();
            ProcessName = "undefined";
        }

        protected override void DisposeManaged()
        {

        }
    }
}
