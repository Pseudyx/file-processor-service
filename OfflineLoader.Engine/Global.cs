﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace OfflineLoader.Engine
{
    public static class Global
    {
        #region enums

        /// <summary>
        /// DB Names
        /// Used for the physical location of data retreaval.
        /// </summary>
        public enum DataLocation
        {
            ERROR,
            Dave,
            Chantal
        }


        public enum AIGroupType
        {
            FZ,
            CG,
            SR
        }

        public enum AIInspectType
        {
            Aerial,
            Ground
        }

        public enum AIViewLevel
        {
            All,
            Inspect,
            Assess,
            Audit
        }

        public enum WorkflowStage
        {
            Schedule,
            Assigned,
            Complete
        }

        public enum BankGuaranteeStatus
        {
            Awaiting = 76,
            Received = 77,
            NotRequired = 78,
            NA = 79,
            GiventoCustomer = 107
        }

        public enum ContractStatus
        {
            Live = 80,
            TradingwithoutContract = 81,
            Expired = 82,
            Superseded = 83,
            CurrentlyNegotiating = 84,
            Executed = 85,
            Terminated = 86

            //Live = 80,
            //Suspended = 81,
            //ExpiredArchived = 82,
            //ExpiredDestroyed = 83,
            //CurrentlyNegotiating = 84,
            //Executed = 85,
            //Terminated = 86,
            //NA = 87
        }

        public enum ContractReviewStatus
        {
            New = 88,
            WorkInProgress = 89,
            Complete = 90
        }

        public enum ContractExpenditureType
        {
            Expense = 1,
            Revenue = 2,
            Other = 3
        }

        public enum ContractType
        {
            StandardContract = 1,
            ASLContract = 2
        }

        public enum ContractAttributeType
        {
            //Common attributes count(12)
            BusinessAreaID = 455,
            BusinessManagerID = 456,
            BusinessOrganisationID = 457,
            Comments = 458,
            CustomerID = 459,
            ExecutionDate = 460,
            SPCompany = 461,
            StatusID = 462,
            Title = 463,
            AnnualValue = 464,
            TotalValue = 514,
            NextCPIReviewDate = 515,
            //Standard type attributes count(12)
            ArchiveBarcode = 465,
            BankGuaranteeID = 466,
            ConfidentialGroups = 467,
            ContractOrderTypeID = 468,
            Description = 469,
            GuaranteedValue = 470,
            Location = 471,
            Template = 472,
            OracleNumber = 473,
            OrderNumber = 474,
            PriceReview = 475,
            ContractServiceTypeID = 476,
            //ASL type attributes count(7)
            ContractAccountNumberID = 477,
            ContractInstallationDetailIDs = 478,
            ContractStageID = 479,
            RenewalPeriod = 480,
            RequestedDate = 481,
            RevisedExpiryDate = 482,
            TermOfContract = 483
        }

        /// <summary>
        /// NOTE these values NEED to be the same as the values for enum AssignAttachmentAction in regards to the direction.
        /// </summary>
        public enum OffsetDirectionFromAsset
        {
            North = 3,
            NorthEast = 4,
            East = 5,
            SouthEast = 6,
            South = 7,
            SouthWest = 8,
            West = 9,
            NorthWest = 10,
            Unidentified = 11
        }

        public enum AssignAttachmentAction
        {
            PreviousTask = 0,
            CurrentTask = 1,
            NextTask = 2,
            NorthOfAsset = 3,
            NorthEastOfAsset = 4,
            EastOfAsset = 5,
            SouthEastOfAsset = 6,
            SouthOfAsset = 7,
            SouthWestOfAsset = 8,
            WestOfAsset = 9,
            NorthWestOfAsset = 10,
            Unidentified = 11
        }

        public enum FileType
        {
            BillingCalendar = 1,
            Routes = 2,
            SPAusNetAssetsPoles = 3,
            WMImage = 4,
            GenericAssets = 5,
            MRP = 6,
            MeterNumber = 7,
            CybleNumber = 8
        }

        public enum Portal
        {
            All = 0,
            WaterMeter = 1,
            Calibration = 2,
            ConditionMonitoring = 3,
            OilTesting = 4,
            AssetInspection = 5,
            CTTesting = 6,
            Electricity = 7,
            Contract = 8,
            RPP = 9,
            MRP = 10,
            TimeSheet = 11
        }

        public enum IntervalType
        {
            NoCycle = 0,
            Hour = 1,
            Day = 2,
            Week = 3,
            Month = 4,
            Year = 5
        }

        /// <summary>
        /// AddNew = adding a brand new item, all fields are available for edit and pre-empty
        /// AddMany = adding a few items at a time
        /// Edit = edit existing item
        /// View = view data
        /// </summary>
        public enum DetailPageMode
        {
            AddNew = 0,
            AddMany = 1,
            Edit = 2,
            View = 3
        }

        public enum ResequenceResponse
        {
            Error = -1,
            LogicError = 0,
            Success = 1
        }

        public enum AttachmentAttributeTypes
        {
            AttachmentImageGrade = 392,
            AttachmentToAssetOffsetDirectionAndUniqueAssetID = 396
        }

        public enum ServiceRequestAttributeTypes
        {
            JobsSuccessfulCount = 437,
            JobsNotSuccesfulCount = 438,
            JobsReadHighCount = 439,
            JobsReadLowCount = 440,
            TaskFirstReadDate = 442,
            TaskLastReadDate = 443,
            Duration = 444,
            Assigned = 446,
            ReaderCode = 447
        }

        public enum AttachmentTypes
        {
            ConditionMonitoring = 4,
            SitePhoto = 5,
            SitePhotoThumbnail = 10,
            DefectPhoto = 6,
            RawPhoto = 7,
            ClosestRawPhoto = 8,
            WaterMeterAdminExport = 9,
            UploadDocument = 11,
            VADocument = 12,
            CalibrationReport = 13,
            ContractReport = 14,
            DefectReport = 15,
            ContractTemplate = 17
        }

        public enum AttachmentStatus
        {
            PendingPhoto = 15,
            Normal = 16,
            Deleted = 17,
            Locked = 18,
            Active = 73,
            Inactive = 74
        }

        public enum ActivityType
        {
            FullScreenView = 1
        }

        public enum AssetDefinitionTypes
        {
            Pole = 2,
            WaterMeter = 10,
            TransformerHV = 11,
            CurrentTransformer = 212,
            VoltageTransformer = 213,
            CurrentVoltageTransformer = 214,
            NMI = 340,
            ElecMeter = 341
        }

        public enum AssetStatus
        {
            Active = 36,
            Inactive = 37,
            InCalibration = 60,
            OutOfCalibration = 62,
            NoCalibration = 63,
            Available = 110,
            Installed = 111
        }

        public enum CalibrationStatus
        {
            InCalibration = 1,
            OutOfCalibration = 2,
            NoCalibration = 3
        }

        public enum CurrentTransformerAssetAttributeType
        {
            CTRatio = 416,
            KFactor = 417,
            Phase = 418
        }

        public enum TransformerHVAssetAttributeType
        {
            YearOfManufacture = 182,
            Voltage = 184,
            Rating = 185,
            Phases = 186,
            VectorGroup = 187,
            SpecificationNo = 188
        }

        public enum CTTestingJobAttributeType
        {
            AgreedTestDate = 351,
            NMI = 395,
            BusinessName = 396,
            SiteName = 397,
            StreetNumberPrefix = 398,
            StreetNumber = 399,
            StreetType = 400,
            Suburb = 401,
            SiteContact = 402,
            Retailer = 403,
            FRMP = 404,
            RP = 405,
            LNSP = 406,
            MDP = 407,
            MPB = 408,
            MPC = 409,
            MeterNo = 410,
            MeterSerialNo = 411,
            LoadType = 412,
            MeterType = 413,
            MeterInstallDate = 414,
            VTRatio = 415,
            StreetName = 421,
            PostCode = 490,
            State = 491,
            CTJobState = 516


        }

        public enum CalibrationJobAttributeType
        {
            ScheduleID = 345,
            AssetLocationID = 348,
            TestProgressID = 349,
            PrefferedNextTest = 350,
            AgreedTestDate = 351,
            ActualTestedDate = 352,
            NextTestScheduledOn = 353,
            PurchaseOrderNo = 354,
            TestLocationID = 360
        }

        public enum CalibrationAssetAttributeType
        {
            AssetUsageID = 377,
            CalibrationStatusID = 378,
            TechSolutionsAssetNo = 379,
            ScheduleID = 380,
            LastTestedDate = 381,
            NextTestedDate = 382,
            CustodianID = 383,
            CalibartionRequirementID = 384,
            TeamLeaderID = 385,
            CalibrationRequirements = 386,
            Remarks = 387,
            AssetSiteID = 355,
            AssetLocationID = 544
        }

        public enum ConMonAssetAttributeType
        {
            Remarks = 393
        }

        public enum ElectricityNMIAssetAttributeType
        {
            Slab = 697
        }

        public enum ElectricityMeterAssetAttributeType
        {
            BusinessName = 648,
            BusinessType = 649,
            ConsumerName = 145,
            PhoneHome = 576,
            PhoneMobile = 577,
            AddressID = 570,
            ReadCycle = 645,
            Sequence = 144,
            MeterRegisters = 647,
            KeyDetails = 159,
            LocationDescription = 567,
            LifeSupport = 650,
            Duplicate = 651,
            SiteType = 653,
            MeterType = 654,
            USAReliability = 655,
            PhaseNumber = 696
        }

        public enum VAJobAttributeType
        {
            VASSystem = 422,
            FileCreationDate = 423,
            TransStatus = 424,
            MPB = 427,
            VAComments = 428
        }



        public enum ContactTypes
        {
            Custodian = 1,
            TeamLeader = 2,
            Administrator = 3,
            Assessor = 4,
            Guest = 5,
            Inspector = 6,
            Schedular = 7,
            Supervisor = 8,
            Tester = 9,
            WaterReader = 10
        }

        /// <summary>
        /// Indicates whether child entities of a parent are required to be populated
        /// when a data call is made
        /// </summary>
        public enum IncludeChildEntities
        {
            False = 0,
            True = 1
        }

        /// <summary>
        /// Indicates where the return list should include items marked as IsPrimary
        /// </summary>
        public enum IncludePrimary
        {
            IncludeAll = 0,
            IncludePrimaryOnly = 1,
            ExcludePrimary = 2
        }

        public enum MappingTable
        {
            Portal = 0,
            Organisation = 1,
            Contact = 2,
            Job = 3,
            Attachment = 4,
            Address = 5,
            ParentOrganisation = 6,
            Customer = 7,
            InternalOrganisation = 8,
            ServiceRequest = 9,
            Asset = 10,
            Defect = 11,
            Category = 12,
            Provider = 13,
            ManagingAgent = 14,
            AssetSchedule = 15,
            Task = 16,
            Status = 17,
            TaskResolution = 18,
            JobResolution = 19,
            Contract = 20,
            ContractReview = 21,
            ContractNotification = 22
        }

        public enum ModuleType
        {
            All = 0,
            ServiceRequest = 1,
            Oganisation = 2,
            Asset = 3,
            AssetType = 4,
            Attachment = 7,
            Job = 8,
            Task = 9,
            DefectType = 10,
            DefectCategory = 11,
            DefectPriority = 12,
            DefectSubtype = 13,
            Defect = 14,
            MobileReplication = 15,
            Manufacturer = 16,
            Make = 17,
            Model = 18,
            Address = 19,
            RouteAdmin = 20,
            ClassificationType = 21,
            Contract = 22,
            BankGuarantee = 23,
            ContractReview = 24,
            FailureCode = 28,
            WorkSpecifications = 40,
            Cause = 36,
            CauseDescription = 37

        }

        public enum JobType
        {
            Calibration = 1,
            ConditionMonitoring = 2,
            AssetInspections = 4,
            MeterRead = 6,
            StationTransformer = 7,
            Testing = 8,
            Validation = 9,
            Billing = 10
        }

        public enum RouteAdminStatus
        {
            New = 70,
            PendingExport = 71,
            Complete = 72
        }

        /// <summary>
        /// Used for Enumeration of Route sequences
        /// </summary>
        public enum RouteDirection
        {
            Normal = 0,
            Reversed = 1
        }

        public enum JobDefinition
        {
            DefaultType1 = 1,
            DefaultType2 = 2,
            GroundAssetInspection = 4,
            AerialAssetInspection = 5,
            WaterMeterRead = 6,
            ConditionMonitoring = 7,
            StationTransformer = 8,
            NATACalibration = 9,
            NonNATACalibration = 10,
            CheckbeforeUse = 11,
            IndicationOnly = 12,
            RegisterOnly = 13,
            Failed = 14,
            Disposal = 15,
            HeldAsSpareQuarantineArea = 16,
            NA = 17,
            CTTesting = 18,
            Validation = 19,
            PreliminaryAssessment = 21,
            Maintenance = 22,
            AssetInpection = 23,
            //Technology
            ChangeRequest = 24,
            Enhancement = 25,
            Incident = 26,
            //Reporting
            SSRChangeRequest = 27,
            SSREnhancement = 28,
            SSRIncident = 29,
            ProductDevelopment = 30,
            BusinessDevelopment = 31,
            MeterReplacement = 94
        }

        public enum TaskDefinition
        {
            GroundAssetInspection = 1,
            AerialAssetInspection = 2,
            Assessment = 4,
            Scheduling = 5,
            MeterRead = 6,
            Audit = 7,
            Water = 30,
            Calibration = 31,
            ScheduleInspection = 33,
            ScheduleAssessment = 34,
            ScheduleAudit = 35,
            CTTest = 36,
            InvestigateFailure = 37,
            FieldPreliminaryTest = 38,
            ScheduleCTTest = 39,
            Maintenance = 40,
            SchedulingAIAerial = 50,
            SchedulingAIGround = 51,
            ReviewTask = 53,
            TruckVisit = 56,
            ScheduleMeterReplacement = 100,
            MeterReplacement = 101,
            ReScheduleMeterReplacement = 102,
            MRPApproval = 103
        }

        public enum JobResolution
        {
            Unresolved = 0,
            Successful = 28,
            NoAccess = 3,
            ReadyforTesting = 33,
            CannotTest = 34,
            Scheduled = 35,
            NewAsset = 32,
            MeterExchanged = 41
        }

        public enum DefectStatus
        {
            Active = 58,
            Inactive = 59
        }

        public enum JobStatus
        {
            New = 19,
            WorkInProgress = 20,
            PendingCustomer = 21,
            PendingThirdParty = 22,
            Complete = 23,
            AwaitingPhotos = 24,
            PendingAssessment = 25,
            Assigned = 49,
            ReportDrafted = 66
        }

        public enum MobilityStatus
        {
            Nothing = 0,
            DoNothing = 51,
            New = 52,
            Update = 53,
            Complete = 54,
            ReplicateNew = 55,
            ReplicateUpdate = 56,
            ReplicateComplete = 57,
            Processed = 91

        }

        public enum Organisations
        {
            GippslandWater = 3,
            Ergon = 124,
            SPINetwork = 275,
            YarraValleyWater = 345,
            CityWestWater = 390
        }

        public enum OrganisationType
        {
            Customer = 1,
            Provider = 2,
            Internal = 3,
            ManagingAgent = 4
        }

        public enum ServiceRequestType
        {
            AerialAssetInspection = 1,
            MeterRead = 2,
            GroundAssetInspection = 3,
            ConditionMonitoring = 4,
            Calibration = 5,
            OilTesting = 6,
            CTTesting = 8,
            Validation = 9,
            AssetInspection = 10,
            SelectSupport = 11,
            RRP = 13,
            MRP = 15,
            eMRP = 17

        }

        public enum ServiceRequestStatus
        {
            All = 0,
            New = 28,
            WorkInProgress = 29,
            Complete = 30,
            PendingCustomer = 31,
            PendingThirdParty = 32,
            Rejected = 33,
            ReturnIncompletePackage = 34,
            Assigned = 35,
            Pending = 48
        }

        public enum TaskResolutionValidation
        {
            OrgCode,
            Description,
            OrgPriority

        }

        public enum TaskResolution
        {
            NotResolved = 0,
            CompleteNoDefects = 1,
            NoAccess = 2,
            CannotLocateAsset = 3,
            CompleteIncorrectDetailsProvided = 4,
            CompleteWithDefects = 6,
            FreeformText = 8,
            RefusedEntry = 9,
            AfterHoursEntry = 10,
            DogInYard = 11,
            NotInRouteSequence = 12,
            MissingMeter = 13,
            MeterOnSide = 14,
            MeterNosDifferent = 15,
            CantLocateMtr = 16,
            HouseFire = 17,
            CantLocateProperty = 18,
            DangerousWildlife = 19,
            TooWet = 20,
            Locked = 21,
            LockedAndVacant = 22,
            KeyGaticRequired = 23,
            MeterBuried = 24,
            ObtructedUnderBush = 25,
            UnableToRead = 26,
            NotRegistering = 27,
            ReadSuccessful = 28,
            ReadSuccessfulHigh = 29,
            ReadSuccessfulLow = 30,
            Rejected = 34,
            Cancelled = 35,
            ReadCardLeftWithResident = 36,
            Scheduled = 37,
            ReadSuccessfulNegative = 38,
            ReAssigned = 39,
            Closed = 40,
            NewAsset = 41,
            ReadyforTesting = 42,
            CannotTest = 43,
            NewBillingCreated = 44,
            BillingExists = 45,
            NoAction = 46,
            TestComplete = 47,
            TestingComplete = 48,
            ErgonTaskCompleteWithNoDefects = 61,
            ErgonTaskCompleteWithDefects = 63,
            ErgonTaskNoAccess = 64,
            MailSent = 65,
            MeterExchanged = 93,
            NoCategory4Photo = 119,
            UnabletoViewAsset = 120

        }

        public enum TaskStatus
        {
            New = 1,
            Assigned = 2,
            WorkInProgress = 3,
            Complete = 4,
            ReAssigned = 5,
            AwaitingPhotos = 6,
            AwaitingAssessment = 7,
            NotIssued = 8,
            AwaitingData = 93,
        }

        public enum TaskType
        {
            notUsed = 1000,
            Inspection = 1001,
            AwaitingPhotos = 1002,
            Assessment = 1003,
            Scheduling = 1004,
            MeterRead = 1005,
            Audit = 1006,
            OilTestType = 1007,
            Billing = 1008,
            Calibration = 1030,
            Validation = 1031
        }

        public enum Team
        {
            NoTeamAssigned = 0,
            Calibration = 1,
            Management = 2,
            ConditionMonitoring = 3,
            AssetInspectionField = 4,
            AssetInspectionAssessment = 5,
            AssetInspectionSupervisors = 6,
            MeterReadingScheduling = 7,
            MeterReaders = 9,
            AssetInspectionScheduling = 11,
            AssetInspectionAuditor = 12,
            CTTestingField = 13,
            SelectSupportBusinessSystems = 88,
            SelectSupportReporting = 89,
            SelectSupportProductDevelopment = 90,
            SelectSupportBusinessEnhancement = 91,
            ScheuleFieldMetering = 114,
            NetworkServiceOrder = 115


        }

        public enum CondMonitorJobAttributeType
        {
            SpecialInstructions = 149,
            CustomerOrdNo = 178,
            ProjectNo = 179,
            ReportNo = 180,
            TestDate = 181
        }

        public enum WaterMeterAssetAttributeType
        {
            MeterMake = 143,
            Sequence = 144,
            ConsumerName = 145,
            MeterAddress = 146,
            MeterSize = 147,
            MeterDials = 148,
            IsKeyRequired = 158,
            KeyDetails = 159,
            OldSequence = 388,
            AssetUsageID = 377,
            LastReadDate = 562,
            LastRead = 563,
            LastReadType = 564,
            MeterStatus = 565,
            LocationCode = 566,
            LocationDescription = 567,
            InstallationCode = 569,
            AddressID = 570,
            AddressExtra = 571,
            MeterDrive = 572,
            AccountNo = 573,
            Owner = 574,
            ConsumerID = 575,
            PhoneHome = 576,
            PhoneMobile = 577,
            Melway = 578,
            SupplyCode = 580,
            District = 581,
            MailName = 582,
            MailAddress = 583,
            MailCity = 584,
            MailPostCode = 585,
            MailState = 586,
            PhoneWork = 587,
            ReadCycle = 645,
        }

        public enum WaterMeterTaskAttributeType
        {
            DateStartRead = 150,
            StartReading = 151,
            MinReadValue = 152,
            MaxReadValue = 153,
            PreviousConsumption = 154,
            ReadDatetime = 155,
            Resequence = 156,
            ReadValue = 157,
            ReadTimeStamp = 174,
            MeterLocation = 177
        }

        public enum WaterMeterJobAttributeType
        {
            SpecialInstructions = 149,
            SelectMinRead = 435,
            SelectMaxRead = 436
        }

        public enum WorkOrderStatus
        {
            New = 64,
            WorkInProgress = 65,
            ReportDrafted = 66,
            PendingCustomer = 67,
            PendingThirdParty = 68,
            Completed = 69
        }

        public enum Schedule
        {
            BiAnnualCalibration = 9
        }

        public enum TestProgress
        {
            New = 1
        }

        public enum IncrementNumber
        {
            CTTestReport = 1
        }

        public enum AssetInspectionAssetAttributeType
        {
            AMFMPoleID = 164,
            EquipType = 165,
            FireArea = 166,
            SwitchZone = 167,
            Area = 168,
            PmNumber = 169,
            PmModule = 170,
            SchedYear = 371,
            MonthFullCycle = 372,
            MidCycleFY = 373,
            MonthImageCapture = 390,
            MonthTechAssess = 391,
            InspectionType = 498,
            ImportFileLogID = 499,
            RegionID = 500,
            Feeder = 503,
            PELAttached = 622,
            MapReference = 623,
            SpanLength = 624
        }


        public enum CoordinateType
        {
            UTM,
            Google
        }

        public enum AIPhotoType
        {
            SitePhoto = 1,
            RawPhoto = 2,
            GoogleMap = 3,
            DefectPhoto = 4,
            ThumbPhoto = 5,
            FlightMap = 6
        }

        public enum RRPReportInterval
        {
            Week = 0,
            Month = 1,
            Day = 2
        }

        public enum ProductCategory
        {
            NoCategory = 0,
            AdditionalItem = 1,
            Equipment = 2,
            Material = 3,
            Other = 4
        }

        public enum ProductUnitType
        {
            NoUnit = 0,
            mm = 1,
            cm = 2,
            m = 3,
            unit = 4,
            bag = 5,
            g = 6,
            kg = 7,
            ton = 8,
            New = 9
        }

        public enum ProductVarianceType
        {
            NoVariance = 0,
            Size = 1,
            Colour = 2,
            MagWheels = 3
        }

        public enum MRPTaskAttributeType
        {
            AgreedDate = 588,
            AgreedTime = 589,
            SubTeamID = 590,
            isLiveService = 591,
            isVegetation = 592,
            isConfinedSpace = 593,
            isSyringe = 594,
            isManualHandling = 595,
            isAnimalHazard = 596,
            isRemoteLocation = 597,
            isAsbestos = 598,
            isTrafficManagement = 599,
            otherReason = 600,
            isControlRisk = 601,
            isAssessmentCompleted = 602,
            correctMeterNum = 603,
            subbedRead = 604,
            newMeterNum = 605,
            newMeterRead = 606,
            cybleNum = 607,
            isSpinderReplaced = 608,
            Latitude = 609,
            Longitude = 610,
            dateCompleted = 611,
            BillingPartyID = 612,
            isRoundFlange = 613,
            newMeterSize = 614
        }

        public enum TimeSheetType
        {
            [Description("Hazard Trees")]
            HazardTrees = 10,
            [Description("56M Trees")]
            Trees56M = 11,
            [Description("In Windows")]
            InWindows = 12,
            [Description("Live Line Works")]
            LiveLineWorks = 13,
            [Description("Trouble Orders")]
            TroubleOrders = 14,
            [Description("SP Other Work / External Work")]
            SPOtherWorkOrExternalWork = 15
        }

        #endregion enums
    }

}
