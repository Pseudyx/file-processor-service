﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using OfflineLoader.Core.Utils;
using OfflineLoader.Core.System;
using OfflineLoader.Core.Data;
using System.Data.Objects;

namespace OfflineLoader.Engine.Processor
{
    public class FileProcessor : BaseData
    {
        XSystem system;
        ImportFileLog importFileLog;
        Stream fileStream;

        private int importFileLogId = 0;
        
        private string importFileStatus = "";
        //private Guid requestId;
        private int fileTypeId = 0;
        private int organisationId = 0;
        private int serviceRequestTypeId = 0;
        private int userID = 0;
        private string fileName = null;
        private string userString = null;

        private Dictionary<string, int> assetAttributeTypes = null;
        private Dictionary<string, int> jobAttributeTypes = null;
        private Dictionary<string, int> taskAttributeTypes = null;
        private int rowCount = 0;
        private int errCount = 0;
        
        public int ImportFileLogId { get { return importFileLogId; } }
        //public Guid RequestId { get { return requestId; } }
        public int FileTypeId { get { return fileTypeId; } }
        public int OrganisationId { get { return organisationId; } }
        public int ServiceRequestTypeId { get { return serviceRequestTypeId; } }
        public int UserID { get { return userID; } }
        public string FileName { get { return fileName; } }
        public string UserString { get { return userString; } }

        public FileProcessor(XSystem System): base(System)
        {
            system = System;
            system.ProcessName = "FileProcessor";

        }

        protected override void DisposeManaged()
        {
            system.Dispose();
        }

        public void Execute(Object obj)
        {
            //system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} executed", system.ProcessName));

            Check("Process File");
            BaseMessage messageQueue = new BaseMessage(system);

            FileParameters prms = (FileParameters)obj;

            string inputFolder = prms.InputFolder;
            string outputFolder = prms.OutputFolder;
            string srcFileName = prms.SrcFileName;

            try
            {
                importFileLog = GetImportFileLog(srcFileName);
                importFileLogId = importFileLog.ImportFileLogId;
                    
                fileStream = File.OpenRead(Path.Combine(inputFolder, srcFileName));

                //system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} Start Process File: {1}", system.ProcessName, srcFileName));

                switch (importFileLog.ServiceRequestTypeId)
                {
                    case (int)Global.ServiceRequestType.eMRP:

                        ImporteMRP(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ImportedBy, importFileLog.UserString, messageQueue);

                        break;

                    case (int)Global.ServiceRequestType.MRP:

                        switch ((int)importFileLog.FileTypeId)
                        {
                            case (int)Global.FileType.MRP:
                                ProcessMRPFile(fileStream, (int)importFileLog.OrganisationId, (int)importFileLog.ImportedBy, importFileLog.UserString, messageQueue);
                                break;
                            case (int)Global.FileType.MeterNumber:
                                ImportMRPAssets(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ImportedBy, messageQueue);
                                break;
                            case (int)Global.FileType.CybleNumber:
                                UpdateMRPAssets(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ImportedBy, messageQueue);
                                break;
                        }

                        break;

                    case (int)Global.ServiceRequestType.AssetInspection:

                        switch ((int)importFileLog.FileTypeId)
                        {
                            case (int)Global.FileType.SPAusNetAssetsPoles:

                                switch (importFileLog.OrganisationId)
                                {
                                    case (int)Global.Organisations.SPINetwork:
                                        ImportAISPAusnet(fileStream, fileName, (int)importFileLog.ImportedBy, importFileLog.UserString, messageQueue);
                                        break;
                                    case (int)Global.Organisations.Ergon:
                                        ImportAISPAusnet(fileStream, fileName, (int)importFileLog.ImportedBy, importFileLog.UserString, messageQueue);
                                        break;
                                }

                                break;
                            case (int)Global.FileType.GenericAssets:
                                
                                ImportGenericAssets(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ServiceRequestTypeId, (int)importFileLog.ImportedBy, importFileLog.UserString, Global.CoordinateType.Google);
                                
                                break;

                        }

                        break;

                    case (int)Global.ServiceRequestType.MeterRead:

                        switch ((int)importFileLog.FileTypeId)
                        {
                            case (int)Global.FileType.BillingCalendar:
                                ImportBillingCalendar(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ServiceRequestTypeId, (int)importFileLog.ImportedBy, messageQueue);
                                break;
                            case (int)Global.FileType.Routes:
                                ImportWaterMeters(fileStream, fileName, (int)importFileLog.OrganisationId, (int)importFileLog.ServiceRequestTypeId, (int)importFileLog.ImportedBy, messageQueue);
                                break;
                        }

                        break;
                }

                fileStream.Close();
                fileStream.Dispose();

                //system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} End Process File: {1}", system.ProcessName, srcFileName));
                
            }
            catch (Exception ex)
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                    fileStream.Dispose();
                }

                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);
                //return;
            }

            MoveFileToArchive(inputFolder, outputFolder, srcFileName);

            Close();
        }

        #region eMRP File Processing

        private void ImporteMRP(Stream uploadFile, string fileName, int organisationID, int userId, string packageName, BaseMessage messageQueue)
        {
            #region Properties

            DataSet ds = null;
            DataTable CommercialDT = null;
            DataTable ResidentialDT = null;

            XMessage message = null; 

            int cnt = 0;
            int errCnt = 0;
            string errMsg = null;
            int importCount = 0;
            int existCount = 0;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            string MeterNumber = String.Empty;
            string AccountNo = String.Empty;
            string ConsumerName = String.Empty;

            List<KeyValuePair<Asset, Address>> addressList = new List<KeyValuePair<Asset, Address>>();
            List<KeyValuePair<string, Asset>> NMIAssetList = new List<KeyValuePair<string, Asset>>();
            List<KeyValuePair<string, Installation>> NMIInstallationList = new List<KeyValuePair<string, Installation>>();
            List<string> NMIList = new List<string>();

            List<Suburb> SuburbList = db.Suburbs.ToList();

            #endregion

            #region Populate AssetAttributeTypes Dictionary

            assetAttributeTypes = new Dictionary<string, int>();
            foreach (Global.ElectricityMeterAssetAttributeType AttrType in Enum.GetValues(typeof(Global.ElectricityMeterAssetAttributeType)))
            {
                assetAttributeTypes.Add(AttrType.ToString(), (int)AttrType);
            }

            foreach (Global.ElectricityNMIAssetAttributeType AttrType in Enum.GetValues(typeof(Global.ElectricityNMIAssetAttributeType)))
            {
                assetAttributeTypes.Add(AttrType.ToString(), (int)AttrType);
            }

            #endregion

            if (!Check("Get eMRP process")) return;

            try
            {

                #region Convert CSV to DataSet

                ds = Fx.ImportCSVFile(uploadFile, "eMRP", ',', true);

                #endregion

                if (ds != null)
                {
                    if (ValidateeMRPFile(ds, out cnt, out errCnt, ref SuburbList))
                    {
                        if (ds.Tables["eMRP"].Rows.Count > 0)
                        {
                            DataRow[] comDR = ds.Tables["eMRP"].Select("[CommercCal or ResCdentCal] = 'C'");
                            CommercialDT = (comDR.Length > 0) ? comDR.OrderBy(x => x["NMI"]).ThenBy(x => x["MTR_NBR"]).ThenBy(x => x["REGISTER_CODE"]).CopyToDataTable() : null;
                            DataRow[] resDR = ds.Tables["eMRP"].Select("[CommercCal or ResCdentCal] = 'R'");
                            ResidentialDT = (resDR.Length > 0) ? resDR.OrderBy(x => x["NMI"]).ThenBy(x => x["MTR_NBR"]).ThenBy(x => x["REGISTER_CODE"]).CopyToDataTable() : null;

                            //Process Valid data
                            cnt = 0;

                            if (CommercialDT != null && errCnt == 0) eMRPProcess("Commercial", CommercialDT, ref NMIList, ref NMIAssetList, ref NMIInstallationList, ref SuburbList, ref addressList, ref cnt, ref errMsg, ref errCnt, ref importCount, organisationID, userId, packageName);
                            if (ResidentialDT != null && errCnt == 0) eMRPProcess("Residential", ResidentialDT, ref NMIList, ref NMIAssetList, ref NMIInstallationList, ref SuburbList, ref addressList, ref cnt, ref errMsg, ref errCnt, ref importCount, organisationID, userId, packageName);

                            if (errCnt == 0)
                            {
                                try
                                {
                                    Save("Save eMRP SR Data");

                                    //Create AddressID attribute for all assets with new addesses
                                    foreach (KeyValuePair<Asset, Address> Pair in addressList)
                                    {
                                        //AddressID
                                        AssetAttribute AddressIDAttr = null;
                                        AddressIDAttr = CreateAssetAttribute(Pair.Key, Global.WaterMeterAssetAttributeType.AddressID.ToString(), Pair.Value.AddressID.ToString(), userId);
                                    }

                                    importFileStatus = "Imported: " + importCount.ToString() + " NMIs";

                                    importFileLog.RecordCnt = cnt;
                                    importFileLog.ErrorCnt = errCnt;
                                    rowCount = cnt;
                                    errCount = errCnt;

                                    importFileLog.Status = importFileStatus;
                                    importFileLog.Complete = true;

                                    Save("Save eMRP Address Data");

                                    

                                    // try to save log
                                    //try
                                    //{
                                    //    importFileLog.RecordCnt = cnt;
                                    //    importFileLog.ErrorCnt = errCnt;
                                    //    rowCount = cnt;
                                    //    errCount = errCnt;

                                    //    importFileLog.Status = importFileStatus;
                                    //    importFileLog.Complete = true;

                                    //    Save("Save eMRP Log Data");

                                    //}
                                    //catch
                                    //{
                                    //    Check("Context");

                                    //    importFileLog = db.ImportFileLogs.Where(x => x.ImportFileLogId == importFileLogId).FirstOrDefault();

                                    //    importFileLog.RecordCnt = cnt;
                                    //    importFileLog.ErrorCnt = errCnt;
                                    //    rowCount = cnt;
                                    //    errCount = errCnt;

                                    //    importFileLog.Status = "Imported";
                                    //    importFileLog.Complete = true;

                                    //    Save("Save eMRP Catch Log Data");
                                    //}

                                    message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                                    messageQueue.SendMessage(message);
                                    
                                }
                                catch (Exception ex)
                                {
                                    IEnumerable<ObjectStateEntry> objs = db.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
                                    foreach (ObjectStateEntry obj in objs)
                                    {
                                        db.Detach(obj);
                                    }

                                    importFileStatus = "Error in import";
                                    errMsg = ex.Message;
                                    throw new ApplicationException(string.Format("Error Importing: {0}", errMsg));
                                }
                            }
                            else
                            {
                                try
                                {
                                    IEnumerable<ObjectStateEntry> objs = db.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
                                    foreach (ObjectStateEntry obj in objs)
                                    {
                                        db.Detach(obj);
                                    }
                                }
                                catch { }

                                importFileStatus = "Error in import";
                                throw new ApplicationException(string.Format("Error Importing: {0}", errMsg));

                            }


                        }
                        else
                        {
                            importFileStatus = "File Error. No Rows";

                            // try to save 
                            importFileLog.RecordCnt = cnt;
                            importFileLog.ErrorCnt = errCnt;
                            rowCount = cnt;
                            errCount = errCnt;

                            importFileLog.Status = importFileStatus;
                            importFileLog.Complete = true;
                            
                            Save("Save eMRP Import Error");

                            String ExcMessage = string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName);
                            throw new ApplicationException(ExcMessage);
                        }


                    }
                    else
                    {
                        importFileStatus = "Validation errors. No record imported";

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save eMRP Validation Error");

                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);
                    }
                }
                else
                {
                    importFileStatus = "No record imported. Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName));
                }
            }
            catch (Exception ex)
            {
                bool re_throw = false;
                importFileStatus = ex.Message;

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Disposal");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateeMRPFile(DataSet vDS, out int cnt, out int errCnt, ref List<Suburb> SuburbList)
        {
            DataRow dr = null;
            bool cont = true;

            cnt = 0;
            errCnt = 0;
            string errMsg = null;
            string[] ComOrRes = new string[] { "C", "R" };
            string[] LifeSup = new string[] { "Y", "N" };

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");



            bool ReturnValue = true;

            try
            {
                DataTable FileTable = vDS.Tables["eMRP"];

                for (int iRow = 0; iRow < FileTable.Rows.Count; iRow++)
                {
                    cnt++;
                    cont = true;
                    errMsg = null;
                    dr = FileTable.Rows[iRow];

                    #region column value Validation

                    if (Fx.IsEmpty(dr["NMI"])) { cont = false; errMsg = String.Format("Error in line {0} NMI can not be blank", iRow + 1); }
                    if (cont) { if (Fx.IsEmpty(dr["MTR_PREFIX"])) { cont = false; errMsg = String.Format("Error in line {0} col MTR_PREFIX can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["MTR_NBR"])) { cont = false; errMsg = String.Format("Error in line {0} col MTR_NBR can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["MR_CYCLE"])) { cont = false; errMsg = String.Format("Error in line {0} col MR_CYCLE can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["MR_ROUTE"])) { cont = false; errMsg = String.Format("Error in line {0} col MR_ROUTE can not be blank", iRow + 1); } }
                    if (cont)
                    {
                        if (Fx.IsEmpty(dr["REGISTER_CODE"]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} REGISTER_CODE can not be blank", iRow + 1);
                        }
                        else
                        {
                            dr["REGISTER_CODE"] = Fx.PadString(dr["REGISTER_CODE"].ToString(), "0", 2);
                        }

                    }

                    //if (cont) { if (IsEmpty(dr["NAME_PREFIX"])) { cont = false; errMsg = String.Format("Error in line {0} NAME_PREFIX can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["FIRST_NAME"])) { cont = false; errMsg = String.Format("Error in line {0} FIRST_NAME can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["LAST_NAME"])) { cont = false; errMsg = String.Format("Error in line {0} LAST_NAME can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["BUSINESS_NAME"])) { cont = false; errMsg = String.Format("Error in line {0} BUSINESS_NAME can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["BUSINESS_TYPE"])) { cont = false; errMsg = String.Format("Error in line {0} BUSINESS_TYPE can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["PH_PRIMARY_NBR"])) { cont = false; errMsg = String.Format("Error in line {0} PH_PRIMARY_NBR can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["MOBILE_PHONE_NBR"])) { cont = false; errMsg = String.Format("Error in line {0} MOBILE_PHONE_NBR can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["FLAT_NBR"])) { cont = false; errMsg = String.Format("Error in line {0} FLAT_NBR can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["APT_NBR"])) { cont = false; errMsg = String.Format("Error in line {0} APT_NBR can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["ST_NBR"])) { dr["ST_NBR"] = "0"; } }
                    if (cont) { if (Fx.IsEmpty(dr["ST_NAME"])) { cont = false; errMsg = String.Format("Error in line {0} ST_NAME can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["ST_SUFFIX"])) { dr["ST_SUFFIX"] = "0"; } }
                    if (cont)
                    {
                        if (Fx.IsEmpty(dr["CITY"]))
                        {
                            cont = false; errMsg = String.Format("Error in line {0} CITY can not be blank", iRow + 1);
                        }
                        else if (!SuburbList.Select(x => x.Name).Contains(dr["CITY"].ToString()))
                        {
                            cont = false; errMsg = String.Format("Error in line {0} Unknown CITY: {1}", iRow + 1, dr["CITY"].ToString());
                        }
                    }
                    //if (cont) { if (IsEmpty(dr["ZIP_CODE"])) { cont = false; errMsg = String.Format("Error in line {0} ZIP_CODE can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["USA_RELIABILITY"])) { cont = false; errMsg = String.Format("Error in line {0} USA_RELIABILITY can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["LOCATION_DETAIL"])) { cont = false; errMsg = String.Format("Error in line {0} LOCATION_DETAIL can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["SITE TYPE"])) { cont = false; errMsg = String.Format("Error in line {0} SITE TYPE can not be blank", iRow + 1); } }
                    if (cont) { if (Fx.IsEmpty(dr["NEW_METER_TYPE-1"])) { cont = false; errMsg = String.Format("Error in line {0} NEW_METER_TYPE-1 can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["KEY_ID"])) { cont = false; errMsg = String.Format("Error in line {0} KEY_ID can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["MR_INSTR_DSCR"])) { cont = false; errMsg = String.Format("Error in line {0} MR_INSTR_DSCR can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["INSTR_DETAILS"])) { cont = false; errMsg = String.Format("Error in line {0} INSTR_DETAILS can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["MAP_ID-1"])) { cont = false; errMsg = String.Format("Error in line {0} MAP_ID-1 can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["MAPX-1"])) { cont = false; errMsg = String.Format("Error in line {0} MAPX-1 can not be blank", iRow + 1); } }
                    //if (cont) { if (IsEmpty(dr["MAPY-1"])) { cont = false; errMsg = String.Format("Error in line {0} MAPY-1 can not be blank", iRow + 1); } }
                    if (cont)
                    {
                        if (Fx.IsEmpty(dr["Life Support"]))
                        { cont = false; errMsg = String.Format("Error in line {0} Life Support can not be blank", iRow + 1); }
                        else if (!LifeSup.Contains(dr["Life Support"].ToString().ToUpper()))
                        { cont = false; errMsg = String.Format("Error in line {0} Life Support value must be Y or N", iRow + 1); }
                    }
                    if (cont)
                    {
                        if (Fx.IsEmpty(dr["CommercCal or ResCdentCal"]))
                        { cont = false; errMsg = String.Format("Error in line {0} CommercCal or ResCdentCal can not be blank", iRow + 1); }
                        else if (!ComOrRes.Contains(dr["CommercCal or ResCdentCal"].ToString().ToUpper()))
                        { cont = false; errMsg = String.Format("Error in line {0} CommercCal or ResCdentCal value must be C or R", iRow + 1); }
                    }
                    if (cont)
                    {
                        if (Fx.IsEmpty(dr["DUPLICATE MTR"]))
                        { cont = false; errMsg = String.Format("Error in line {0} DUPLICATE MTR can not be blank", iRow + 1); }
                        else
                        {
                            bool blnDup; Boolean.TryParse(dr["DUPLICATE MTR"].ToString(), out blnDup);
                            if (blnDup == null)
                            { cont = false; errMsg = String.Format("Error in line {0} DUPLICATE MTR must be True or False", iRow + 1); }
                        }

                    }
                    if (cont) { if (Fx.IsEmpty(dr["MR_ROUTE_CY_SEQ-1"])) { cont = false; errMsg = String.Format("Error in line {0} MR_ROUTE_CY_SEQ-1 can not be blank", iRow + 1); } }

                    #endregion

                    if (cont)
                    {
                        for (int x = 0; x < dr.Table.Columns.Count; x++)
                        {
                            if (dr[x] == DBNull.Value) dr[x] = String.Empty;
                        }
                    }


                    if (!cont)
                    {
                        errCnt++;

                        ImpException impException = new ImpException();
                        impException.Description = errMsg;
                        impException.ImpExceptionId = -iRow;
                        impException.ImportFileLog = importFileLog;
                        impException.RowNo = iRow + 1;

                        db.ImpExceptions.AddObject(impException);
                    }
                }

                if (errCnt > 0)
                {
                    ReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                ReturnValue = false;

                errMsg = ex.Message;

                ImpException impException = new ImpException();
                impException.Description = errMsg;
                impException.ImpExceptionId = 0;
                impException.ImportFileLog = importFileLog;
                impException.RowNo = 0;

                db.ImpExceptions.AddObject(impException);
            }

            return ReturnValue;
        }
        private void eMRPProcess(String eType, DataTable dt, ref List<string> NMIList, ref List<KeyValuePair<string, Asset>> NMIAssetList, ref List<KeyValuePair<string, Installation>> NMIInstallationList, ref List<Suburb> SuburbList, ref List<KeyValuePair<Asset, Address>> addressList, ref int cnt, ref string errMsg, ref int errCnt, ref int importCount, int organisationID, int userId, string packageName)
        {
            DataRow dr = null;
            String NMI = String.Empty;

            Asset NMIAsset;
            Installation NMIInstallation;
            Asset MtrAsset;
            AssetAttribute MeterRegisterAttr;

            ServiceRequest SR = new ServiceRequest();
            Job job;
            Task task;

            int newImport = 0;

            List<KeyValuePair<string, AssetAttribute>> MeterRegList = new List<KeyValuePair<string, AssetAttribute>>();
            List<string> NMIMeterList = new List<string>();
            List<string> NMIMeterRegList = new List<string>();
            List<int> JobIDMultiOcs = new List<int>();

            List<StreetType> StreetTypeList = db.StreetTypes.ToList();

            try
            {

                #region Create ServiceRequest

                SR.ServiceRequestTypeID = (int)Global.ServiceRequestType.eMRP;
                SR.ContactID = 0;
                SR.IsNotReplicated = true;
                SR.Jobcount = 0;
                SR.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                SR.OrganisationID = organisationID;
                SR.StatusID = (int)Global.ServiceRequestStatus.New;
                SR.IsDeleted = false;
                SR.CreatedBy = userId;
                SR.CreatedDate = DateTime.Now;
                SR.LastModifiedBy = userId;
                SR.LastModifiedDate = DateTime.Now;
                SR.CustomerGrouping = packageName + "-" + eType;

                db.ServiceRequests.AddObject(SR);


                #endregion

                #region for loop

                for (int i1 = 0; i1 < dt.Rows.Count; i1++)
                {
                    cnt++;
                    errMsg = null;
                    dr = dt.Rows[i1];

                    NMI = dr["NMI"].ToString();
                    string installationName = ((eType == "Commercial") ? "C-NMI: " : "R-NMI: ") + NMI;
                    bool CreateJob = false;

                    // check if this NMI exists ELSE create it
                    var q = db.Assets.Where(a => a.OrganisationID == organisationID && a.CustomerAssetNumber == NMI && a.AssetDefinitionID == (int)Global.AssetDefinitionTypes.NMI).FirstOrDefault();
                    if (q == null && !NMIList.Contains(NMI))
                    {
                        NMIList.Add(NMI);

                        #region Create NMI
                        NMIAsset = new Asset();

                        NMIAsset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.NMI;
                        NMIAsset.CustomerAssetNumber = NMI;
                        NMIAsset.IsNotReplicated = true;
                        NMIAsset.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                        NMIAsset.OrganisationID = organisationID;
                        NMIAsset.StatusID = (int)Global.AssetStatus.Active;

                        NMIAsset.CreatedBy = userId;
                        NMIAsset.CreatedDate = DateTime.Now;
                        NMIAsset.LastModifiedBy = userId;
                        NMIAsset.LastModifiedDate = DateTime.Now;
                        NMIAsset.IsDeleted = false;

                        db.Assets.AddObject(NMIAsset);
                        NMIAssetList.Add(new KeyValuePair<string, Asset>(NMI, NMIAsset));
                        #endregion

                        AssetAttribute SlabAttr = null;
                        string Slab = dr["Slab_Site"].ToString();
                        if (Slab != string.Empty)
                        {
                            Slab = Slab.Trim();
                            SlabAttr = CreateAssetAttribute(NMIAsset, Global.ElectricityNMIAssetAttributeType.Slab.ToString(), Slab, userId);
                        }

                        #region Create NMI Installation
                        NMIInstallation = new Installation();

                        NMIInstallation.Name = installationName;
                        NMIInstallation.Description = "Connection Point Installation";

                        NMIInstallation.CreatedBy = userId;
                        NMIInstallation.CreatedDate = DateTime.Now;
                        NMIInstallation.LastModifiedBy = userId;
                        NMIInstallation.LastModifiedDate = DateTime.Now;

                        db.Installations.AddObject(NMIInstallation);
                        NMIInstallationList.Add(new KeyValuePair<string, Installation>(installationName, NMIInstallation));
                        #endregion

                        CreateJob = true;

                        newImport++;
                        importCount++;
                    }
                    else
                    {
                        NMIAsset = (q != null) ? (Asset)q : NMIAssetList.Where(x => x.Key == NMI).Select(x => x.Value).FirstOrDefault();
                        if (!NMIList.Contains(NMI))
                        {
                            CreateJob = true;
                            NMIList.Add(NMI);
                        }

                        var inst = db.Installations.Where(x => x.Name == installationName).FirstOrDefault();
                        NMIInstallation = (inst != null) ? (Installation)inst : NMIInstallationList.Where(x => x.Key == installationName).Select(x => x.Value).FirstOrDefault();
                    }

                    string currentNMIMeter = NMI + dr["MTR_NBR"].ToString();
                    string currentNMIMeterReg = currentNMIMeter + dr["REGISTER_CODE"].ToString();

                    if (!NMIMeterRegList.Contains(currentNMIMeterReg))
                    {
                        NMIMeterRegList.Add(currentNMIMeterReg);

                        if (!NMIMeterList.Contains(currentNMIMeter))
                        {
                            NMIMeterList.Add(currentNMIMeter);

                            #region Create METER

                            string CustAssetNum = dr["MTR_NBR"].ToString();
                            string MtrPrefix = dr["MTR_PREFIX"].ToString();

                            MtrAsset = db.Assets.Where(a => a.CustomerAssetNumber == CustAssetNum
                                                                             && a.Description == MtrPrefix
                                                                             && a.OrganisationID == organisationID
                                                                             && a.IsDeleted == false).FirstOrDefault();
                            if (MtrAsset == null)
                            {
                                MtrAsset = new Asset();

                                MtrAsset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.ElecMeter;
                                MtrAsset.CustomerAssetNumber = CustAssetNum;
                                MtrAsset.CustomerGrouping = dr["MR_ROUTE"].ToString();
                                MtrAsset.IsNotReplicated = true;
                                MtrAsset.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                MtrAsset.OrganisationID = organisationID;
                                MtrAsset.StatusID = (int)Global.AssetStatus.Active;
                                MtrAsset.Description = MtrPrefix;

                                string Instr = dr["MR_INSTR_DSCR"].ToString();
                                string InstrDet = dr["INSTR_DETAILS"].ToString();

                                string LocationDescription = ((Instr != "0") ? Instr : string.Empty) + ((InstrDet != "0") ? " " + InstrDet : string.Empty);
                                MtrAsset.Location = (LocationDescription != string.Empty) ? LocationDescription.Trim() : null;

                                MtrAsset.CreatedBy = userId;
                                MtrAsset.CreatedDate = DateTime.Now;
                                MtrAsset.LastModifiedBy = userId;
                                MtrAsset.LastModifiedDate = DateTime.Now;
                                MtrAsset.IsDeleted = false;

                                db.Assets.AddObject(MtrAsset);
                            }

                            #endregion

                            #region Add METER to Installation

                            bool NewMap = true;
                            if (NMIInstallation.EntityState != EntityState.Added)
                            {
                                var mapping = db.MappingInstallationAssets
                                                                    .Where(m => m.InstallationID == NMIInstallation.InstallationID);

                                if (MtrAsset.EntityState != EntityState.Added)
                                {
                                    MappingInstallationAsset mapped = mapping.Where(m => m.ChildAssetID == MtrAsset.AssetID).FirstOrDefault();
                                    NewMap = (mapped == null);
                                }
                            }

                            if (NewMap)
                            {
                                MappingInstallationAsset MappingInstallation = new MappingInstallationAsset();

                                MappingInstallation.Installation = NMIInstallation;
                                MappingInstallation.AssetParent = NMIAsset;
                                MappingInstallation.AssetChild = MtrAsset;

                                db.MappingInstallationAssets.AddObject(MappingInstallation);
                            }

                            #endregion

                            if (MtrAsset.EntityState == EntityState.Added)
                            {
                                #region Create Meter Register

                                MeterRegisterAttr = null;
                                string MeterRegister = dr["REGISTER_CODE"].ToString().Trim();
                                MeterRegisterAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.MeterRegisters.ToString(), MeterRegister, userId);

                                MeterRegList.Add(new KeyValuePair<string, AssetAttribute>(currentNMIMeter, MeterRegisterAttr));

                                #endregion

                                #region Address Details

                                string StreetNumber = (string)dr["ST_NBR"];
                                string StreetName = (string)dr["ST_NAME"];
                                string SuburbName = (string)dr["CITY"];

                                string UnitNumber = (string)dr["FLAT_NBR"];
                                string PostCode = (string)dr["ZIP_CODE"];
                                string Suffix = (string)dr["ST_SUFFIX"];

                                string SiteName = (string)dr["APT_NBR"];

                                Suburb Suburb = SuburbList.Where(x => x.Name == SuburbName.ToUpper()).FirstOrDefault();
                                if (Suburb == null) Suburb = SuburbList.Where(x => x.SuburbID == 0).FirstOrDefault();

                                StreetType StreetType = StreetTypeList.Where(x => x.Name == Suffix).FirstOrDefault();
                                if (StreetType == null)
                                {
                                    StreetType ST = new StreetType();
                                    ST.Name = Suffix;
                                    db.StreetTypes.AddObject(ST);
                                    StreetType = ST;
                                }

                                Address address = null;
                                if (UnitNumber != string.Empty)
                                {
                                    address = db.Addresses.Where(x => x.StreetNumber == StreetNumber
                                                                                       && x.StreetName == StreetName
                                                                                       && x.SuburbID == Suburb.SuburbID
                                                                                       && x.Unitflat == UnitNumber).FirstOrDefault();
                                }
                                else
                                {
                                    address = db.Addresses.Where(x => x.StreetNumber == StreetNumber
                                                                                       && x.StreetName == StreetName
                                                                                       && x.SuburbID == Suburb.SuburbID).FirstOrDefault();
                                }
                                if (address == null)
                                {
                                    address = new Address();
                                    address.AddressTypeID = 1; //Physical
                                    address.IsActive = true;
                                    address.Postcode = PostCode;
                                    address.SiteName = SiteName;
                                    address.StreetType = StreetType;
                                    address.Suburb = Suburb;
                                    address.StreetNumber = StreetNumber;
                                    address.StreetName = StreetName;
                                    address.Unitflat = UnitNumber;
                                    address.CreatedBy = userId;
                                    address.CreatedDate = DateTime.Now;
                                    address.LastModifiedBy = userId;
                                    address.LastModifiedDate = DateTime.Now;

                                    db.Addresses.AddObject(address);

                                    addressList.Add(new KeyValuePair<Asset, Address>(MtrAsset, address));
                                }
                                else
                                {
                                    address.SiteName = SiteName;

                                    //AddressID
                                    AssetAttribute AddressIDAttr = null;
                                    AddressIDAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.AddressID.ToString(), address.AddressID.ToString(), userId);
                                }

                                #endregion

                                #region Meter Attributes

                                AssetAttribute ConsumerNameAttr = null;
                                string NmPref = dr["NAME_PREFIX"].ToString();
                                string NmFirst = dr["FIRST_NAME"].ToString();
                                string NmLast = dr["LAST_NAME"].ToString();

                                string ConsumerName = NmPref + ' ' + NmFirst + ' ' + NmLast;
                                if (ConsumerName != string.Empty)
                                {
                                    ConsumerName = ConsumerName.Trim();
                                    ConsumerNameAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.ConsumerName.ToString(), ConsumerName, userId);
                                }

                                AssetAttribute BusinessNameAttr = null;
                                string BusinessName = dr["BUSINESS_NAME"].ToString();
                                if (BusinessName != string.Empty)
                                {
                                    BusinessName = BusinessName.Trim();
                                    BusinessNameAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.BusinessName.ToString(), BusinessName, userId);
                                }

                                AssetAttribute BusinessTypeAttr = null;
                                string BusinessType = dr["BUSINESS_TYPE"].ToString();
                                if (BusinessType != string.Empty)
                                {
                                    BusinessType = BusinessType.Trim();
                                    BusinessTypeAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.BusinessType.ToString(), BusinessType, userId);
                                }

                                AssetAttribute PhoneHomeAttr = null;
                                string PhoneHome = dr["PH_PRIMARY_NBR"].ToString();
                                if (PhoneHome != string.Empty)
                                {
                                    PhoneHome = PhoneHome.Trim();
                                    PhoneHomeAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.PhoneHome.ToString(), PhoneHome, userId);
                                }

                                AssetAttribute PhoneMobileAttr = null;
                                string PhoneMobile = dr["MOBILE_PHONE_NBR"].ToString();
                                if (PhoneMobile != string.Empty)
                                {
                                    PhoneMobile = PhoneMobile.Trim();
                                    PhoneMobileAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.PhoneMobile.ToString(), PhoneMobile, userId);
                                }

                                AssetAttribute ReadCycleAttr = null;
                                string ReadCycle = dr["MR_CYCLE"].ToString();
                                if (ReadCycle != string.Empty)
                                {
                                    ReadCycle = ReadCycle.Trim();
                                    ReadCycleAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.ReadCycle.ToString(), ReadCycle, userId);
                                }

                                AssetAttribute SequenceAttr = null;
                                string Sequence = dr["MR_ROUTE_CY_SEQ-1"].ToString();
                                if (Sequence != string.Empty)
                                {
                                    Sequence = Sequence.Trim();
                                    SequenceAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.Sequence.ToString(), Sequence, userId);
                                }

                                AssetAttribute KeyDetailsAttr = null;
                                string KeyDetails = dr["KEY_ID"].ToString();
                                if (KeyDetails != string.Empty)
                                {
                                    KeyDetails = KeyDetails.Trim();
                                    KeyDetailsAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.KeyDetails.ToString(), KeyDetails, userId);
                                }

                                AssetAttribute LifeSupportAttr = null;
                                string LifeSupport = dr["Life Support"].ToString();
                                if (LifeSupport != string.Empty)
                                {
                                    LifeSupport = LifeSupport.Trim();
                                    LifeSupportAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.LifeSupport.ToString(), LifeSupport, userId);
                                }

                                AssetAttribute DuplicateAttr = null;
                                string Duplicate = dr["DUPLICATE MTR"].ToString();
                                if (Duplicate != string.Empty)
                                {
                                    Duplicate = Duplicate.Trim();
                                    DuplicateAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.Duplicate.ToString(), Duplicate, userId);
                                }

                                AssetAttribute SiteTypeAttr = null;
                                string SiteType = dr["SITE TYPE"].ToString();
                                if (SiteType != string.Empty)
                                {
                                    SiteType = SiteType.Trim();
                                    SiteTypeAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.SiteType.ToString(), SiteType, userId);
                                }

                                AssetAttribute MeterTypeAttr = null;
                                string MeterType = dr["NEW_METER_TYPE-1"].ToString();
                                if (MeterType != string.Empty)
                                {
                                    MeterType = MeterType.Trim();
                                    MeterTypeAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.MeterType.ToString(), MeterType, userId);
                                }

                                AssetAttribute USAReliabilityAttr = null;
                                string USAReliability = dr["USA_RELIABILITY"].ToString();
                                if (USAReliability != string.Empty)
                                {
                                    USAReliability = USAReliability.Trim();
                                    USAReliabilityAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.USAReliability.ToString(), USAReliability, userId);
                                }

                                AssetAttribute PhaseNumberAttr = null;
                                string PhaseNumber = dr["SP_PHASE_NBR"].ToString();
                                if (PhaseNumber != string.Empty)
                                {
                                    PhaseNumber = PhaseNumber.Trim();
                                    PhaseNumberAttr = CreateAssetAttribute(MtrAsset, Global.ElectricityMeterAssetAttributeType.PhaseNumber.ToString(), PhaseNumber, userId);
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            //string MtrNbr =  dr["MTR_NBR"].ToString();
                            //MtrAsset = cUnifiedPortalEntities.Assets.Where(x => x.AssetDefinitionID == (int)Global.AssetDefinitionTypes.ElecMeter
                            //                                                 && x.CustomerAssetNumber == MtrNbr
                            //                                                 && x.IsDeleted == false).FirstOrDefault();
                            if (MeterRegList.Count > 0)
                            {
                                MeterRegisterAttr = MeterRegList.Where(x => x.Key == currentNMIMeter).Select(x => x.Value).FirstOrDefault();

                                string Registers = MeterRegisterAttr.Value.ToString();
                                MeterRegisterAttr.Value = Registers + "|" + dr["REGISTER_CODE"].ToString().Trim();
                            }
                        }
                    }

                    if (CreateJob)
                    {
                        #region Create Job

                        job = new Job();

                        job.Asset = NMIAsset;
                        job.ServiceRequest = SR;
                        job.IsAddedByInspector = false;
                        job.JobDefinitionID = (int)Global.JobDefinition.MeterReplacement;
                        job.JobResolutionID = (int)Global.JobResolution.Unresolved;
                        job.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                        job.OrganisationID = organisationID;
                        job.StatusID = (int)Global.JobStatus.New;
                        job.IsDeleted = false;
                        job.CreatedBy = userId;
                        job.CreatedDate = DateTime.Now;
                        job.LastModifiedBy = userId;
                        job.LastModifiedDate = DateTime.Now;
                        job.OwnerSystem = (eType == "Commercial") ? "C" : "R";

                        db.Jobs.AddObject(job);


                        #endregion

                        #region Create Task

                        task = new Task();

                        task.Job = job;
                        task.TaskDefinitionID = (int)Global.TaskDefinition.ScheduleMeterReplacement;
                        task.IsDeleted = false;
                        task.TeamID = 0;
                        task.ContactID = 0;
                        task.ProviderID = 0;
                        task.SelectReadSequence = 0;
                        task.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                        task.StatusID = (int)Global.TaskStatus.New;
                        task.TaskResolutionID = (int)Global.TaskResolution.NotResolved;

                        task.CreatedBy = userId;
                        task.CreatedDate = DateTime.Now;
                        task.LastModifiedBy = userId;
                        task.LastModifiedDate = DateTime.Now;

                        db.Tasks.AddObject(task);

                        #endregion
                    }

                }

                #endregion

                SR.Jobcount = newImport;
            }
            catch (Exception ex)
            {
                errCnt++;
                errMsg = ex.Message;
            }

        }

        #endregion

        #region MRP File Processing

        enum CWW_CSVIndex
        {
            MeterNumber = 0,
            LastReadDate = 1,
            LastRead = 2,
            LastReadType = 3,
            MeterStatus = 5,
            LocationCode = 6,
            LocationDescription1 = 7,
            LocationDescription2 = 8,
            InstallationCode = 10,
            UnitNo = 11,
            StreetNo = 12,
            StreetName = 13,
            Suburb = 14,
            PostCode = 15,
            AddressExtra1 = 16,
            AddressExtra2 = 17,
            Manufacturer = 18,
            MeterModel = 19,
            MeterDrive = 20,
            MeterSize = 21,
            AccountNo = 23,
            Owner = 26,
            ConsumerName = 27,
            ConsumerID = 28,
            PhoneHome = 29,
            PhoneMobile = 30,
            Melway = 31,
            Route = 32,
            SupplyCode = 33
        }
        enum YVW_CSVIndex
        {
            MeterID = 0,
            MeterNumber = 1,
            MeterSize = 3,
            ReadCycle = 11,
            Route = 12,
            District = 13,
            LocationDescription1 = 14,
            LocationCode = 15,
            LocationDescription2 = 16,
            Melway = 18,
            InstallationCode = 19,
            UnitNo = 23,
            StreetNo = 24,
            StreetName = 25,
            StreetSuffix = 26,
            Suburb = 27,
            PostCode = 28,
            AccountNo = 31,
            MailName = 32,
            MailAddress = 33,
            MailCity = 34,
            MailPostCode = 35,
            MailState = 36,
            ConsumerName = 38,
            PhoneHome = 39,
            PhoneMobile = 40,
            PhoneWork = 42,
            LastReadDate = 44,
            LastReadType = 45,
            LastRead = 46
        }
        private void ProcessMRPFile(Stream uploadFile, int organisationID, int userId, string packageName, BaseMessage messageQueue)
        {
            #region Properties

            DataSet ds = null;
            DataRow dr = null;

            int cnt = 0;
            int errCnt = 0;
            string errMsg = null;
            int importCount = 0;
            int existCount = 0;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            int ColCount = 0;
            string MeterNumber = String.Empty;
            string AccountNo = String.Empty;
            string ConsumerName = String.Empty;

            ServiceRequest SR = null;
            Asset asset;
            Job job;
            Task task;
            IList<KeyValuePair<Asset, Address>> addressList = new List<KeyValuePair<Asset, Address>>();
            IList<KeyValuePair<Asset, ManufacturerClassification>> modelList = new List<KeyValuePair<Asset, ManufacturerClassification>>();
            IList<String> MeterNo = new List<String>();

            XMessage message = null; 

            #endregion

            #region Populate AssetAttributeTypes Dictionary

            assetAttributeTypes = new Dictionary<string, int>();
            foreach (Global.WaterMeterAssetAttributeType AttrType in Enum.GetValues(typeof(Global.WaterMeterAssetAttributeType)))
            {
                assetAttributeTypes.Add(AttrType.ToString(), (int)AttrType);
            }

            #endregion

            if (!Check("Get MRP process")) return;

            try
            {

                #region Convert CSV to DataSet

                switch (organisationID)
                {
                    case (int)Global.Organisations.YarraValleyWater:
                        ColCount = 49;
                        ds = Fx.ImportCSVFile(uploadFile, "MRP", ',', true, ColCount);
                        break;
                    case (int)Global.Organisations.CityWestWater:
                        ColCount = 34;
                        ds = Fx.ImportCSVFile(uploadFile, "MRP", ',', true, ColCount);
                        break;
                }

                #endregion


                if (ds != null)
                {
                    if (ds.Tables["MRP"].Columns.Count != ColCount)
                    {
                        throw new ApplicationException(string.Format("Error Column count {0}, expected {1}", ds.Tables["MRP"].Columns.Count, ColCount));
                    }

                    if (ValidateMRPFile(ds, organisationID, out cnt, out errCnt))
                    {
                        //Process Valid data
                        cnt = 0;

                        #region Create ServiceRequest

                        SR = db.ServiceRequests.Where(x => x.CustomerGrouping.ToLower() == packageName.ToLower() && x.ServiceRequestTypeID == (int)Global.ServiceRequestType.MRP).FirstOrDefault();

                        if (SR == null)
                        {
                            SR = new ServiceRequest();
                            SR.ServiceRequestTypeID = (int)Global.ServiceRequestType.MRP;
                            SR.ContactID = 0;
                            SR.IsNotReplicated = true;
                            SR.Jobcount = 0;
                            SR.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                            SR.OrganisationID = organisationID;
                            SR.StatusID = (int)Global.ServiceRequestStatus.New;
                            SR.IsDeleted = false;
                            SR.CreatedBy = userId;
                            SR.CreatedDate = DateTime.Now;
                            SR.LastModifiedBy = userId;
                            SR.LastModifiedDate = DateTime.Now;
                            SR.CustomerGrouping = packageName;

                            db.ServiceRequests.AddObject(SR);
                        }

                        #endregion

                        IList<StreetType> StreetTypeList = db.StreetTypes.ToList();

                        for (int i1 = 0; i1 < ds.Tables["MRP"].Rows.Count; i1++)
                        {
                            cnt++;
                            errMsg = null;
                            dr = ds.Tables["MRP"].Rows[i1];

                            switch (organisationID)
                            {
                                case (int)Global.Organisations.YarraValleyWater:
                                    MeterNumber = (string)dr[(int)YVW_CSVIndex.MeterNumber];
                                    AccountNo = (string)dr[(int)YVW_CSVIndex.AccountNo];
                                    ConsumerName = (string)dr[(int)YVW_CSVIndex.ConsumerName];
                                    break;
                                case (int)Global.Organisations.CityWestWater:
                                    MeterNumber = (string)dr[(int)CWW_CSVIndex.MeterNumber];
                                    AccountNo = (string)dr[(int)CWW_CSVIndex.AccountNo];
                                    ConsumerName = (string)dr[(int)CWW_CSVIndex.ConsumerName];
                                    break;
                            }

                            // check if this meter exists
                            var q = db.Assets.Where(a => a.OrganisationID == organisationID && a.CustomerAssetNumber == MeterNumber).FirstOrDefault();

                            if (q == null && !MeterNo.Contains(MeterNumber))
                            {
                                MeterNo.Add(MeterNumber);

                                #region Create Asset
                                asset = new Asset();

                                asset.OrganisationID = organisationID;
                                asset.CustomerAssetNumber = MeterNumber;
                                asset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.WaterMeter;
                                asset.StatusID = (int)Global.AssetStatus.Active;
                                asset.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                asset.CreatedBy = userId;
                                asset.CreatedDate = DateTime.Now;
                                asset.LastModifiedBy = userId;
                                asset.LastModifiedDate = DateTime.Now;
                                asset.IsDeleted = false;
                                asset.CustomerGrouping = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.Route] : (string)dr[(int)YVW_CSVIndex.Route];
                                //MeterID
                                asset.SerialNumber = (organisationID == (int)Global.Organisations.CityWestWater) ? null : (string)dr[(int)YVW_CSVIndex.MeterID];
                                //MeterLocationDetail
                                asset.Location = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LocationDescription2] : (string)dr[(int)YVW_CSVIndex.LocationDescription2];
                                db.Assets.AddObject(asset);
                                #endregion

                                #region Create Asset Attributes

                                //LastReadDate
                                AssetAttribute LastReadDateAttr = null;
                                string LastReadDate = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LastReadDate] : (string)dr[(int)YVW_CSVIndex.LastReadDate];
                                LastReadDate = LastReadDate.Trim();
                                LastReadDateAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.LastReadDate.ToString(), LastReadDate, userId);

                                //LastRead
                                AssetAttribute LastReadAttr = null;
                                string LastRead = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LastRead] : (string)dr[(int)YVW_CSVIndex.LastRead];
                                LastRead = LastRead.Trim();
                                LastReadAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.LastRead.ToString(), LastRead, userId);

                                //LastReadType
                                AssetAttribute LastReadTypeAttr = null;
                                string LastReadType = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LastReadType] : (string)dr[(int)YVW_CSVIndex.LastReadType];
                                LastReadType = LastReadType.Trim();
                                LastReadTypeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.LastReadType.ToString(), LastReadType, userId);

                                //LocationCode
                                AssetAttribute LocationCodeAttr = null;
                                string LocationCode = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LocationCode] : (string)dr[(int)YVW_CSVIndex.LocationCode];
                                LocationCode = LocationCode.Trim();
                                LocationCodeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.LocationCode.ToString(), LocationCode, userId);

                                //MeterLocation
                                AssetAttribute LocationDescriptionAttr = null;
                                string LocationDescription = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.LocationDescription1] : (string)dr[(int)YVW_CSVIndex.LocationDescription1];
                                LocationDescription = LocationDescription.Trim();
                                LocationDescriptionAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.LocationDescription.ToString(), LocationDescription, userId);

                                //InstallationCode
                                AssetAttribute InstallationCodeAttr = null;
                                string InstallationCode = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.InstallationCode] : (string)dr[(int)YVW_CSVIndex.InstallationCode];
                                InstallationCode = InstallationCode.Trim();
                                InstallationCodeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.InstallationCode.ToString(), InstallationCode, userId);

                                //Meter Size
                                AssetAttribute MeterSizeAttr = null;
                                string MeterSize = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.MeterSize] : (string)dr[(int)YVW_CSVIndex.MeterSize];
                                MeterSize = MeterSize.Trim();
                                MeterSizeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MeterSize.ToString(), MeterSize, userId);

                                //AccountNo
                                AssetAttribute AccountNoAttr = null;
                                AccountNo = AccountNo.Trim();
                                AccountNoAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.AccountNo.ToString(), AccountNo, userId);

                                //Consumername
                                AssetAttribute ConsumerNameAttr = null;
                                ConsumerName = ConsumerName.Trim();
                                ConsumerNameAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.ConsumerName.ToString(), ConsumerName, userId);

                                //PhoneHome
                                AssetAttribute PhoneHomeAttr = null;
                                string PhoneHome = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.PhoneHome] : (string)dr[(int)YVW_CSVIndex.PhoneHome];
                                PhoneHome = PhoneHome.Trim();
                                PhoneHomeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.PhoneHome.ToString(), PhoneHome, userId);

                                //PhoneMobile
                                AssetAttribute PhoneMobileAttr = null;
                                string PhoneMobile = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.PhoneMobile] : (string)dr[(int)YVW_CSVIndex.PhoneMobile];
                                PhoneMobile = PhoneMobile.Trim();
                                PhoneMobileAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.PhoneMobile.ToString(), PhoneMobile, userId);

                                //Melway
                                AssetAttribute MelwayAttr = null;
                                string Melway = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.Melway] : (string)dr[(int)YVW_CSVIndex.Melway];
                                Melway = Melway.Trim();
                                MelwayAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.Melway.ToString(), Melway, userId);

                                switch (organisationID)
                                {
                                    case (int)Global.Organisations.YarraValleyWater:

                                        #region YarraValleyWater Specific Attributes

                                        //District
                                        AssetAttribute DistrictAttr = null;
                                        string District = (string)dr[(int)YVW_CSVIndex.District];
                                        District = District.Trim();
                                        DistrictAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.District.ToString(), District, userId);

                                        //MailName
                                        AssetAttribute MailNameAttr = null;
                                        string MailName = (string)dr[(int)YVW_CSVIndex.MailName];
                                        MailName = MailName.Trim();
                                        MailNameAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MailName.ToString(), MailName, userId);

                                        //MailAddress
                                        AssetAttribute MailAddressAttr = null;
                                        string MailAddress = (string)dr[(int)YVW_CSVIndex.MailAddress];
                                        MailAddress = MailAddress.Trim();
                                        MailAddressAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MailAddress.ToString(), MailAddress, userId);

                                        //MailCity
                                        AssetAttribute MailCityAttr = null;
                                        string MailCity = (string)dr[(int)YVW_CSVIndex.MailCity];
                                        MailCity = MailCity.Trim();
                                        MailCityAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MailCity.ToString(), MailCity, userId);

                                        //MailPostCode
                                        AssetAttribute MailPostCodeAttr = null;
                                        string MailPostCode = (string)dr[(int)YVW_CSVIndex.MailPostCode];
                                        MailPostCode = MailPostCode.Trim();
                                        MailPostCodeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MailPostCode.ToString(), MailPostCode, userId);

                                        //MailState
                                        AssetAttribute MailStateAttr = null;
                                        string MailState = (string)dr[(int)YVW_CSVIndex.MailState];
                                        MailState = MailState.Trim();
                                        MailStateAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MailState.ToString(), MailState, userId);

                                        //PhoneWork
                                        AssetAttribute PhoneWorkAttr = null;
                                        string PhoneWork = (string)dr[(int)YVW_CSVIndex.PhoneWork];
                                        PhoneWork = PhoneWork.Trim();
                                        PhoneWorkAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.PhoneWork.ToString(), PhoneWork, userId);

                                        //ReadCycle
                                        AssetAttribute ReadCycleAttr = null;
                                        string ReadCycle = (string)dr[(int)YVW_CSVIndex.ReadCycle];
                                        ReadCycleAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.ReadCycle.ToString(), ReadCycle, userId);

                                        #endregion

                                        break;
                                    case (int)Global.Organisations.CityWestWater:

                                        #region CityWestWater Specific Attributes

                                        //MeterStatus
                                        AssetAttribute MeterStatusAttr = null;
                                        string MeterStatus = (string)dr[(int)CWW_CSVIndex.MeterStatus];
                                        MeterStatus = MeterStatus.Trim();
                                        MeterStatusAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MeterStatus.ToString(), MeterStatus, userId);

                                        //AddressExtra
                                        AssetAttribute AddressExtraAttr = null;
                                        string AddressExtra1 = (string)dr[(int)CWW_CSVIndex.AddressExtra1];
                                        string AddressExtra2 = (string)dr[(int)CWW_CSVIndex.AddressExtra2];
                                        string AddressExtra = AddressExtra1 + " | " + AddressExtra2;
                                        AddressExtra = AddressExtra.Trim();
                                        AddressExtraAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.AddressExtra.ToString(), AddressExtra, userId);

                                        //MeterDrive
                                        AssetAttribute MeterDriveAttr = null;
                                        string MeterDrive = (string)dr[(int)CWW_CSVIndex.MeterDrive];
                                        MeterDrive = MeterDrive.Trim();
                                        MeterDriveAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.MeterDrive.ToString(), MeterDrive, userId);

                                        //Owner
                                        AssetAttribute OwnerAttr = null;
                                        string Owner = (string)dr[(int)CWW_CSVIndex.Owner];
                                        Owner = Owner.Trim();
                                        OwnerAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.Owner.ToString(), Owner, userId);

                                        //ConsumerID
                                        AssetAttribute ConsumerIDAttr = null;
                                        string ConsumerID = (string)dr[(int)CWW_CSVIndex.ConsumerID];
                                        ConsumerID = ConsumerID.Trim();
                                        ConsumerIDAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.ConsumerID.ToString(), ConsumerID, userId);

                                        //SupplyCode
                                        AssetAttribute SupplyCodeAttr = null;
                                        string SupplyCode = (string)dr[(int)CWW_CSVIndex.SupplyCode];
                                        SupplyCode = SupplyCode.Trim();
                                        SupplyCodeAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.SupplyCode.ToString(), SupplyCode, userId);

                                        ManufacturerClassification manufacturer = null;
                                        ManufacturerClassification Model = null;
                                        int ManufacturerClassificationID = 0;

                                        if (dr[(int)CWW_CSVIndex.Manufacturer] != null)
                                        {
                                            string ManuName = (string)dr[(int)CWW_CSVIndex.Manufacturer];
                                            manufacturer = db.ManufacturerClassifications
                                                                                 .Where(x => x.ManufacturerClassificationParentID == 0 && x.Name == ManuName)
                                                                                 .FirstOrDefault();

                                            if (manufacturer == null)
                                            {
                                                manufacturer = new ManufacturerClassification();
                                                manufacturer.Name = ManuName;
                                                manufacturer.ManufacturerClassificationParentID = 0;
                                                manufacturer.ModuleTypeID = (int)Global.ModuleType.Manufacturer;
                                                manufacturer.PendingReplication = true;
                                                manufacturer.IsActive = true;
                                                manufacturer.CreatedBy = userId;
                                                manufacturer.CreatedDate = DateTime.Now;
                                                manufacturer.LastModifiedBy = userId;
                                                manufacturer.LastModifiedDate = DateTime.Now;

                                                db.ManufacturerClassifications.AddObject(manufacturer);

                                                Save("Save MRP Data");
                                            }

                                            if (dr[(int)CWW_CSVIndex.MeterModel] != null)
                                            {
                                                string ModelName = (string)dr[(int)CWW_CSVIndex.MeterModel];
                                                Model = db.ManufacturerClassifications
                                                                              .Where(x => x.ModuleTypeID == 18
                                                                                       && x.Name == ModelName
                                                                                       && x.ManufacturerClassificationParentID == manufacturer.ManufacturerClassificationID)
                                                                              .FirstOrDefault();

                                                if (Model == null)
                                                {

                                                    Model = new ManufacturerClassification();
                                                    Model.Name = ModelName;
                                                    Model.ManufacturerClassificationParentID = manufacturer.ManufacturerClassificationID;
                                                    Model.ModuleTypeID = (int)Global.ModuleType.Model;
                                                    Model.PendingReplication = true;
                                                    Model.IsActive = true;
                                                    Model.CreatedBy = userId;
                                                    Model.CreatedDate = DateTime.Now;
                                                    Model.LastModifiedBy = userId;
                                                    Model.LastModifiedDate = DateTime.Now;

                                                    db.ManufacturerClassifications.AddObject(Model);

                                                    modelList.Add(new KeyValuePair<Asset, ManufacturerClassification>(asset, Model));
                                                }
                                            }
                                            else
                                            {
                                                ManufacturerClassificationID = manufacturer.ManufacturerClassificationID;
                                            }

                                        }

                                        asset.ManufactureClassificationID = ManufacturerClassificationID;

                                        #endregion

                                        break;
                                }

                                #region Address Details

                                string StreetNumber = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.StreetNo] : (string)dr[(int)YVW_CSVIndex.StreetNo];
                                string StreetName = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.StreetName] : (string)dr[(int)YVW_CSVIndex.StreetName];
                                string SuburbName = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.Suburb] : (string)dr[(int)YVW_CSVIndex.Suburb];

                                string UnitNumber = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.UnitNo] : (string)dr[(int)YVW_CSVIndex.UnitNo];
                                string PostCode = (organisationID == (int)Global.Organisations.CityWestWater) ? (string)dr[(int)CWW_CSVIndex.PostCode] : (string)dr[(int)YVW_CSVIndex.PostCode];
                                string Suffix = (string)dr[(int)YVW_CSVIndex.StreetSuffix];

                                Suburb Suburb = db.Suburbs.Where(x => x.Name == SuburbName.ToUpper()).FirstOrDefault();
                                if (Suburb == null) Suburb = db.Suburbs.Where(x => x.SuburbID == 0).FirstOrDefault();
                                StreetType StreetType = null;

                                switch (organisationID)
                                {
                                    case (int)Global.Organisations.CityWestWater:
                                        StreetTypeList.ToList().ForEach(x =>
                                        {
                                            if (StreetName.Contains(x.Name))
                                            {
                                                StreetName = StreetName.Remove(StreetName.IndexOf(x.Name), x.Name.Length);
                                                StreetType = x;
                                            }

                                        });
                                        break;
                                    case (int)Global.Organisations.YarraValleyWater:
                                        StreetType = StreetTypeList.Where(x => x.Name == Suffix).FirstOrDefault();
                                        if (StreetType == null)
                                        {
                                            StreetType ST = new StreetType();
                                            ST.Name = Suffix;
                                            db.StreetTypes.AddObject(ST);
                                            StreetType = ST;
                                        }
                                        break;
                                }

                                //Address address = db.Addresses.Where(x => x.StreetNumber == StreetNumber && x.StreetName == StreetName && x.SuburbID == Suburb.SuburbID).FirstOrDefault();
                                Address address = null;
                                if (UnitNumber != string.Empty)
                                {
                                    address = db.Addresses.Where(x => x.StreetNumber == StreetNumber
                                                                   && x.StreetName == StreetName
                                                                   && x.SuburbID == Suburb.SuburbID
                                                                   && x.Unitflat == UnitNumber).FirstOrDefault();
                                }
                                else
                                {
                                    address = db.Addresses.Where(x => x.StreetNumber == StreetNumber
                                                                   && x.StreetName == StreetName
                                                                   && x.SuburbID == Suburb.SuburbID).FirstOrDefault();
                                }
                                if (address == null)
                                {
                                    address = new Address();
                                    address.AddressTypeID = 1; //Physical
                                    address.IsActive = true;
                                    address.Postcode = PostCode;
                                    address.StreetType = StreetType;
                                    address.Suburb = Suburb;
                                    address.StreetNumber = StreetNumber;
                                    address.StreetName = StreetName;
                                    address.Unitflat = UnitNumber;
                                    address.CreatedBy = userId;
                                    address.CreatedDate = DateTime.Now;
                                    address.LastModifiedBy = userId;
                                    address.LastModifiedDate = DateTime.Now;

                                    db.Addresses.AddObject(address);

                                    addressList.Add(new KeyValuePair<Asset, Address>(asset, address));
                                }
                                else
                                {
                                    //AddressID
                                    AssetAttribute AddressIDAttr = null;
                                    AddressIDAttr = CreateAssetAttribute(asset, Global.WaterMeterAssetAttributeType.AddressID.ToString(), address.AddressID.ToString(), userId);
                                }

                                #endregion

                                #endregion

                                #region Create Job

                                job = new Job();

                                job.Asset = asset;
                                job.ServiceRequest = SR;
                                job.IsAddedByInspector = false;
                                job.JobDefinitionID = (int)Global.JobDefinition.MeterReplacement;
                                job.JobResolutionID = (int)Global.JobResolution.Unresolved;
                                job.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                job.OrganisationID = organisationID;
                                job.StatusID = (int)Global.JobStatus.New;
                                job.IsDeleted = false;
                                job.CreatedBy = userId;
                                job.CreatedDate = DateTime.Now;
                                job.LastModifiedBy = userId;
                                job.LastModifiedDate = DateTime.Now;

                                db.Jobs.AddObject(job);


                                #endregion

                                #region Create Task

                                task = new Task();

                                task.Job = job;
                                task.TaskDefinitionID = (int)Global.TaskDefinition.ScheduleMeterReplacement;
                                task.IsDeleted = false;
                                task.TeamID = 0;
                                task.ContactID = 0;
                                task.ProviderID = 0;
                                task.SelectReadSequence = 0;
                                task.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                task.StatusID = (int)Global.TaskStatus.New;
                                task.TaskResolutionID = (int)Global.TaskResolution.NotResolved;

                                task.CreatedBy = userId;
                                task.CreatedDate = DateTime.Now;
                                task.LastModifiedBy = userId;
                                task.LastModifiedDate = DateTime.Now;

                                db.Tasks.AddObject(task);

                                #endregion

                                importCount++;
                            }
                            else
                            {
                                errMsg = String.Format("Meter {0} in Line {1} exists. Account: #{2} - {3}", MeterNumber, i1 + 1, AccountNo, ConsumerName);

                                ImpException impException = new ImpException();
                                impException.Description = errMsg;
                                impException.ImpExceptionId = -i1;
                                impException.ImportFileLog = importFileLog;
                                impException.RowNo = i1 + 1;

                                db.ImpExceptions.AddObject(impException);

                                existCount++;
                            }
                        }

                        SR.Jobcount = importCount;

                        if (existCount != cnt)
                        {
                            Save("Save MRP Data");

                            #region Update attributes unlinked attributes

                            //Create AddressID attribute for all assets with new addesses
                            foreach (KeyValuePair<Asset, Address> Pair in addressList)
                            {
                                //AddressID
                                AssetAttribute AddressIDAttr = null;
                                AddressIDAttr = CreateAssetAttribute(Pair.Key, Global.WaterMeterAssetAttributeType.AddressID.ToString(), Pair.Value.AddressID.ToString(), userId);
                            }

                            //link all new models to assets
                            foreach (KeyValuePair<Asset, ManufacturerClassification> Model in modelList)
                            {
                                Model.Key.ManufactureClassificationID = Model.Value.ManufacturerClassificationID;
                            }

                            #endregion
                        }
                        else
                        {
                            db.ServiceRequests.DeleteObject(SR);
                        }

                        importFileStatus = "Imported Records: " + importCount.ToString() + " of " + ds.Tables["MRP"].Rows.Count.ToString();

                        if (errCnt > 0)
                            importFileStatus = existCount.ToString() + " Meters already imported";


                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = existCount;
                        rowCount = cnt;
                        errCount = existCount;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save MRP Data");

                        message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);

                    }
                    else
                    {
                        importFileStatus = "Validation errors. No record imported";

                        system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Validation, importFileStatus);

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save MRP Validation Error");
                        
                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);
                    }
                }
                else
                {
                    importFileStatus = "No record imported. Empty or corrupt file";
                    String ExcMessage = string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName);
                    throw new ApplicationException(ExcMessage);
                }
            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);

                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Disposal");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateMRPFile(DataSet vDS, int vOrganisationID, out int cnt, out int errCnt)
        {
            DataRow dr = null;
            bool cont = true;

            cnt = 0;
            errCnt = 0;
            string errMsg = null;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            bool ReturnValue = true;

            try
            {
                //Find duplicates and send exception
                DataTable FileTable = vDS.Tables["MRP"];

                //Validate empty fields
                for (int iRow = 0; iRow < FileTable.Rows.Count; iRow++)
                {
                    cnt++;
                    cont = true;
                    errMsg = null;
                    dr = FileTable.Rows[iRow];

                    switch (vOrganisationID)
                    {
                        case (int)Global.Organisations.YarraValleyWater:
                            #region Yarra Valley Water column value

                            if (Fx.IsEmpty(dr[0]))//MeterID
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} MeterID can not be blank", iRow + 1);
                            }
                            else
                            {
                                if (dr[0].ToString().Length < 10)
                                {
                                    string zero = string.Empty;
                                    for (int i = 0; i < (10 - dr[0].ToString().Length); i++)
                                    {
                                        zero = "0" + zero;
                                    }
                                    dr[0] = zero + dr[0];
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[1]))//Meter Number
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Meter Number can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[24]))//Street Number
                                {
                                    dr[24] = 0;
                                    //cont = false;
                                    //errMsg = String.Format("Error in line {0} Street Number can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[25]))//Street Name
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Stree Name can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[27]))//Suburb
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Suburb can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                foreach (YVW_CSVIndex Col in Enum.GetValues(typeof(YVW_CSVIndex)))
                                {
                                    if (dr[(int)Col] == DBNull.Value) dr[(int)Col] = String.Empty;
                                }
                            }

                            #endregion
                            break;
                        case (int)Global.Organisations.CityWestWater:
                            #region City West Water column value

                            if (Fx.IsEmpty(dr[0]))//Meter Number
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Meter Number can not be blank", iRow + 1);
                            }


                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[12]))//Street Number
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Stree Number can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[13]))//Street Name
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Stree Name can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                if (Fx.IsEmpty(dr[14]))//Suburb
                                {
                                    cont = false;
                                    errMsg = String.Format("Error in line {0} Suburb can not be blank", iRow + 1);
                                }
                            }

                            if (cont)
                            {
                                foreach (CWW_CSVIndex Col in Enum.GetValues(typeof(CWW_CSVIndex)))
                                {
                                    if (dr[(int)Col] == DBNull.Value) dr[(int)Col] = String.Empty;
                                }
                            }

                            #endregion
                            break;
                    }

                    if (!cont)
                    {
                        errCnt++;

                        ImpException impException = new ImpException();
                        impException.Description = errMsg;
                        impException.ImpExceptionId = -iRow;
                        impException.ImportFileLog = importFileLog;
                        impException.RowNo = iRow + 1;

                        db.ImpExceptions.AddObject(impException);
                    }
                }

                if (errCnt > 0)
                {
                    ReturnValue = false;
                }
                //}
            }
            catch (Exception ex)
            {
                ReturnValue = false;

                errMsg = ex.Message;

                ImpException impException = new ImpException();
                impException.Description = errMsg;
                impException.ImpExceptionId = 0;
                impException.ImportFileLog = importFileLog;
                impException.RowNo = 0;

                db.ImpExceptions.AddObject(impException);
            }


            return ReturnValue;
        }
        private void ImportMRPAssets(Stream uploadFile, string fileName, int organisationID, int userId, BaseMessage messageQueue)
        {
            DataSet ds = null;
            DataRow dr = null;

            int cnt = 0;
            int errCnt = 0;
            int importCount = 0;
            int existCount = 0;
            string errMsg;

            int ColCount = 1;
            string MeterNumber = null;
            Asset asset = null;

            XMessage message = null;

            if (!Check("Get MRP Asset process")) return;

            try
            {

                ds = Fx.ImportCSVFile(uploadFile, "MRPAssets", ',', true, ColCount);

                if (ds != null)
                {
                    if (ds.Tables["MRPAssets"].Columns.Count != ColCount)
                    {
                        throw new ApplicationException(string.Format("Error Column count {0}, expected {1}", ds.Tables["MRPAssets"].Columns.Count, ColCount));
                    }

                    if (ValidateMRPAssetFile(ds, out cnt, out errCnt))
                    {
                        //Process Valid data: Create "Available" Meters
                        for (int i1 = 0; i1 < ds.Tables["MRPAssets"].Rows.Count; i1++)
                        {
                            dr = ds.Tables["MRPAssets"].Rows[i1];

                            MeterNumber = (string)dr[0];
                            MeterNumber = MeterNumber.Trim();

                            #region Create Asset

                            // check if this meter exists in assets table
                            var q = db.Assets.Where(a => a.OrganisationID == organisationID && a.CustomerAssetNumber == MeterNumber).FirstOrDefault();
                            if (q == null)
                            {
                                asset = new Asset();

                                asset.OrganisationID = organisationID;
                                asset.CustomerAssetNumber = MeterNumber;
                                asset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.WaterMeter;
                                asset.StatusID = (int)Global.AssetStatus.Available;
                                asset.CreatedBy = userId;
                                asset.CreatedDate = DateTime.Now;
                                asset.LastModifiedBy = userId;
                                asset.LastModifiedDate = DateTime.Now;
                                asset.IsDeleted = false;

                                db.Assets.AddObject(asset);
                                importCount++;
                            }
                            else
                            {
                                errMsg = String.Format("Meter {0} in Line {1} already exist", MeterNumber, i1 + 1);

                                ImpException impException = new ImpException();
                                impException.Description = errMsg;
                                impException.ImpExceptionId = -i1;
                                impException.ImportFileLog = importFileLog;
                                impException.RowNo = i1 + 1;

                                db.ImpExceptions.AddObject(impException);

                                existCount++;
                            }

                            #endregion

                        }

                        importFileStatus = "Imported Meters: " + importCount.ToString() + " of " + ds.Tables["MRPAssets"].Rows.Count.ToString();

                        if (existCount > 0)
                            importFileStatus = existCount.ToString() + " Meters already in store";


                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = existCount;
                        rowCount = cnt;
                        errCount = existCount;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save MRP Asset Data");

                        message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);

                    }
                    else
                    {

                        importFileStatus = "Validation errors. No record imported";

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save MRPAsset Validation Error");

                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, cnt, errCnt, userId);
                        messageQueue.SendMessage(message);

                    }
                }
                else
                {
                    importFileStatus = "No record imported. Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName));
                }
            }
            catch (Exception ex)
            {
                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Dispose");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateMRPAssetFile(DataSet vDS, out int cnt, out int errCnt)
        {
            DataRow dr = null;
            bool cont = true;

            cnt = 0;
            errCnt = 0;
            string errMsg = null;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            bool ReturnValue = true;

            for (int iRow = 0; iRow < vDS.Tables["MRPAssets"].Rows.Count; iRow++)
            {
                cnt++;
                cont = true;
                errMsg = null;
                dr = vDS.Tables["MRPAssets"].Rows[iRow]; ;


                if (Fx.IsEmpty(dr[0]))//Meter Number
                {
                    cont = false;
                    errMsg = String.Format("Error in line {0} Meter Number can not be blank", iRow + 1);
                }


                if (!cont)
                {
                    errCnt++;

                    ImpException impException = new ImpException();
                    impException.Description = errMsg;
                    impException.ImpExceptionId = -iRow;
                    impException.ImportFileLog = importFileLog;
                    impException.RowNo = iRow + 1;

                    db.ImpExceptions.AddObject(impException);
                }
            }

            if (errCnt > 0)
            {
                ReturnValue = false;
            }

            return ReturnValue;
        }
        private void UpdateMRPAssets(Stream uploadFile, string fileName, int organisationID, int userId, BaseMessage messageQueue)
        {
            DataSet ds = null;
            DataRow dr = null;

            int cnt = 0;
            int errCnt = 0;
            int updateCount = 0;
            int notExistCount = 0;
            string errMsg;

            int ColCount = 2;
            string MeterNumber = null;
            string CybleNumber = null;
            Asset asset = null;
            
            XMessage message = null;

            if (!Check("Get MRP Cyble process")) return;

            try
            {

                ds = Fx.ImportCSVFile(uploadFile, "MRPCyble", ',', true, ColCount);


                if (ds != null)
                {
                    if (ds.Tables["MRPCyble"].Columns.Count != ColCount)
                    {
                        throw new ApplicationException(string.Format("Error Column count {0}, expected {1}", ds.Tables["MRPCyble"].Columns.Count, ColCount));
                    }

                    if (ValidateMRPCybleFile(ds, organisationID, out cnt, out errCnt))
                    {
                        //Process Valid data: Create "Available" Meters
                        for (int i1 = 0; i1 < ds.Tables["MRPCyble"].Rows.Count; i1++)
                        {
                            dr = ds.Tables["MRPCyble"].Rows[i1];

                            MeterNumber = (string)dr[0];
                            MeterNumber = MeterNumber.Trim();
                            CybleNumber = (string)dr[1];
                            CybleNumber = CybleNumber.Trim();

                            #region Update Asset

                            // check if this meter exists in assets table
                            var q = db.Assets.Where(a => a.OrganisationID == organisationID && a.CustomerAssetNumber == MeterNumber).FirstOrDefault();
                            if (q != null)
                            {
                                asset = q;
                                asset.SerialNumber = CybleNumber;
                                asset.LastModifiedBy = userId;
                                asset.LastModifiedDate = DateTime.Now;

                                updateCount++;
                            }
                            else
                            {
                                errMsg = String.Format("Meter {0} in Line {1} does not exist", MeterNumber, i1 + 1);

                                ImpException impException = new ImpException();
                                impException.Description = errMsg;
                                impException.ImpExceptionId = -i1;
                                impException.ImportFileLog = importFileLog;
                                impException.RowNo = i1 + 1;

                                db.ImpExceptions.AddObject(impException);

                                notExistCount++;
                            }

                            #endregion

                        }

                        importFileStatus = "Updated " + updateCount.ToString() + " of " + ds.Tables["MRPCyble"].Rows.Count.ToString();

                        if (notExistCount > 0)
                            importFileStatus = notExistCount.ToString() + " not in store";


                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = notExistCount;
                        rowCount = cnt;
                        errCount = notExistCount;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;


                        Save("Save MRP Cyble Data");

                        message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);
                    }
                    else
                    {

                        importFileStatus = "Validation errors. No record imported";

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save MRP Cyble Validation Error");

                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, cnt, errCnt, userId);
                        messageQueue.SendMessage(message);

                    }
                }
                else
                {
                    importFileStatus = "No record imported. Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName));
                }
            }
            catch (Exception ex)
            {
                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Dispose");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateMRPCybleFile(DataSet vDS, int vOrganisationID, out int cnt, out int errCnt)
        {
            DataRow dr = null;
            bool cont = true;

            cnt = 0;
            errCnt = 0;
            string errMsg = null;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");
            
            bool ReturnValue = true;

            for (int iRow = 0; iRow < vDS.Tables["MRPCyble"].Rows.Count; iRow++)
            {
                cnt++;
                cont = true;
                errMsg = null;
                dr = vDS.Tables["MRPCyble"].Rows[iRow]; ;


                if (Fx.IsEmpty(dr[0]))//Meter Number
                {
                    cont = false;
                    errMsg = String.Format("Error in line {0} Meter Number can not be blank", iRow + 1);
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[1]))//Cyble Number
                    {
                        cont = false;
                        errMsg = String.Format("Error in line {0} Cyble Number can not be blank", iRow + 1);
                    }
                }

                if (!cont)
                {
                    errCnt++;

                    ImpException impException = new ImpException();
                    impException.Description = errMsg;
                    impException.ImpExceptionId = -iRow;
                    impException.ImportFileLog = importFileLog;
                    impException.RowNo = iRow + 1;

                    db.ImpExceptions.AddObject(impException);
                }
            }

            if (errCnt > 0)
            {
                ReturnValue = false;
            }

            return ReturnValue;
        }

        #endregion

        #region AI File Processing

        enum SPI_CSVIndex
        {
            Longitude,
            Latitude,
            Map_Reference,
            Pole_Number,
            Feeder,
            Work_Sector,
            AMFM_Pole_ID,
            Equip_Type,
            Description,
            PEL_Attached,
            Suburb_Town,
            Fire_Area,
            Q4_Switch_Zone,
            Area,
            Span_Length,
            PM_Number,
            PM_Module,
            Sched_Year,
            Month_Full_Cycle,
            Mid_Cycle_FY,
            Month_Image_Capture,
            Month_Tech_Assess,
            Inspection_Type
        }
        private void ImportAISPAusnet(Stream uploadFile, string fileName, int ContactID, string SRLocation, BaseMessage messageQueue)
        {
            #region Properties

            DataSet ds = null;

            int cnt = 0;
            int errCnt = 0;
            string errMsg = null;
            int importCount = 0;
            int existCount = 0;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            int SRTypeID = (int)Global.ServiceRequestType.AssetInspection;
            int OrganisationID = (int)Global.Organisations.SPINetwork;

            ServiceRequest serviceRequest;
            Asset asset;
            JobAsset jobAsset;
            Job job;
            Task task;

            #endregion

            #region Populate AssetAttributeTypes Dictionary

            assetAttributeTypes = new Dictionary<string, int>();
            foreach (Global.AssetInspectionAssetAttributeType AttrType in Enum.GetValues(typeof(Global.AssetInspectionAssetAttributeType)))
            {
                assetAttributeTypes.Add(AttrType.ToString(), (int)AttrType);
            }

            #endregion

            XMessage message = null;
            if (!Check("Get AI process")) return;

            try
            {

                #region Convert CSV to DataSet

                ds = Fx.ImportCSVFile(uploadFile, "Poles", ',', true, 0);


                #endregion

                if (ds != null)
                {
                    if (ValidateAISPAusnetFile(ds, out cnt, out errCnt))
                    {
                        //Process Valid data
                        cnt = 0;

                        DataTable DT = ds.Tables["Poles"];

                        string[] WorkSectors;
                        IList<DataRow> AerialInspections;
                        IList<DataRow> GroundInspections;
                        IList<DataRow> GPSInspections;
                        IList<KeyValuePair<Job, JobAsset>> JobAssetList = new List<KeyValuePair<Job, JobAsset>>();

                        WorkSectors = DT.AsEnumerable().Select(x => x[SPI_CSVIndex.Work_Sector.ToString()].ToString()).Distinct().ToArray();
                        foreach (string WorkSector in WorkSectors)
                        {
                            AerialInspections = DT.AsEnumerable().Where(x => x[SPI_CSVIndex.Inspection_Type.ToString()].ToString().ToLower() == "aerial"
                                                                          && x[SPI_CSVIndex.Work_Sector.ToString()].ToString() == WorkSector
                                                                          && (x[SPI_CSVIndex.Latitude.ToString()].ToString() != String.Empty
                                                                          && x[SPI_CSVIndex.Longitude.ToString()].ToString() != String.Empty)).ToList();
                            GroundInspections = DT.AsEnumerable().Where(x => x[SPI_CSVIndex.Inspection_Type.ToString()].ToString().ToLower() == "ground"
                                                                          && x[SPI_CSVIndex.Work_Sector.ToString()].ToString() == WorkSector
                                                                          && (x[SPI_CSVIndex.Latitude.ToString()].ToString() != String.Empty
                                                                          && x[SPI_CSVIndex.Longitude.ToString()].ToString() != String.Empty)).ToList();
                            GPSInspections = DT.AsEnumerable().Where(x => x[SPI_CSVIndex.Work_Sector.ToString()].ToString() == WorkSector
                                                                       && (x[SPI_CSVIndex.Latitude.ToString()].ToString() == String.Empty
                                                                       || x[SPI_CSVIndex.Longitude.ToString()].ToString() == String.Empty)).ToList();


                            bool Cont = true;
                            String CustomerNumber;

                            #region Aerial ServiceRequest
                            if (AerialInspections != null && AerialInspections.Count > 0)
                            {
                                //create Service Request
                                #region ServiceRequest
                                //Sector Due Date
                                int[] TechAccesDates = AerialInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString())).ToArray();
                                int TechAccess = TechAccesDates.Min();
                                DateTime SectorDueDate = GetDueDate(TechAccess);

                                serviceRequest = db.ServiceRequests
                                                    .Where(x => x.ServiceRequestTypeID == SRTypeID
                                                            && x.CustomerGrouping == WorkSector
                                                            && x.AgreedDueDate == SectorDueDate)
                                                    .FirstOrDefault();

                                if (serviceRequest == null)
                                {
                                    //Sector Start Date
                                    int[] ImageCaptureDates = AerialInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString())).ToArray();
                                    int ImageCapture = ImageCaptureDates.Min();
                                    DateTime SectorStartDate = GetStartDate(ImageCapture);

                                    serviceRequest = db.ServiceRequests.CreateObject();

                                    serviceRequest.CreatedBy = ContactID;
                                    serviceRequest.CreatedDate = DateTime.Now;
                                    serviceRequest.CustomerGrouping = WorkSector;
                                    serviceRequest.IsDeleted = false;
                                    serviceRequest.IsNotReplicated = false;
                                    serviceRequest.Jobcount = AerialInspections.Count();
                                    serviceRequest.LastModifiedBy = ContactID;
                                    serviceRequest.LastModifiedDate = DateTime.Now;
                                    serviceRequest.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                    serviceRequest.OrganisationID = OrganisationID;
                                    serviceRequest.ServiceRequestTypeID = SRTypeID;
                                    serviceRequest.StatusID = (int)Global.ServiceRequestStatus.New;
                                    serviceRequest.Location = SRLocation;
                                    serviceRequest.AgreedStartDate = SectorStartDate;
                                    serviceRequest.AgreedDueDate = SectorDueDate;
                                    serviceRequest.AuditPercentage = 6;

                                    db.ServiceRequests.AddObject(serviceRequest);

                                }
                                #endregion

                                foreach (DataRow dr in AerialInspections)
                                {
                                    cnt++;
                                    errMsg = null;

                                    CustomerNumber = dr[SPI_CSVIndex.Pole_Number.ToString()].ToString();
                                    int AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                    int vRegionID = 0;

                                    #region Asset
                                    // check if asset exists
                                    asset = db.Assets
                                                .Where(a => a.OrganisationID == OrganisationID
                                                        && a.CustomerAssetNumber == CustomerNumber
                                                        && a.AssetDefinitionID == AssetDefinitionID)
                                                .FirstOrDefault();

                                    if (asset == null)
                                    {
                                        asset = db.Assets.CreateObject();

                                        asset.OrganisationID = OrganisationID;
                                        asset.CustomerAssetNumber = CustomerNumber;
                                        asset.AssetDefinitionID = AssetDefinitionID;
                                        asset.IsNotReplicated = true;
                                        asset.CreatedBy = ContactID;
                                        asset.CreatedDate = DateTime.Now;
                                        asset.IsDeleted = false;

                                        db.Assets.AddObject(asset);
                                    }

                                    if (!string.Equals(asset.CustomerGrouping, dr[SPI_CSVIndex.Feeder.ToString()].ToString()))
                                        asset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();

                                    if (!string.Equals(asset.Description, dr[SPI_CSVIndex.Description.ToString()].ToString()))
                                        asset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();

                                    if (!string.Equals(asset.Location, dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString()))
                                        asset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefX, dr[SPI_CSVIndex.Longitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefX = dr[SPI_CSVIndex.Longitude.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefY, dr[SPI_CSVIndex.Latitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefY = dr[SPI_CSVIndex.Latitude.ToString()].ToString();

                                    asset.StatusID = (int)Global.AssetStatus.Active;


                                    if (asset.EntityState == EntityState.Added || asset.EntityState == EntityState.Modified)
                                    {
                                        asset.LastModifiedBy = ContactID;
                                        asset.LastModifiedDate = DateTime.Now;
                                    }
                                    #endregion

                                    if (asset.EntityState == EntityState.Modified && serviceRequest.EntityState != EntityState.Added)
                                    {
                                        bool JobCheck = db.Jobs.Any(x => x.AssetID == asset.AssetID && x.ServiceRequestID == serviceRequest.ServiceRequestID);
                                        Cont = !JobCheck;
                                    }

                                    if (Cont)
                                    {
                                        //Create JobAsset
                                        #region JobAsset
                                        jobAsset = db.JobAssets.CreateObject();

                                        string orgName = db.Organisations.Where(o => o.OrganisationID == OrganisationID).Select(o => o.BusinessName).FirstOrDefault();

                                        jobAsset.OrganisationID = OrganisationID;
                                        jobAsset.OrganisationName = orgName;
                                        jobAsset.CustomerAssetNumber = CustomerNumber;
                                        jobAsset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                        jobAsset.AssetDefinition = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.AssetTypeID = 9;
                                        jobAsset.AssetType = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.CreatedBy = ContactID;
                                        jobAsset.CreatedDate = DateTime.Now;
                                        jobAsset.LastModifiedBy = ContactID;
                                        jobAsset.LastModifiedDate = DateTime.Now;
                                        jobAsset.IsDeleted = false;

                                        jobAsset.AssetID = asset.AssetID;

                                        jobAsset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();
                                        jobAsset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();
                                        jobAsset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();
                                        jobAsset.GpsGoogleMapRefX = (dr[SPI_CSVIndex.Longitude.ToString()].ToString() != string.Empty) ? dr[SPI_CSVIndex.Longitude.ToString()].ToString() : null;
                                        jobAsset.GpsGoogleMapRefY = (dr[SPI_CSVIndex.Latitude.ToString()].ToString() != string.Empty) ? dr[SPI_CSVIndex.Latitude.ToString()].ToString() : null;
                                        jobAsset.StatusID = (int)Global.AssetStatus.Active;
                                        jobAsset.StatusName = Global.AssetStatus.Active.ToString();

                                        db.JobAssets.AddObject(jobAsset);
                                        #endregion

                                        //Create/Update Asset & Job Asset Attributes
                                        #region Attributes
                                        //Set Region ID
                                        vRegionID = GetRegionByName(asset.CustomerGrouping, OrganisationID);
                                        vRegionID = (vRegionID == 0) ? CreateRegion(asset.CustomerGrouping, OrganisationID, ContactID) : vRegionID;
                                        SetAssetAttribute(asset, "RegionID", vRegionID.ToString(), ContactID);
                                        SetJobAssetAttribute(jobAsset, "RegionID", vRegionID.ToString(), ContactID);

                                        if (dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Equip_Type.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Fire_Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Number.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Module.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Sched_Year.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Map_Reference.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MapReference.ToString(),
                                                                     dr[SPI_CSVIndex.Map_Reference.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MapReference.ToString(),
                                                                     dr[SPI_CSVIndex.Map_Reference.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Span_Length.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SpanLength.ToString(),
                                                                     dr[SPI_CSVIndex.Span_Length.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SpanLength.ToString(),
                                                                     dr[SPI_CSVIndex.Span_Length.ToString()].ToString(), ContactID);
                                        }
                                        #endregion

                                        //Create Job
                                        #region Job
                                        job = db.Jobs.CreateObject();
                                        job.JobDefinitionID = (int)Global.JobDefinition.AssetInpection;
                                        job.Asset = asset;
                                        job.ServiceRequest = serviceRequest;
                                        job.JobAssetID = jobAsset.JobAssetID;
                                        job.OrganisationID = OrganisationID;
                                        job.StatusID = (int)Global.JobStatus.New;
                                        job.JobResolutionID = (int)Global.JobResolution.Unresolved;
                                        job.IsAddedByInspector = false;
                                        job.CreatedBy = ContactID;
                                        job.CreatedDate = DateTime.Now;
                                        job.LastModifiedBy = ContactID;
                                        job.LastModifiedDate = DateTime.Now;

                                        db.Jobs.AddObject(job);
                                        #endregion

                                        // create a new Scheduling task
                                        #region Task
                                        task = new Task();

                                        task.Job = job;
                                        task.TaskResolutionID = (int)Global.TaskResolution.NotResolved;
                                        task.SelectReadSequence = 0;
                                        task.StatusID = (int)Global.TaskStatus.New;
                                        task.TaskDefinitionID = (int)Global.TaskDefinition.SchedulingAIAerial;
                                        task.TeamID = (int)Global.Team.NoTeamAssigned;
                                        task.CreatedBy = ContactID;
                                        task.CreatedDate = DateTime.Now;
                                        task.LastModifiedBy = ContactID;
                                        task.LastModifiedDate = DateTime.Now;

                                        db.Tasks.AddObject(task);
                                        #endregion

                                        JobAssetList.Add(new KeyValuePair<Job, JobAsset>(job, jobAsset));

                                        importCount++;
                                    }
                                    else
                                    {
                                        existCount++;
                                    }
                                }

                            }
                            #endregion

                            #region Ground ServiceRequest
                            if (GroundInspections != null && GroundInspections.Count > 0)
                            {
                                //create Service Request
                                #region ServiceRequest
                                //Sector Due Date
                                int[] TechAccesDates = GroundInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString())).ToArray();
                                int TechAccess = TechAccesDates.Min();
                                DateTime SectorDueDate = GetDueDate(TechAccess);

                                serviceRequest = db.ServiceRequests
                                                                       .Where(x => x.ServiceRequestTypeID == SRTypeID
                                                                                && x.CustomerGrouping == WorkSector
                                                                                && x.AgreedDueDate == SectorDueDate)
                                                                       .FirstOrDefault();

                                if (serviceRequest == null)
                                {
                                    //Sector Start Date
                                    int[] ImageCaptureDates = GroundInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString())).ToArray();
                                    int ImageCapture = ImageCaptureDates.Min();
                                    DateTime SectorStartDate = GetStartDate(ImageCapture);

                                    serviceRequest = db.ServiceRequests.CreateObject();

                                    serviceRequest.CreatedBy = ContactID;
                                    serviceRequest.CreatedDate = DateTime.Now;
                                    serviceRequest.CustomerGrouping = WorkSector;
                                    serviceRequest.IsDeleted = false;
                                    serviceRequest.IsNotReplicated = false;
                                    serviceRequest.Jobcount = AerialInspections.Count();
                                    serviceRequest.LastModifiedBy = ContactID;
                                    serviceRequest.LastModifiedDate = DateTime.Now;
                                    serviceRequest.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                    serviceRequest.OrganisationID = OrganisationID;
                                    serviceRequest.ServiceRequestTypeID = SRTypeID;
                                    serviceRequest.StatusID = (int)Global.ServiceRequestStatus.New;
                                    serviceRequest.Location = SRLocation;
                                    serviceRequest.AgreedStartDate = SectorStartDate;
                                    serviceRequest.AgreedDueDate = SectorDueDate;

                                    db.ServiceRequests.AddObject(serviceRequest);

                                }
                                #endregion

                                foreach (DataRow dr in GroundInspections)
                                {
                                    cnt++;
                                    errMsg = null;

                                    CustomerNumber = dr[SPI_CSVIndex.Pole_Number.ToString()].ToString();
                                    int AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                    int vRegionID = 0;

                                    #region Asset
                                    // check if asset exists
                                    asset = db.Assets
                                                                  .Where(a => a.OrganisationID == OrganisationID
                                                                           && a.CustomerAssetNumber == CustomerNumber
                                                                           && a.AssetDefinitionID == AssetDefinitionID)
                                                                  .FirstOrDefault();

                                    if (asset == null)
                                    {
                                        asset = db.Assets.CreateObject();

                                        asset.OrganisationID = OrganisationID;
                                        asset.CustomerAssetNumber = CustomerNumber;
                                        asset.AssetDefinitionID = AssetDefinitionID;
                                        asset.IsNotReplicated = true;
                                        asset.CreatedBy = ContactID;
                                        asset.CreatedDate = DateTime.Now;
                                        asset.IsDeleted = false;

                                        db.Assets.AddObject(asset);
                                    }

                                    if (!string.Equals(asset.CustomerGrouping, dr[SPI_CSVIndex.Feeder.ToString()].ToString()))
                                        asset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();

                                    if (!string.Equals(asset.Description, dr[SPI_CSVIndex.Description.ToString()].ToString()))
                                        asset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();

                                    if (!string.Equals(asset.Location, dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString()))
                                        asset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefX, dr[SPI_CSVIndex.Longitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefX = dr[SPI_CSVIndex.Longitude.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefY, dr[SPI_CSVIndex.Latitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefY = dr[SPI_CSVIndex.Latitude.ToString()].ToString();

                                    asset.StatusID = (int)Global.AssetStatus.Active;


                                    if (asset.EntityState == EntityState.Added || asset.EntityState == EntityState.Modified)
                                    {
                                        asset.LastModifiedBy = ContactID;
                                        asset.LastModifiedDate = DateTime.Now;
                                    }
                                    #endregion

                                    if (asset.EntityState == EntityState.Modified && serviceRequest.EntityState != EntityState.Added)
                                    {
                                        bool JobCheck = db.Jobs.Any(x => x.AssetID == asset.AssetID && x.ServiceRequestID == serviceRequest.ServiceRequestID);
                                        Cont = !JobCheck;
                                    }

                                    if (Cont)
                                    {
                                        //Create JobAsset
                                        #region JobAsset
                                        jobAsset = db.JobAssets.CreateObject();

                                        string orgName = db.Organisations.Where(o => o.OrganisationID == OrganisationID).Select(o => o.BusinessName).FirstOrDefault();

                                        jobAsset.OrganisationID = OrganisationID;
                                        jobAsset.OrganisationName = orgName;
                                        jobAsset.CustomerAssetNumber = CustomerNumber;
                                        jobAsset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                        jobAsset.AssetDefinition = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.AssetTypeID = 9;
                                        jobAsset.AssetType = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.CreatedBy = ContactID;
                                        jobAsset.CreatedDate = DateTime.Now;
                                        jobAsset.LastModifiedBy = ContactID;
                                        jobAsset.LastModifiedDate = DateTime.Now;
                                        jobAsset.IsDeleted = false;

                                        jobAsset.AssetID = asset.AssetID;

                                        jobAsset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();
                                        jobAsset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();
                                        jobAsset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();
                                        jobAsset.GpsGoogleMapRefX = dr[SPI_CSVIndex.Longitude.ToString()].ToString();
                                        jobAsset.GpsGoogleMapRefY = dr[SPI_CSVIndex.Latitude.ToString()].ToString();
                                        jobAsset.StatusID = (int)Global.AssetStatus.Active;
                                        jobAsset.StatusName = Global.AssetStatus.Active.ToString();

                                        db.JobAssets.AddObject(jobAsset);
                                        #endregion

                                        //Create/Update Asset & Job Asset Attributes
                                        #region Attributes
                                        //Set Region ID
                                        vRegionID = GetRegionByName(asset.CustomerGrouping, OrganisationID);
                                        vRegionID = (vRegionID == 0) ? CreateRegion(asset.CustomerGrouping, OrganisationID, ContactID) : vRegionID;
                                        SetAssetAttribute(asset, "RegionID", vRegionID.ToString(), ContactID);
                                        SetJobAssetAttribute(jobAsset, "RegionID", vRegionID.ToString(), ContactID);

                                        if (dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Equip_Type.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Fire_Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Number.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Module.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Sched_Year.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);
                                        }
                                        #endregion

                                        //Create Job
                                        #region Job
                                        job = db.Jobs.CreateObject();
                                        job.JobDefinitionID = (int)Global.JobDefinition.AssetInpection;
                                        job.Asset = asset;
                                        job.ServiceRequest = serviceRequest;
                                        job.JobAssetID = jobAsset.JobAssetID;
                                        job.OrganisationID = OrganisationID;
                                        job.StatusID = (int)Global.JobStatus.New;
                                        job.JobResolutionID = (int)Global.JobResolution.Unresolved;
                                        job.IsAddedByInspector = false;
                                        job.CreatedBy = ContactID;
                                        job.CreatedDate = DateTime.Now;
                                        job.LastModifiedBy = ContactID;
                                        job.LastModifiedDate = DateTime.Now;

                                        db.Jobs.AddObject(job);
                                        #endregion

                                        // create a new Scheduling task
                                        #region Task
                                        task = new Task();

                                        task.Job = job;
                                        task.TaskResolutionID = (int)Global.TaskResolution.NotResolved;
                                        task.SelectReadSequence = 0;
                                        task.StatusID = (int)Global.TaskStatus.New;
                                        task.TaskDefinitionID = (int)Global.TaskDefinition.SchedulingAIGround;
                                        task.TeamID = (int)Global.Team.NoTeamAssigned;
                                        task.CreatedBy = ContactID;
                                        task.CreatedDate = DateTime.Now;
                                        task.LastModifiedBy = ContactID;
                                        task.LastModifiedDate = DateTime.Now;

                                        db.Tasks.AddObject(task);
                                        #endregion

                                        JobAssetList.Add(new KeyValuePair<Job, JobAsset>(job, jobAsset));

                                        importCount++;
                                    }
                                    else
                                    {
                                        existCount++;
                                    }
                                }

                            }
                            #endregion

                            #region GPS Required ServiceRequest

                            if (GPSInspections != null && GPSInspections.Count > 0)
                            {
                                //create Service Request
                                #region ServiceRequest
                                //Sector Due Date
                                int[] TechAccesDates = GPSInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString())).ToArray();
                                int TechAccess = TechAccesDates.Min();
                                DateTime SectorDueDate = GetDueDate(TechAccess);

                                serviceRequest = db.ServiceRequests
                                                                       .Where(x => x.ServiceRequestTypeID == SRTypeID
                                                                                && x.CustomerGrouping == WorkSector
                                                                                && x.AgreedDueDate == SectorDueDate)
                                                                       .FirstOrDefault();

                                if (serviceRequest == null)
                                {
                                    //Sector Start Date
                                    int[] ImageCaptureDates = GPSInspections.Select(x => int.Parse(x[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString())).ToArray();
                                    int ImageCapture = ImageCaptureDates.Min();
                                    DateTime SectorStartDate = GetStartDate(ImageCapture);

                                    serviceRequest = db.ServiceRequests.CreateObject();

                                    serviceRequest.CreatedBy = ContactID;
                                    serviceRequest.CreatedDate = DateTime.Now;
                                    serviceRequest.CustomerGrouping = WorkSector;
                                    serviceRequest.IsDeleted = false;
                                    serviceRequest.IsNotReplicated = false;
                                    serviceRequest.Jobcount = AerialInspections.Count();
                                    serviceRequest.LastModifiedBy = ContactID;
                                    serviceRequest.LastModifiedDate = DateTime.Now;
                                    serviceRequest.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                    serviceRequest.OrganisationID = OrganisationID;
                                    serviceRequest.ServiceRequestTypeID = SRTypeID;
                                    serviceRequest.StatusID = (int)Global.ServiceRequestStatus.New;
                                    serviceRequest.Location = SRLocation;
                                    serviceRequest.AgreedStartDate = SectorStartDate;
                                    serviceRequest.AgreedDueDate = SectorDueDate;

                                    db.ServiceRequests.AddObject(serviceRequest);

                                }
                                #endregion

                                foreach (DataRow dr in GPSInspections)
                                {
                                    cnt++;
                                    errMsg = null;

                                    CustomerNumber = dr[SPI_CSVIndex.Pole_Number.ToString()].ToString();
                                    int AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                    int vRegionID = 0;

                                    #region Asset
                                    // check if asset exists
                                    asset = db.Assets
                                                                  .Where(a => a.OrganisationID == OrganisationID
                                                                           && a.CustomerAssetNumber == CustomerNumber
                                                                           && a.AssetDefinitionID == AssetDefinitionID)
                                                                  .FirstOrDefault();

                                    if (asset == null)
                                    {
                                        asset = db.Assets.CreateObject();

                                        asset.OrganisationID = OrganisationID;
                                        asset.CustomerAssetNumber = CustomerNumber;
                                        asset.AssetDefinitionID = AssetDefinitionID;
                                        asset.IsNotReplicated = true;
                                        asset.CreatedBy = ContactID;
                                        asset.CreatedDate = DateTime.Now;
                                        asset.IsDeleted = false;

                                        db.Assets.AddObject(asset);
                                    }

                                    if (!string.Equals(asset.CustomerGrouping, dr[SPI_CSVIndex.Feeder.ToString()].ToString()))
                                        asset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();

                                    if (!string.Equals(asset.Description, dr[SPI_CSVIndex.Description.ToString()].ToString()))
                                        asset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();

                                    if (!string.Equals(asset.Location, dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString()))
                                        asset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefX, dr[SPI_CSVIndex.Longitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefX = dr[SPI_CSVIndex.Longitude.ToString()].ToString();

                                    if (!string.Equals(asset.GpsGoogleMapRefY, dr[SPI_CSVIndex.Latitude.ToString()].ToString()))
                                        asset.GpsGoogleMapRefY = dr[SPI_CSVIndex.Latitude.ToString()].ToString();

                                    asset.StatusID = (int)Global.AssetStatus.Active;


                                    if (asset.EntityState == EntityState.Added || asset.EntityState == EntityState.Modified)
                                    {
                                        asset.LastModifiedBy = ContactID;
                                        asset.LastModifiedDate = DateTime.Now;
                                    }
                                    #endregion

                                    if (asset.EntityState == EntityState.Modified && serviceRequest.EntityState != EntityState.Added)
                                    {
                                        bool JobCheck = db.Jobs.Any(x => x.AssetID == asset.AssetID && x.ServiceRequestID == serviceRequest.ServiceRequestID);
                                        Cont = !JobCheck;
                                    }

                                    if (Cont)
                                    {
                                        //Create JobAsset
                                        #region JobAsset
                                        jobAsset = db.JobAssets.CreateObject();

                                        string orgName = db.Organisations.Where(o => o.OrganisationID == OrganisationID).Select(o => o.BusinessName).FirstOrDefault();

                                        jobAsset.OrganisationID = OrganisationID;
                                        jobAsset.OrganisationName = orgName;
                                        jobAsset.CustomerAssetNumber = CustomerNumber;
                                        jobAsset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.Pole;
                                        jobAsset.AssetDefinition = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.AssetTypeID = 9;
                                        jobAsset.AssetType = Global.AssetDefinitionTypes.Pole.ToString();
                                        jobAsset.CreatedBy = ContactID;
                                        jobAsset.CreatedDate = DateTime.Now;
                                        jobAsset.LastModifiedBy = ContactID;
                                        jobAsset.LastModifiedDate = DateTime.Now;
                                        jobAsset.IsDeleted = false;

                                        jobAsset.AssetID = asset.AssetID;

                                        jobAsset.CustomerGrouping = dr[SPI_CSVIndex.Feeder.ToString()].ToString();
                                        jobAsset.Description = dr[SPI_CSVIndex.Description.ToString()].ToString();
                                        jobAsset.Location = dr[SPI_CSVIndex.Suburb_Town.ToString()].ToString();
                                        jobAsset.GpsGoogleMapRefX = dr[SPI_CSVIndex.Longitude.ToString()].ToString();
                                        jobAsset.GpsGoogleMapRefY = dr[SPI_CSVIndex.Latitude.ToString()].ToString();
                                        jobAsset.StatusID = (int)Global.AssetStatus.Active;
                                        jobAsset.StatusName = Global.AssetStatus.Active.ToString();

                                        db.JobAssets.AddObject(jobAsset);
                                        #endregion

                                        //Create/Update Asset & Job Asset Attributes
                                        #region Attributes
                                        //Set Region ID
                                        vRegionID = GetRegionByName(asset.CustomerGrouping, OrganisationID);
                                        vRegionID = (vRegionID == 0) ? CreateRegion(asset.CustomerGrouping, OrganisationID, ContactID) : vRegionID;
                                        SetAssetAttribute(asset, "RegionID", vRegionID.ToString(), ContactID);
                                        SetJobAssetAttribute(jobAsset, "RegionID", vRegionID.ToString(), ContactID);

                                        if (dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.AMFMPoleID.ToString(),
                                                                     dr[SPI_CSVIndex.AMFM_Pole_ID.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Equip_Type.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.EquipType.ToString(),
                                                                     dr[SPI_CSVIndex.Equip_Type.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PELAttached.ToString(),
                                                                     dr[SPI_CSVIndex.PEL_Attached.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Fire_Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.FireArea.ToString(),
                                                                     dr[SPI_CSVIndex.Fire_Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SwitchZone.ToString(),
                                                                     dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Area.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.Area.ToString(),
                                                                     dr[SPI_CSVIndex.Area.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Number.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmNumber.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Number.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.PM_Module.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.PmModule.ToString(),
                                                                     dr[SPI_CSVIndex.PM_Module.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Sched_Year.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.SchedYear.ToString(),
                                                                     dr[SPI_CSVIndex.Sched_Year.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthFullCycle.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Full_Cycle.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MidCycleFY.ToString(),
                                                                     dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthImageCapture.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Image_Capture.ToString()].ToString(), ContactID);
                                        }

                                        if (dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString() != string.Empty)
                                        {
                                            SetAssetAttribute(asset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);

                                            SetJobAssetAttribute(jobAsset, Global.AssetInspectionAssetAttributeType.MonthTechAssess.ToString(),
                                                                     dr[SPI_CSVIndex.Month_Tech_Assess.ToString()].ToString(), ContactID);
                                        }
                                        #endregion

                                        //Create Job
                                        #region Job
                                        job = db.Jobs.CreateObject();
                                        job.JobDefinitionID = (int)Global.JobDefinition.AssetInpection;
                                        job.Asset = asset;
                                        job.ServiceRequest = serviceRequest;
                                        job.JobAssetID = jobAsset.JobAssetID;
                                        job.OrganisationID = OrganisationID;
                                        job.StatusID = (int)Global.JobStatus.New;
                                        job.JobResolutionID = (int)Global.JobResolution.Unresolved;
                                        job.IsAddedByInspector = false;
                                        job.CreatedBy = ContactID;
                                        job.CreatedDate = DateTime.Now;
                                        job.LastModifiedBy = ContactID;
                                        job.LastModifiedDate = DateTime.Now;

                                        db.Jobs.AddObject(job);
                                        #endregion

                                        // create a new Scheduling task
                                        #region Task
                                        task = new Task();

                                        task.Job = job;
                                        task.TaskResolutionID = (int)Global.TaskResolution.NotResolved;
                                        task.SelectReadSequence = 0;
                                        task.StatusID = (int)Global.TaskStatus.New;
                                        task.TaskDefinitionID = (int)Global.TaskDefinition.SchedulingAIGround;
                                        task.TeamID = (int)Global.Team.NoTeamAssigned;
                                        task.CreatedBy = ContactID;
                                        task.CreatedDate = DateTime.Now;
                                        task.LastModifiedBy = ContactID;
                                        task.LastModifiedDate = DateTime.Now;

                                        db.Tasks.AddObject(task);
                                        #endregion

                                        JobAssetList.Add(new KeyValuePair<Job, JobAsset>(job, jobAsset));

                                        importCount++;
                                    }
                                    else
                                    {
                                        existCount++;
                                    }
                                }

                            }

                            #endregion
                        }


                        importFileStatus = "Imported Records: " + importCount.ToString() + " of " + ds.Tables["Poles"].Rows.Count.ToString();

                        if (errCnt > 0)
                            importFileStatus = existCount.ToString() + " Poles already imported";


                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = existCount;
                        rowCount = cnt;
                        errCount = existCount;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;
                        db.SaveChanges();

                        foreach (KeyValuePair<Job, JobAsset> Link in JobAssetList)
                        {
                            Job uJob = Link.Key;
                            JobAsset uJobAsset = Link.Value;

                            uJob.JobAssetID = uJobAsset.JobAssetID;
                            uJobAsset.AssetID = uJob.AssetID;
                        }

                        Save("Save AI Data");

                        message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, ContactID);
                        messageQueue.SendMessage(message);

                    }
                    else
                    {
                        importFileStatus = "Validation errors. No record imported";

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save AI Validation Error");

                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, rowCount, errCount, ContactID);
                        messageQueue.SendMessage(message);
                    }
                }
                else
                {
                    importFileStatus = "No record imported. Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty or incorrect format", fileName));
                }
            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);

                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, ContactID);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Disposal");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateAISPAusnetFile(DataSet vDS, out int cnt, out int errCnt)
        {
            DataRow dr = null;
            bool cont = true;

            cnt = 0;
            errCnt = 0;
            string errMsg = null;

            System.Globalization.CultureInfo ac = new System.Globalization.CultureInfo("en-AU");

            bool ReturnValue = true;

            try
            {
                //Find duplicates and send exception
                DataTable FileTable = vDS.Tables["Poles"];

                //Validate empty fields
                for (int iRow = 0; iRow < FileTable.Rows.Count; iRow++)
                {
                    cnt++;
                    cont = true;
                    errMsg = null;
                    dr = FileTable.Rows[iRow];

                    //if (Fx.IsEmpty(dr[SPI_CSVIndex.Longitude.ToString()]))//Longitude
                    //{
                    //    cont = false;
                    //    errMsg = String.Format("Error in line {0} Longitude can not be blank", iRow + 1);
                    //}
                    //else
                    //{
                    //    decimal gpsRefX;
                    //    if (!decimal.TryParse((string)dr[SPI_CSVIndex.Longitude.ToString()], out gpsRefX))
                    //    {
                    //        cont = false;
                    //        errMsg = String.Format("Error in line {0} Longitude should be a number", iRow + 1);
                    //    }
                    //}


                    //if (cont)
                    //{
                    //    if (Fx.IsEmpty(dr[SPI_CSVIndex.Latitude.ToString()]))
                    //    {
                    //        cont = false;
                    //        errMsg = String.Format("Error in line {0} Latitude can not be blank", iRow + 1);
                    //    }
                    //    else
                    //    {
                    //        decimal gpsRefY;
                    //        if (!decimal.TryParse((string)dr[SPI_CSVIndex.Latitude.ToString()], out gpsRefY))
                    //        {
                    //            cont = false;
                    //            errMsg = String.Format("Error in line {0} Latitude should be a number", iRow + 1);
                    //        }
                    //    }
                    //}

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Pole_Number.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Pole_Number can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Feeder.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Feeder can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Work_Sector.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Work_Sector can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Equip_Type.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Equip_Type can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Description.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Description can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.PEL_Attached.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} PEL_Attached can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Suburb_Town.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Suburb_Town can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Fire_Area.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Fire_Area can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Q4_Switch_Zone.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Q4_Switch_Zone can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Area.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Area can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Sched_Year.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Sched_Year can not be blank", iRow + 1);
                        }
                    }

                    //if (cont)
                    //{
                    //    if (Fx.IsEmpty(dr[SPI_CSVIndex.Month_Full_Cycle.ToString()]))
                    //    {
                    //        cont = false;
                    //        errMsg = String.Format("Error in line {0} Month_Full_Cycle can not be blank", iRow + 1);
                    //    }
                    //}

                    //if (cont)
                    //{
                    //    if (Fx.IsEmpty(dr[SPI_CSVIndex.Mid_Cycle_FY.ToString()]))
                    //    {
                    //        cont = false;
                    //        errMsg = String.Format("Error in line {0} Mid_Cycle_FY can not be blank", iRow + 1);
                    //    }
                    //}

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Month_Image_Capture.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Month_Image_Capture can not be blank", iRow + 1);
                        }
                        else
                        {
                            int MonthimageCapture;
                            if (!int.TryParse((string)dr[SPI_CSVIndex.Month_Image_Capture.ToString()], out MonthimageCapture))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Month_Image_Capture should be a number", iRow + 1);
                            }
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Month_Tech_Assess.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Month_Tech_Assess can not be blank", iRow + 1);
                        }
                        else
                        {
                            int MonthTechAccess;
                            if (!int.TryParse((string)dr[SPI_CSVIndex.Month_Tech_Assess.ToString()], out MonthTechAccess))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Month_Tech_Assess should be a number", iRow + 1);
                            }
                        }
                    }

                    if (cont)
                    {
                        if (Fx.IsEmpty(dr[SPI_CSVIndex.Inspection_Type.ToString()]))
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Inspection_Type can not be blank", iRow + 1);
                        }
                    }

                    if (cont)
                    {
                        foreach (SPI_CSVIndex Col in Enum.GetValues(typeof(SPI_CSVIndex)))
                        {
                            if (dr[Col.ToString()] == DBNull.Value) dr[Col.ToString()] = String.Empty;
                        }
                    }


                    if (!cont)
                    {
                        errCnt++;

                        ImpException impException = new ImpException();
                        impException.Description = errMsg;
                        impException.ImpExceptionId = -iRow;
                        impException.ImportFileLog = importFileLog;
                        impException.RowNo = iRow + 1;

                        db.ImpExceptions.AddObject(impException);
                    }
                }

                if (errCnt > 0)
                {
                    ReturnValue = false;
                }

            }
            catch (Exception ex)
            {
                ReturnValue = false;

                errMsg = ex.Message;

                ImpException impException = new ImpException();
                impException.Description = errMsg;
                impException.ImpExceptionId = 0;
                impException.ImportFileLog = importFileLog;
                impException.RowNo = 0;

                db.ImpExceptions.AddObject(impException);
            }


            return ReturnValue;
        }
        private DateTime GetStartDate(int mt)
        {
            int yy = DateTime.Today.Year;

            if (DateTime.Today.Month > mt)
                yy++;

            return new DateTime(yy, mt, 1);
        }
        private DateTime GetDueDate(int mt)
        {
            int yy = DateTime.Today.Year;

            if (DateTime.Today.Month > mt)
                yy++;

            DateTime dt = new DateTime(yy, mt, 1);

            dt = dt.AddMonths(1).AddDays(-1); // get last day of the month 

            return dt;
        }
        private void ImportGenericAssets(Stream uploadFile, string fileName, int organisationID, int serviceRequestTypeID, int userId, string locationSR, Global.CoordinateType coordinateType)
        {
            List<GenericAsset> source = null;
            int errorCount = 0, totalCount = 0;

            if (!Check("Get AI process")) return;

            try
            {
                try
                {
                    char delimitor;
                    totalCount = 0;
                    if (".txt".Equals(Path.GetExtension(fileName), StringComparison.CurrentCultureIgnoreCase)) delimitor = '|';
                    else if (".csv".Equals(Path.GetExtension(fileName), StringComparison.CurrentCultureIgnoreCase)) delimitor = ',';
                    else throw new Exception("Incorrect file extension; supported types are txt or csv");
                    // CSV parsing into objects
                    uploadFile.Position = 0;
                    source = uploadFile.CsvParse<GenericAsset>(true, (lineNo, comment, ex) =>
                    {
                        db.AddToImpExceptions(CreateExceptionRecord(ex, "Parser exception", lineNo));
                        errorCount++;
                        return true;
                    }, delimitor);

                    totalCount = source.Count + errorCount;
                    if (errorCount > 0)
                    {
                        FinishFileImport(totalCount, errorCount, true);
                        return;
                    }

                    // infrastracture preparation
                    StringBuilder results = new StringBuilder();
                    Dictionary<string, AssetType> types;
                    Dictionary<string, AssetDefinition> definitions;

                    FillAssetDefinitionAndTypeLists(out types, out definitions);

                    // loading AttributeTypeID 
                    AttributeType customerAssetIDAttribute = null,
                        commentsAttribute = null,
                        lastTestDateAttribute = null;

                    try
                    {
                        customerAssetIDAttribute = GetAttributeType("CustomerAssetID");
                        commentsAttribute = GetAttributeType("Comments");
                        lastTestDateAttribute = GetAttributeType("LastTestDate");

                        if (customerAssetIDAttribute == null) throw new Exception("The 'CustomerAssetID' attribute type is not found");
                        if (commentsAttribute == null) throw new Exception("The 'Comments' attribute type is not found");
                        if (lastTestDateAttribute == null) throw new Exception("The 'LastTestDate' attribute type is not found");
                    }
                    catch (Exception ex)
                    {
                        db.AddToImpExceptions(CreateExceptionRecord(ex, "Preparing attribute exception", 0));
                        errorCount = totalCount;
                        return;
                    }

                    // creating assets
                    List<ImpException> errors = new List<ImpException>();
                    errorCount = 0;

                    Dictionary<string, ServiceRequest> serviceRequests = new Dictionary<string, ServiceRequest>(StringComparer.CurrentCultureIgnoreCase);
                    Action<string, string, int> maxLengthCheck = (fieldName, fieldValue, maxLength) =>
                    {
                        if ((fieldValue ?? string.Empty).Length > maxLength)
                            throw new InvalidDataException(string.Format("The '{0}' field is too long; actual size is {1} characters, max size is {2} characters", fieldName, fieldValue.Length, maxLength));
                    };

                    foreach (var item in source)
                    {
                        try
                        {
                            // --- Asset ------------------------------------------
                            Asset asset = new Asset();
                            asset.CustomerGrouping = item.LineNames;
                            asset.Location = item.TrackPosition;
                            asset.Description = item.TPWSupport;
                            asset.CustomerAssetNumber = item.AssetID;
                            switch (coordinateType)
                            {
                                case Global.CoordinateType.Google:
                                    asset.GpsGoogleMapRefX = item.Easting;
                                    asset.GpsGoogleMapRefY = item.Northing;
                                    break;
                                case Global.CoordinateType.UTM:
                                    double easting = double.Parse(item.Easting), northing = double.Parse(item.Northing), lat, lon;
                                    Fx.ConvertUTCToGeog(55, true, easting, northing, out lat, out lon);
                                    asset.GpsGoogleMapRefX = lat.ToString();
                                    asset.GpsGoogleMapRefY = lon.ToString();
                                    break;
                                default: throw new Exception("Unsupported coordinate type");
                            }
                            asset.IsNotReplicated = true;
                            asset.MobilityStatusID = 0;
                            asset.OrganisationID = organisationID;
                            asset.StatusID = (int)Global.AssetStatus.Active;
                            asset.CreatedBy = userId;
                            asset.CreatedDate = DateTime.Now;
                            asset.LastModifiedBy = userId;
                            asset.LastModifiedDate = DateTime.Now;
                            asset.IsDeleted = false;

                            maxLengthCheck("Asset.CustomerAssetNumber", asset.CustomerAssetNumber, 250);
                            maxLengthCheck("Asset.CustomerGrouping", asset.CustomerGrouping, 100);
                            maxLengthCheck("Asset.Description", asset.Description, 150);
                            maxLengthCheck("Asset.GpsGoogleMapRefX", asset.GpsGoogleMapRefX, 50);
                            maxLengthCheck("Asset.GpsGoogleMapRefY", asset.GpsGoogleMapRefY, 50);
                            maxLengthCheck("Asset.Location", asset.Location, 250);
                            maxLengthCheck("Asset.ManufacturerClass", asset.ManufacturerClass, 50);
                            maxLengthCheck("Asset.SerialNumber", asset.SerialNumber, 50);

                            var definition = CreateAssetDefinition(types, definitions, item.Type, item.Purpose, userId);
                            asset.AssetDefinition = definition;

                            if (item.ID != null) CreateAssetAttribute(asset, customerAssetIDAttribute, userId).Value = item.ID;
                            if (item.SurveyDate != null) CreateAssetAttribute(asset, lastTestDateAttribute, userId).Value = item.SurveyDate;
                            if (item.Comments != null) CreateAssetAttribute(asset, commentsAttribute, userId).Value = item.Comments;

                            db.Assets.AddObject(asset);

                            // --- ServiceRequest ------------------------------------------
                            ServiceRequest serviceRequest;
                            if (!serviceRequests.TryGetValue(asset.CustomerGrouping, out serviceRequest))
                            {
                                serviceRequest = new ServiceRequest();
                                serviceRequest.CreatedBy = userId;
                                serviceRequest.CreatedDate = DateTime.Now;
                                serviceRequest.CustomerGrouping = asset.CustomerGrouping;
                                serviceRequest.IsDeleted = false;
                                serviceRequest.IsNotReplicated = false;
                                serviceRequest.Jobcount = 0;
                                serviceRequest.LastModifiedBy = userId;
                                serviceRequest.LastModifiedDate = DateTime.Now;
                                serviceRequest.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                serviceRequest.OrganisationID = organisationID;
                                serviceRequest.ServiceRequestTypeID = (int)Global.ServiceRequestType.AssetInspection;
                                serviceRequest.StatusID = (int)Global.ServiceRequestStatus.New;
                                serviceRequest.Location = locationSR;

                                maxLengthCheck("ServiceRequest.Comments", serviceRequest.Comments, 1000);
                                maxLengthCheck("ServiceRequest.CustomerGrouping", serviceRequest.CustomerGrouping, 100);
                                maxLengthCheck("ServiceRequest.Location", serviceRequest.Location, 50);
                                maxLengthCheck("ServiceRequest.MonthBilled", serviceRequest.MonthBilled, 50);

                                db.ServiceRequests.AddObject(serviceRequest);
                                serviceRequests[asset.CustomerGrouping] = serviceRequest;
                            }

                            // --- Job ------------------------------------------
                            var job = new Job();
                            job.Asset = asset;
                            job.CreatedBy = userId;
                            job.CreatedDate = DateTime.Now;
                            job.IsAddedByInspector = false;
                            job.JobDefinitionID = (int)Global.JobDefinition.AssetInpection;
                            job.JobResolutionID = 0;
                            job.OrganisationID = organisationID;
                            job.ServiceRequest = serviceRequest;
                            job.StatusID = (int)Global.JobStatus.New;
                            job.LastModifiedBy = userId;
                            job.LastModifiedDate = DateTime.Now;
                            serviceRequest.Jobcount++;

                            maxLengthCheck("Job.CustomerComments", job.CustomerComments, 150);
                            maxLengthCheck("Job.OwnerSystem", job.OwnerSystem, 50);
                            maxLengthCheck("Job.PhotoRangeEnd", job.PhotoRangeEnd, 50);
                            maxLengthCheck("Job.PhotoRangeStart", job.PhotoRangeStart, 50);

                            db.Jobs.AddObject(job);

                            // --- Task ------------------------------------------
                            var task = new Task();
                            task.Job = job;
                            task.TaskResolutionID = 0;
                            task.SelectReadSequence = 0;
                            task.StatusID = (int)Global.TaskStatus.New;
                            task.TaskDefinitionID = (int)Global.TaskDefinition.ScheduleInspection;
                            task.TeamID = (int)Global.Team.AssetInspectionScheduling;
                            task.CreatedBy = userId;
                            task.CreatedDate = DateTime.Now;
                            task.LastModifiedBy = userId;
                            task.LastModifiedDate = DateTime.Now;

                            maxLengthCheck("Task.Comments", task.Comments, 512);
                            maxLengthCheck("Task.Description", task.Description, 100);

                            db.Tasks.AddObject(task);
                        }
                        catch (Exception ex)
                        {
                            errors.Add(CreateExceptionRecord(ex, "Creating DB entities exception", item.LineNo));
                            errorCount++;
                        }
                    }

                    // if any errors - rollback
                    if (errorCount > 0)
                    {
                        errors.ForEach((x) => db.Detach(x));
                        Close();
                        Check("Re-Create Entities");
                        errors.ForEach((x) => { x.ImportFileLog = importFileLog; db.ImpExceptions.AddObject(x); });
                    }
                }
                catch (Exception ex)
                {
                    errorCount = totalCount + 1;
                    db.ImpExceptions.AddObject(CreateExceptionRecord(ex, "General exception", -1));
                }
            }
            finally
            {
                FinishFileImport(totalCount, errorCount, true);
                try { Save("Generic Assets"); }
                catch (Exception ex)
                {
                    Close();
                    Check("Re-Create Entities");
                    errorCount = totalCount + 1;
                    db.ImpExceptions.AddObject(CreateExceptionRecord(ex, "General exception", -1));
                    FinishFileImport(totalCount, errorCount, true);
                    Save("Generic Assets exception");
                }
            }
        }

        #endregion

        #region Water Meter Read File Processing

        private void ImportBillingCalendar(Stream uploadFile, string fileName, int organisationID, int serviceRequestTypeID, int userId, BaseMessage messageQueue)
        {
            DataSet ds = null;
            DataRow dr = null;
            bool processed = false;
            int cnt = 0;
            bool cont = true;
            int errCnt = 0;
            string errMsg = null;
            //int i2 = 0;
            string s1 = null;
            CultureInfo ac = new System.Globalization.CultureInfo("en-AU");
            DateTimeStyles styles = DateTimeStyles.AssumeLocal;

            int jobCnt = 0;

            int statusID = -1;
            DateTime requestedStartDate = DateTime.Now;
            DateTime agreedDueDate = DateTime.Now;
            DateTime usageReviewDate = DateTime.Now;

            XMessage message = null;

            if (!Check("Get WaterMeter Read process")) return;

            try
            {

                statusID = (int)Global.ServiceRequestStatus.Pending;

                ds = Fx.ImportCSVFile(uploadFile, "BillingCalendar", ',', true, 0);

                if (ds != null)
                {
                    for (int i1 = 0; i1 < ds.Tables["BillingCalendar"].Rows.Count; i1++)
                    {
                        cont = true;
                        errMsg = null;
                        dr = ds.Tables["BillingCalendar"].Rows[i1];

                        if (Fx.IsEmpty(dr["Seq"]))
                        {
                            cont = false; // just ignore this line
                            errMsg = String.Format("Error in line {0} Seq can not be blank", i1 + 1);
                        }
                        else if (Regex.IsMatch((string)dr["Seq"], @"^[a-zA-Z]+$"))
                        {

                            cont = false;
                            errMsg = String.Format("Error in line {0} Seq should not contain Alpha Characters", i1 + 1);

                            //if (Int32.TryParse((string)dr["Seq"], out i2) == false)
                            //{
                            //    cont = false;
                            //    errMsg = String.Format("Error in line {0} Seq should be a number", i1 + 1);
                            //}
                            //else
                            //{
                            //    if (i2 < 1000 || i2 > 9999)
                            //    {
                            //        cont = false; // ignore this line without error
                            //        errMsg = String.Format("Error in line {0} Invalid Seq", i1 + 1);
                            //    }
                            //}
                        }


                        if (cont)
                        {
                            if (Fx.IsEmpty(dr["Meters"]))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Meters can not be blank", i1 + 1);
                            }
                            else if (Int32.TryParse((string)dr["Meters"], out jobCnt) == false)
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Meters should be a number", i1 + 1);
                            }
                        }

                        if (cont)
                        {
                            if (Fx.IsEmpty(dr["Download"]))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Download date can not be blank", i1 + 1);
                            }
                            else if (DateTime.TryParse((string)dr["Download"], ac, styles, out requestedStartDate) == false)
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Download date is in invalid format", i1 + 1);
                            }
                        }

                        if (cont)
                        {
                            if (Fx.IsEmpty(dr["Meter Read"]))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Meter Read Date can not be blank", i1 + 1);
                            }
                            else if (DateTime.TryParse((string)dr["Meter Read"], ac, styles, out agreedDueDate) == false)
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Meter Read Date is in invalid format", i1 + 1);
                            }
                        }

                        if (cont)
                        {
                            if (Fx.IsEmpty(dr["Usage Review"]))
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Usage Review can not be blank", i1 + 1);
                            }
                            else if (DateTime.TryParse((string)dr["Usage Review"], ac, styles, out usageReviewDate) == false)
                            {
                                cont = false;
                                errMsg = String.Format("Error in line {0} Usage Review is in invalid format", i1 + 1);
                            }
                        }

                        if (cont)
                        {
                            string route = (string)dr["Seq"];
                            string month = (string)dr["Month Billed"];

                            route = route.Trim();
                            month = month.Trim();

                            DateTime lastYear = DateTime.Now.AddYears(-1);

                            var q_sr = (from sreq in db.ServiceRequests
                                        where sreq.OrganisationID == organisationID && sreq.CustomerGrouping == route && sreq.MonthBilled == month
                                        && sreq.CreatedDate > lastYear //month must be within same year to be invalid
                                        select sreq).FirstOrDefault();

                            if (q_sr == null)
                            {
                                cnt++;
                                ServiceRequest sr = new ServiceRequest();
                                sr.AgreedDueDate = agreedDueDate;
                                sr.CreatedBy = userId;
                                sr.CreatedDate = DateTime.Now;
                                sr.CustomerGrouping = route;
                                sr.IsDeleted = false;
                                sr.IsNotReplicated = true;
                                sr.Jobcount = jobCnt;
                                sr.LastModifiedBy = userId;
                                sr.LastModifiedDate = DateTime.Now;
                                if (dr["Location"] != null)
                                    sr.Location = (string)dr["Location"];
                                sr.MonthBilled = month;
                                sr.OrganisationID = organisationID;
                                sr.RequestedStartDate = requestedStartDate;
                                sr.ServiceRequestID = -i1;
                                sr.ServiceRequestTypeID = serviceRequestTypeID;
                                sr.MobilityStatusID = (int)Global.MobilityStatus.DoNothing;
                                sr.StatusID = statusID;

                                db.ServiceRequests.AddObject(sr);
                            }
                            else
                            {
                                errMsg = String.Format("Error in line {0} Service request exists for this month", i1 + 1);
                            }
                        }


                        if (errMsg != null)
                        {
                            errCnt++;

                            ImpException impException = new ImpException();
                            impException.Description = errMsg;
                            impException.ImpExceptionId = -i1;
                            impException.ImportFileLog = importFileLog;
                            impException.RowNo = i1 + 1;

                            db.ImpExceptions.AddObject(impException);

                            message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, rowCount, errCount, userId);
                            messageQueue.SendMessage(message);
                        }
                    }       //for (int i1 = 0; i1 < ds.Tables["BillingCalendar"].Rows.Count; i1++)

                    importFileStatus = "Imported";

                    if (errCnt == 0 && cnt > 0)
                        importFileStatus = "Processed Successfully";

                    if (errCnt > 0 && cnt - errCnt > 0)
                        importFileStatus = "Processed with errors";

                    if (errCnt > 0 && cnt == errCnt)
                        importFileStatus = "Import Failed";

                    if (errCnt == 0 && cnt == 0)
                        importFileStatus = "No record imported";

                    // try to save 
                    importFileLog.RecordCnt = cnt;
                    importFileLog.ErrorCnt = errCnt;
                    rowCount = cnt;
                    errCount = errCnt;

                    importFileLog.Status = importFileStatus;
                    importFileLog.Complete = true;
                    Save("Billing Calendar");
                    
                    message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                    messageQueue.SendMessage(message);

                }   // if (ds != null)
                else
                {
                    importFileStatus = "No record imported.Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty", fileName));
                }
            }
            catch (Exception ex)
            {
                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Dispoase");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private void ImportWaterMeters(Stream uploadFile, string fileName, int organisationID, int serviceRequestTypeID, int userId, BaseMessage messageQueue)
        {
            DataSet ds = null;
            DataRow dr = null;
            int cnt = 0;
            int errCnt = 0;
            string errMsg = null;
            string routeID = "0000";
            string routeIDold = "0000";
            int sequence = 0;
            DateTime dt;
            string customerNumber = null;
            string location = null;
            int serviceRequestId = 0;
            int lastRead = 0;
            int iVar = 0;

            IList<int> SRIDList = new List<int>();

            DateTime? dtStartRead = null;

            Asset asset = null;
            Job job = null;
            Task task = null;
            ServiceRequest serviceRequest = null;

            CultureInfo ac = new CultureInfo("en-AU");
            DateTimeStyles styles = DateTimeStyles.AssumeLocal;

            XMessage message = null;

            if (!Check("Get WaterMeter Read process")) return;

            try
            {
                FillWaterMeterReadAssetAttributesTypes();
                FillWaterMeterReadJobAttributesTypes();
                FillWaterMeterReadTaskAttributesTypes();

                ds = Fx.ImportCSVFile(uploadFile, "WaterMeters", '|', false, 16);

                if (ds != null)
                {
                    if (ds.Tables["WaterMeters"].Columns.Count < 14)
                    {
                        throw new ApplicationException("This file has invalid structure");
                    }

                    if (ValidateWaterMeterFile(ds, organisationID, out cnt, out errCnt))
                    {
                        cnt = 0;
                        for (int i1 = 0; i1 < ds.Tables["WaterMeters"].Rows.Count; i1++)
                        {
                            cnt++;
                            errMsg = null;
                            dr = ds.Tables["WaterMeters"].Rows[i1];

                            sequence = 0;
                            location = null;

                            routeID = (string)dr[0];
                            Int32.TryParse((string)dr[1], out sequence);
                            customerNumber = (string)dr[4];
                            DateTime.TryParse((string)dr[10], ac, styles, out dt);
                            dtStartRead = dt;


                            #region Get Service Request

                            // get serviceRequestId 
                            if (routeID != routeIDold)
                            {
                                serviceRequestId = -1;
                                serviceRequest = null;

                                string custGr = "";
                                string padder = null;

                                if (routeID.Length < 4)
                                {
                                    for (int x1 = 4 - routeID.Length; x1 < 4; x1++)
                                    {
                                        padder = "0" + padder;
                                    }
                                }

                                custGr = padder + routeID;

                                var qsr = (from sr in db.ServiceRequests
                                           where sr.OrganisationID == organisationID && sr.StatusID == (int)Global.ServiceRequestStatus.Pending &&
                                                 sr.ServiceRequestTypeID == serviceRequestTypeID && sr.CustomerGrouping == custGr
                                           orderby sr.RequestedStartDate
                                           select sr).FirstOrDefault();

                                if (qsr != null)
                                {
                                    serviceRequest = qsr;
                                    serviceRequestId = serviceRequest.ServiceRequestID;

                                    //GlobalStaticInt on OrganisationExtension is used as the Min and Max Read Varience % from previous consumptiom

                                    var val = (from item in db.OrganisationExtensions
                                               where item.OrganisationID == organisationID
                                               select item.GlobalStaticInt).FirstOrDefault();

                                    Int32.TryParse(val.ToString(), out iVar);

                                    iVar = (iVar == 0) ? 10 : iVar;
                                }
                            }

                            #endregion Get Service Request

                            routeIDold = routeID;

                            customerNumber = customerNumber.Trim();

                            #region Get or Create Asset and attributes

                            // check if this meter exists in assets table
                            var q = (from a in db.Assets
                                     where a.OrganisationID == organisationID && a.CustomerAssetNumber == customerNumber
                                     select a).FirstOrDefault();

                            if (q != null)
                            {
                                asset = q;
                            }
                            else
                            {
                                asset = new Asset();

                                asset.OrganisationID = organisationID;
                                asset.CustomerAssetNumber = customerNumber;
                                asset.AssetDefinitionID = (int)Global.AssetDefinitionTypes.WaterMeter;
                                asset.StatusID = 36;	 // status of active
                                asset.CreatedBy = userId;
                                asset.CreatedDate = DateTime.Now;
                                asset.IsDeleted = false;

                                db.Assets.AddObject(asset);
                            }

                            if (asset.CustomerGrouping != routeID)
                                asset.CustomerGrouping = routeID;

                            if (dr[8] != null && dr[8] != DBNull.Value)
                                location = (string)dr[8];

                            if (string.Equals(asset.Location, location) == false)
                                asset.Location = location;

                            if (asset.EntityState == EntityState.Added || asset.EntityState == EntityState.Modified)
                            {
                                asset.LastModifiedBy = userId;
                                asset.LastModifiedDate = DateTime.Now;
                            }

                            // create or update Meter Make asset attribute
                            if (Fx.IsEmpty(dr[5]) == false)
                            {
                                AssetAttribute makeAssetAttr = null;
                                string meterMake = (string)dr[5];
                                meterMake = meterMake.Trim();

                                if (asset.EntityState != EntityState.Added)
                                    makeAssetAttr = GetAssetAttribute(asset.AssetID, "Meter Make");

                                if (makeAssetAttr == null)
                                    makeAssetAttr = CreateAssetAttribute(asset, "Meter Make", meterMake, userId);

                                if (string.Equals(makeAssetAttr.Value, meterMake) == false)
                                    makeAssetAttr.Value = meterMake;

                                if (makeAssetAttr.EntityState == EntityState.Modified)
                                {
                                    makeAssetAttr.LastModifiedBy = userId;
                                    makeAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            if (sequence > 0)
                            {
                                AssetAttribute sequenceAssetAttr = null;
                                // create or update Sequence asset attribute
                                if (asset.EntityState != EntityState.Added)
                                    sequenceAssetAttr = GetAssetAttribute(asset.AssetID, "Sequence");

                                if (sequenceAssetAttr == null)
                                    sequenceAssetAttr = CreateAssetAttribute(asset, "Sequence", sequence.ToString(), userId);

                                if (string.Equals(sequenceAssetAttr.Value, sequence.ToString()) == false)
                                    sequenceAssetAttr.Value = sequence.ToString();

                                if (sequenceAssetAttr.EntityState == EntityState.Modified)
                                {
                                    sequenceAssetAttr.LastModifiedBy = userId;
                                    sequenceAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            // create or update Consumer Name asset attribute

                            if (Fx.IsEmpty(dr[2]) == false)
                            {
                                AssetAttribute consumerAssetAttr = null;
                                string conName = (string)dr[2];
                                conName = conName.Trim();

                                if (asset.EntityState != EntityState.Added)
                                    consumerAssetAttr = GetAssetAttribute(asset.AssetID, "Consumer name");

                                if (consumerAssetAttr == null)
                                    consumerAssetAttr = CreateAssetAttribute(asset, "Consumer name", conName, userId);

                                if (string.Equals(consumerAssetAttr.Value, conName) == false)
                                    consumerAssetAttr.Value = conName;

                                if (consumerAssetAttr.EntityState == EntityState.Modified)
                                {
                                    consumerAssetAttr.LastModifiedBy = userId;
                                    consumerAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            // create or update Meter Address asset attribute
                            if (Fx.IsEmpty(dr[3]) == false)
                            {
                                AssetAttribute addressAssetAttr = null;
                                string address = (string)dr[3];
                                address = address.Trim();

                                if (asset.EntityState != EntityState.Added)
                                    addressAssetAttr = GetAssetAttribute(asset.AssetID, "Meter Address");

                                if (addressAssetAttr == null)
                                    addressAssetAttr = CreateAssetAttribute(asset, "Meter Address", address, userId);

                                if (string.Equals(addressAssetAttr.Value, address) == false)
                                    addressAssetAttr.Value = address;

                                if (addressAssetAttr.EntityState == EntityState.Modified)
                                {
                                    addressAssetAttr.LastModifiedBy = userId;
                                    addressAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            // create or update Meter Size asset attribute
                            if (Fx.IsEmpty(dr[6]) == false)
                            {
                                AssetAttribute sizeAssetAttr = null;
                                string meterSize = (string)dr[6];
                                meterSize = meterSize.Trim();

                                if (asset.EntityState != EntityState.Added)
                                    sizeAssetAttr = GetAssetAttribute(asset.AssetID, "Meter Size");

                                if (sizeAssetAttr == null)
                                    sizeAssetAttr = CreateAssetAttribute(asset, "Meter Size", meterSize, userId);

                                if (string.Equals(sizeAssetAttr.Value, meterSize) == false)
                                    sizeAssetAttr.Value = meterSize;

                                if (sizeAssetAttr.EntityState == EntityState.Modified)
                                {
                                    sizeAssetAttr.LastModifiedBy = userId;
                                    sizeAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            // create or update Meter Meter Dials asset attribute
                            if (Fx.IsEmpty(dr[7]) == false)
                            {
                                AssetAttribute dialsAssetAttr = null;
                                string meterDials = (string)dr[7];
                                meterDials = meterDials.Trim();

                                if (asset.EntityState != EntityState.Added)
                                    dialsAssetAttr = GetAssetAttribute(asset.AssetID, "Meter Dials");

                                if (dialsAssetAttr == null)
                                    dialsAssetAttr = CreateAssetAttribute(asset, "Meter Dials", meterDials, userId);

                                if (string.Equals(dialsAssetAttr.Value, meterDials) == false)
                                    dialsAssetAttr.Value = meterDials;

                                if (dialsAssetAttr.EntityState == EntityState.Modified)
                                {
                                    dialsAssetAttr.LastModifiedBy = userId;
                                    dialsAssetAttr.LastModifiedDate = DateTime.Now;
                                }
                            }

                            #endregion Get or Create Asset and attributes

                            #region Add Job, Task and Attributes

                            var jobQ = (from item in db.Jobs
                                        where item.ServiceRequestID == serviceRequestId && item.AssetID == asset.AssetID
                                        select item).FirstOrDefault();

                            if (jobQ == null)
                            {
                                // now create a new job
                                job = new Job();

                                job.Asset = asset;
                                job.CreatedBy = userId;
                                job.CreatedDate = DateTime.Now;
                                job.IsAddedByInspector = false;
                                job.JobDefinitionID = (int)Global.JobDefinition.WaterMeterRead;
                                job.JobResolutionID = 0;
                                job.OrganisationID = organisationID;
                                job.ServiceRequestID = serviceRequestId;
                                job.StatusID = (int)Global.JobStatus.New;
                                job.LastModifiedBy = userId;
                                job.LastModifiedDate = DateTime.Now;

                                db.Jobs.AddObject(job);

                                // create Special Instructions job attribute
                                if (Fx.IsEmpty(dr[9]) == false)
                                {
                                    JobAttribute siJobAttr = null;
                                    string specIns = (string)dr[9];
                                    specIns = specIns.Trim();

                                    siJobAttr = CreateJobAttribute(job, "Special Instructions", specIns, userId);
                                }

                                // create a new task
                                task = new Task();

                                task.Job = job;
                                task.TaskResolutionID = 0;
                                task.SelectReadSequence = 0;
                                task.StatusID = (int)Global.TaskStatus.New;
                                task.TaskDefinitionID = (int)Global.TaskDefinition.MeterRead;
                                task.TeamID = (int)Global.Team.MeterReaders;
                                task.CreatedBy = userId;
                                task.CreatedDate = DateTime.Now;
                                task.LastModifiedBy = userId;
                                task.LastModifiedDate = DateTime.Now;

                                db.Tasks.AddObject(task);

                                // add Date Start Read task attribute
                                if (dtStartRead.HasValue)
                                {
                                    TaskAttribute startReadTaskAttr = null;
                                    string str = dtStartRead.Value.ToString("yyyy-MM-dd HH:mm:ss.fff");

                                    startReadTaskAttr = CreateTaskAttribute(task, "Date Start Read", str, userId);
                                }

                                lastRead = 0;
                                // create Start Reading task attribeute
                                if (Fx.IsEmpty(dr[11]) == false)
                                {
                                    string str1 = (string)dr[11];
                                    str1 = str1.Trim();
                                    lastRead = Int32.Parse(str1);
                                }

                                TaskAttribute taskAttrStartReading = null;
                                taskAttrStartReading = CreateTaskAttribute(task, "Start Reading", lastRead.ToString(), userId);

                                // create Min Read Value task attribute
                                TaskAttribute taskAttrminRead = null;
                                string strmminread = null;

                                if (Fx.IsEmpty(dr[12]))
                                    //strmminread = lastRead.ToString();
                                    strmminread = null;
                                else
                                {
                                    strmminread = (string)dr[12];
                                    strmminread = strmminread.Trim();
                                }

                                taskAttrminRead = CreateTaskAttribute(task, "Min Read Value", strmminread, userId);

                                // create Max Read Value task attribute
                                TaskAttribute taskAttrmaxRead = null;
                                string strmaxread = null;

                                if (Fx.IsEmpty(dr[13]))
                                    //strmaxread = lastRead.ToString();
                                    strmaxread = null;
                                else
                                {
                                    strmaxread = (string)dr[13];
                                    strmaxread = strmaxread.Trim();
                                }

                                taskAttrmaxRead = CreateTaskAttribute(task, "Max Read Value", strmaxread, userId);

                                // create Previous Consumption task attribute
                                if (Fx.IsEmpty(dr[14]) == false)
                                {
                                    TaskAttribute taskAttr = null;
                                    string str1 = (string)dr[14];
                                    str1 = str1.Trim();

                                    taskAttr = CreateTaskAttribute(task, "Previous Consumption", str1, userId);

                                    int iPrev; Int32.TryParse(str1, out iPrev);
                                    int iConst = lastRead + iPrev;

                                    JobAttribute SelectMinRead = null;
                                    SelectMinRead = CreateJobAttribute(job, "Select Min Read", (iConst - (iPrev * iVar / 100)).ToString(), userId);

                                    JobAttribute SelectMaxRead = null;
                                    SelectMaxRead = CreateJobAttribute(job, "Select Max Read", (iConst + (iPrev * iVar / 100)).ToString(), userId);
                                }
                                else
                                {
                                    JobAttribute SelectMinRead = null;
                                    SelectMinRead = CreateJobAttribute(job, "Select Min Read", lastRead.ToString(), userId);

                                    JobAttribute SelectMaxRead = null;
                                    SelectMaxRead = CreateJobAttribute(job, "Select Max Read", lastRead.ToString(), userId);
                                }

                                // create Resequence task attribute
                                TaskAttribute taskAttr2 = null;

                                taskAttr2 = CreateTaskAttribute(task, "Resequence", "0", userId);

                                // set status of the current service request to new
                                serviceRequest.StatusID = (int)Global.ServiceRequestStatus.New;

                                if (!SRIDList.Contains(serviceRequest.ServiceRequestID))
                                {
                                    SRIDList.Add(serviceRequest.ServiceRequestID);
                                }

                            }
                            else
                            {
                                errMsg = String.Format("A Job already exists for this meter in service request. Line {0}", i1 + 1);

                                ImpException impException = new ImpException();
                                impException.Description = errMsg;
                                impException.ImpExceptionId = -i1;
                                impException.ImportFileLog = importFileLog;
                                impException.RowNo = i1 + 1;

                                db.ImpExceptions.AddObject(impException);
                            }
                            #endregion Add Job, Task and Attributes

                        }   // for (int i1 = 0; i1 < ds.Tables["WaterMeters"].Rows.Count; i1++)

                        importFileStatus = "Imported";

                        if (errCnt == 0 && cnt > 0)
                            importFileStatus = "Processed Successfully";

                        if (errCnt > 0 && cnt - errCnt > 0)
                            importFileStatus = "Processed with errors";

                        if (errCnt > 0 && cnt == errCnt)
                            importFileStatus = "Import Failed";

                        if (errCnt == 0 && cnt == 0)
                            importFileStatus = "No record imported";


                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;
                        Save("Water Meter Route");

                        foreach (int SRID in SRIDList)
                        {
                            ServiceRequest SR = new ServiceRequest();

                            SR = (from item in db.ServiceRequests
                                  where item.ServiceRequestID == SRID
                                  select item).FirstOrDefault();

                            SR.Jobcount = GetServiceRequestJobCount(SRID);
                        }

                        Save("Water Meter Route");

                        message = RenderProcessedEmail(importFileLog.FileName, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);

                    }
                    else
                    {

                        importFileStatus = "Validation errors. No record imported";

                        // try to save 
                        importFileLog.RecordCnt = cnt;
                        importFileLog.ErrorCnt = errCnt;
                        rowCount = cnt;
                        errCount = errCnt;

                        importFileLog.Status = importFileStatus;
                        importFileLog.Complete = true;

                        Save("Save Water Meter Read Validation Error");

                        message = RenderValidationEmail(importFileLog.FileName, importFileLog.ImportFileLogId, importFileStatus, rowCount, errCount, userId);
                        messageQueue.SendMessage(message);

                    }

                }   // if (ds != null)
                else
                {
                    importFileStatus = "No record imported. Empty file";
                    throw new ApplicationException(string.Format("Error uploading {0}. The file may be empty", fileName));
                }
            }
            catch (Exception ex)
            {
                bool re_throw = false;
                importFileStatus = "Error";

                if (importFileLogId > 0)
                {
                    SetImportError(ex);
                }
                else
                    re_throw = true;

                // re-creatr context object
                try
                {
                    message = RenderExceptionEmail(importFileLog.FileName, importFileLogId, ex, userId);
                    messageQueue.SendMessage(message);

                    Close();
                    Check("Disposal");
                }
                catch { }

                if (re_throw)
                    throw ex;
            }
        }
        private Boolean ValidateWaterMeterFile(DataSet vDS, int vOrganisationID, out int cnt, out int errCnt)
        {
            DataRow dr = null;
            bool cont = true;
            cnt = 0;
            errCnt = 0;
            string errMsg = null;

            int i2 = 0;
            string routeID = null;
            int sequence = 0;
            DateTime dt;

            string routeIDold = "0000";
            int serviceRequestId = 0;
            ServiceRequest serviceRequest = null;

            CultureInfo ac = new CultureInfo("en-AU");
            DateTimeStyles styles = DateTimeStyles.AssumeLocal;

            bool ReturnValue = true;

            for (int i1 = 0; i1 < vDS.Tables["WaterMeters"].Rows.Count; i1++)
            {
                cnt++;
                cont = true;
                errMsg = null;
                dr = vDS.Tables["WaterMeters"].Rows[i1];

                routeID = "0000";
                sequence = 0;


                #region Validate Column Values

                if (Fx.IsEmpty(dr[0]))
                {
                    cont = false;
                    errMsg = String.Format("Error in line {0} Route ID can not be blank", i1 + 1);
                }
                else if (Regex.IsMatch((string)dr[0], @"^[a-zA-Z]+$"))
                {

                    cont = false;
                    errMsg = String.Format("Error in line {0} Route Id should not contain Alpha Characters", i1 + 1);
                }
                else
                {
                    routeID = (string)dr[0];
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[1]))
                    {
                        cont = false;
                        errMsg = String.Format("Error in line {0} Sequence can not be blank", i1 + 1);
                    }
                    else if (Int32.TryParse((string)dr[1], out sequence) == false)
                    {
                        cont = false;
                        errMsg = String.Format("Error in line {0} Sequence should be a number", i1 + 1);
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[3]))
                    {
                        cont = false;
                        errMsg = String.Format("Error in line {0} Address can not be blank", i1 + 1);
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[4]))
                    {
                        cont = false;
                        errMsg = String.Format("Error in line {0} Meter number can not be blank", i1 + 1);
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[7]) == false)
                    {
                        if (Int32.TryParse((string)dr[7], out i2) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Dials should be a number", i1 + 1);
                        }
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[10]) == false)
                    {
                        if (DateTime.TryParse((string)dr[10], ac, styles, out dt) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Requested Start date is in invalid format", i1 + 1);
                        }
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[11]) == false)
                    {
                        if (Int32.TryParse((string)dr[11], out i2) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Start Reading should be a number", i1 + 1);
                        }
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[12]) == false)
                    {
                        if (Int32.TryParse((string)dr[12], out i2) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Min Read Value should be a number", i1 + 1);
                        }
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[13]) == false)
                    {
                        if (Int32.TryParse((string)dr[13], out i2) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Max Read Value should be a number", i1 + 1);
                        }
                    }
                }

                if (cont)
                {
                    if (Fx.IsEmpty(dr[14]) == false)
                    {
                        if (Int32.TryParse((string)dr[14], out i2) == false)
                        {
                            cont = false;
                            errMsg = String.Format("Error in line {0} Previous Consumption should be a number", i1 + 1);
                        }
                    }
                }

                #endregion Validate Column Values

                #region Validate Service Request
                if (cont)
                {
                    // get serviceRequestId 
                    if (routeID != routeIDold)
                    {
                        serviceRequestId = -1;
                        serviceRequest = null;

                        string custGr = "";
                        string padder = null;

                        if (routeID.Length < 4)
                        {
                            for (int x1 = 4 - routeID.Length; x1 < 4; x1++)
                            {
                                padder = "0" + padder;
                            }
                        }

                        custGr = padder + routeID;


                        var qsr = (from sr in db.ServiceRequests
                                   where sr.OrganisationID == vOrganisationID && sr.StatusID == (int)Global.ServiceRequestStatus.Pending &&
                                         sr.ServiceRequestTypeID == (int)Global.ServiceRequestType.MeterRead && sr.CustomerGrouping == custGr
                                   orderby sr.RequestedStartDate
                                   select sr).FirstOrDefault();

                        if (qsr != null)
                        {
                            serviceRequest = qsr;
                            serviceRequestId = serviceRequest.ServiceRequestID;
                        }
                    }
                }

                if (cont)
                {
                    if (serviceRequestId <= 0)
                    {
                        cont = false;
                        errMsg = String.Format("Can not find service request for this meter. Line {0}", i1 + 1);
                    }
                }

                #endregion Validate Service Request

                if (!cont)
                {
                    errCnt++;

                    ImpException impException = new ImpException();
                    impException.Description = errMsg;
                    impException.ImpExceptionId = -i1;
                    impException.ImportFileLog = importFileLog;
                    impException.RowNo = i1 + 1;

                    db.ImpExceptions.AddObject(impException);
                }
            }

            if (errCnt > 0)
            {
                ReturnValue = false;
            }

            return ReturnValue;
        }

        private void FillWaterMeterReadAssetAttributesTypes()
        {
            assetAttributeTypes = new Dictionary<string, int>();

            assetAttributeTypes.Add("Meter Make", (int)Global.WaterMeterAssetAttributeType.MeterMake);
            assetAttributeTypes.Add("Sequence", (int)Global.WaterMeterAssetAttributeType.Sequence);
            assetAttributeTypes.Add("Consumer name", (int)Global.WaterMeterAssetAttributeType.ConsumerName);
            assetAttributeTypes.Add("Meter Address", (int)Global.WaterMeterAssetAttributeType.MeterAddress);
            assetAttributeTypes.Add("Meter Size", (int)Global.WaterMeterAssetAttributeType.MeterSize);
            assetAttributeTypes.Add("Meter Dials", (int)Global.WaterMeterAssetAttributeType.MeterDials);
        }
        private void FillWaterMeterReadJobAttributesTypes()
        {
            jobAttributeTypes = new Dictionary<string, int>();

            jobAttributeTypes.Add("Special Instructions", (int)Global.WaterMeterJobAttributeType.SpecialInstructions);
            jobAttributeTypes.Add("Select Min Read", (int)Global.WaterMeterJobAttributeType.SelectMinRead);
            jobAttributeTypes.Add("Select Max Read", (int)Global.WaterMeterJobAttributeType.SelectMaxRead);
        }
        private void FillWaterMeterReadTaskAttributesTypes()
        {
            taskAttributeTypes = new Dictionary<string, int>();

            taskAttributeTypes.Add("Date Start Read", (int)Global.WaterMeterTaskAttributeType.DateStartRead);
            taskAttributeTypes.Add("Start Reading", (int)Global.WaterMeterTaskAttributeType.StartReading);
            taskAttributeTypes.Add("Min Read Value", (int)Global.WaterMeterTaskAttributeType.MinReadValue);
            taskAttributeTypes.Add("Max Read Value", (int)Global.WaterMeterTaskAttributeType.MaxReadValue);
            taskAttributeTypes.Add("Previous Consumption", (int)Global.WaterMeterTaskAttributeType.PreviousConsumption);
            taskAttributeTypes.Add("Resequence", (int)Global.WaterMeterTaskAttributeType.Resequence);
        }

        #endregion

        #region Private Functions

        //Message Queue
        private XMessage RenderValidationEmail(String FileName, int ImportID, String ImportStatus, int RowCount, int ErrorCount, int ContactID)
        {
            XMessage message = new XMessage();

            Contact contact = db.Contacts.Where(x => x.ContactID == ContactID).FirstOrDefault();
            message.ToEmail = contact.Email;
            message.ToName = contact.FullName;

            StringBuilder HtmlMessage = new StringBuilder();
            String MessageHeader = string.Format(@"<!DOCTYPE html>
                            <html>
                                <head>
                                    <style type=""text/css"">*{{ font-family: Arial; font-size: 10pt; }}</style>
                                </head>
                                <body>Validation Results for {0}:<br/><br/>", FileName);
            HtmlMessage.Append(MessageHeader);
            String MessageTable = string.Format(@"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                            <tr bgcolor=""#CCCCCC"">
                                <td colspan=""2""  style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>File {0} has not been imported</span><br /></td>
                            </tr>
                            <tr bgcolor=""#EEEEEE"">
                                <td style=""font-family: Verdana; font-size: small"">Status</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{1}</td>
                            </tr>
                            <tr bgcolor=""#CCCCCC"">
                                <td style=""font-family: Verdana; font-size: small"" width=""180px"">Number of rows</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"" width=""200px"">{2}</td>
                            </tr>
                            <tr bgcolor=""#EEEEEE"">
                                <td style=""font-family: Verdana; font-size: small;"">Number of exceptions</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{3}</td>
                            </tr>
                        </table><br /><br />", FileName, ImportStatus, RowCount.ToString(), ErrorCount.ToString());
            HtmlMessage.Append(MessageTable);

            StringBuilder ExceptionTable = new StringBuilder();
            ExceptionTable.Append(@"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                        <tr bgcolor=""#CCCCCC"">
                            <td colspan=""2""  style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>Exception List</span><br /></td>
                        </tr>
                        <tr bgcolor=""#CCCCCC"">
                            <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>RowNo</span></td>
                            <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>Description</span></td>
                        </tr>");

            Check("Get Import Exception List");

            List<ImpException> excList = db.ImpExceptions.Where(x => x.ImportFileLogId == ImportID).ToList();

            int num = 2;
            foreach (ImpException exc in excList)
            {
                if (num % 2 == 0)
                {
                    ExceptionTable.Append("<tr bgcolor=\"#EEEEEE\">");
                }
                else
                {
                    ExceptionTable.Append("<tr bgcolor=\"#CCCCCC\">");
                }

                ExceptionTable.Append(string.Format(@"
                            <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{0}</td>
                            <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{1}</td>
                        </tr>
                        ", exc.RowNo.ToString(), exc.Description));

                num++;
            }
            ExceptionTable.Append("</table><br /><br />");
            HtmlMessage.Append(ExceptionTable.ToString());

            String MessageFooter = "<br/><br/><b>This message was automatically generated please do not reply.</b><br/></body></html>";
            HtmlMessage.Append(MessageFooter);

            message.MessageBody = HtmlMessage.ToString();
            message.Subject = string.Format("Evolution File Validation: {0}", FileName);

            return message;
            
        }
        private XMessage RenderProcessedEmail(String FileName, String ImportStatus, int RowCount, int ErrorCount, int ContactID)
        {
            XMessage message = new XMessage();

            Contact contact = db.Contacts.Where(x => x.ContactID == ContactID).FirstOrDefault();
            message.ToEmail = contact.Email;
            message.ToName = contact.FullName;

            StringBuilder HtmlMessage = new StringBuilder();
            String MessageHeader = string.Format(@"<!DOCTYPE html>
                            <html>
                                <head>
                                    <style type=""text/css"">*{{ font-family: Arial; font-size: 10pt; }}</style>
                                </head>
                                <body>Processor Results for {0}:<br/><br/>", FileName);
            HtmlMessage.Append(MessageHeader);
            String MessageTable = string.Format(@"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                            <tr bgcolor=""#CCCCCC"">
                                <td colspan=""2""  style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>File {0} has been imported</span><br /></td>
                            </tr>
                            <tr bgcolor=""#EEEEEE"">
                                <td style=""font-family: Verdana; font-size: small"">Status</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{1}</td>
                            </tr>
                            <tr bgcolor=""#CCCCCC"">
                                <td style=""font-family: Verdana; font-size: small"" width=""180px"">Number of rows</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"" width=""200px"">{2}</td>
                            </tr>
                            <tr bgcolor=""#EEEEEE"">
                                <td style=""font-family: Verdana; font-size: small;"">Number of exceptions</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{3}</td>
                            </tr>
                        </table><br /><br />", FileName, ImportStatus, RowCount.ToString(), ErrorCount.ToString());
            HtmlMessage.Append(MessageTable);

            String MessageFooter = "<br/><br/><b>This message was automatically generated please do not reply.</b><br/></body></html>";
            HtmlMessage.Append(MessageFooter);

            message.MessageBody = HtmlMessage.ToString();
            message.Subject = string.Format("Evolution File Processor: {0}", FileName);

            return message;
        }
        private XMessage RenderExceptionEmail(String FileName, int ImportID, Exception ex, int ContactID)
        {
            XMessage message = new XMessage();

            Contact contact = db.Contacts.Where(x => x.ContactID == ContactID).FirstOrDefault();
            message.ToEmail = contact.Email;
            message.ToName = contact.FullName;

            StringBuilder HtmlMessage = new StringBuilder();
            String MessageHeader = string.Format(@"<!DOCTYPE html>
                            <html>
                                <head>
                                    <style type=""text/css"">*{{ font-family: Arial; font-size: 10pt; }}</style>
                                </head>
                                <body>Processor Results for {0}:<br/><br/>", FileName);
            HtmlMessage.Append(MessageHeader);
            string ErrorMessage = string.Format("Application Error: {0}", ex.Message);
            String MessageTable = string.Format(@"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                            <tr bgcolor=""#CCCCCC"">
                                <td colspan=""2""  style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;""><span>File {0} has not been imported</span><br /></td>
                            </tr>
                            <tr bgcolor=""#EEEEEE"">
                                <td style=""font-family: Verdana; font-size: small"">Status</td>
                                <td style=""font-family: Verdana; font-size: small; font-weight: bolder; color: #000099;"">{1}</td>
                            </tr>
                        </table><br /><br />", FileName, ErrorMessage);
            HtmlMessage.Append(MessageTable);

            String MessageWarning = string.Format("<b>Please contact your <a href=\"mailto:select.support@select-solutions.com.au\">System Administrator</a> with ImportFileLogID: {0} .</b>", ImportID);

            String MessageFooter = "<br/><br/><b>This message was automatically generated please do not reply.</b><br/></body></html>";
            HtmlMessage.Append(MessageFooter);

            message.MessageBody = HtmlMessage.ToString();
            message.Subject = string.Format("Evolution File Processor Error: {0}", FileName);

            return message;
        }

        //Directory IO
        private void MoveFileToArchive(string sourcePath, string targetPath, string fileName)
        {
            if (CheckDirectory(targetPath))
            {
                string sourceFile = Path.Combine(sourcePath, fileName);
                string ArchiveStamp = DateTime.Now.ToString("yyyyMMddTHHmmss");
                string NewFileName = fileName.Substring(0, fileName.IndexOf('.')) + "_" + ArchiveStamp + fileName.Substring(fileName.IndexOf('.'));
                string destinationFile = Path.Combine(targetPath, NewFileName);

                File.Move(sourceFile, destinationFile);
            }
        }
        private bool CheckDirectory(string path)
        {
            bool result = false;
            try
            {
                DirectoryInfo info = new DirectoryInfo(path);
                if (!info.Exists)
                {
                    info.Create();
                }

                result = info.Exists;
            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);
                result = false;
            }
            return result;
        }

        //Import Log
        private ImportFileLog GetImportFileLog(String FileName)
        {
            if (!Check("Get GetImportFileLog process")) return null;
            return db.ImportFileLogs.Where(imp => imp.FileName == FileName && imp.Status == "Upload").FirstOrDefault();
        }
        private void SetImportError(Exception ex)
        {
            rowCount = 0;
            errCount = 0;
            string errMsg = "";

            if (ex != null)
            {
                errMsg = ex.Message;
                if (ex.InnerException != null)
                    errMsg += " " + ex.InnerException.Message;
            }

            Check("Context");

            var q = (from ifl in db.ImportFileLogs
                     where ifl.ImportFileLogId == importFileLogId
                     select ifl).FirstOrDefault();

            if (q != null)
            {
                q.RecordCnt = 0;
                q.ErrorCnt = 0;
                q.Status = importFileStatus;
                q.ErrMessage = errMsg;
                q.Complete = true;

                Save("Import Error");
            }
        }
        private ImpException CreateExceptionRecord(Exception ex, string comment, int lineNo)
        {
            ImpException impException = new ImpException();
            if (ex.InnerException != null) ex = ex.InnerException;
            impException.Description = string.Format("{0}: {1}", comment, ex.Message);
            impException.ImportFileLog = importFileLog;
            impException.RowNo = lineNo;
            return impException;
        }
        private void FinishFileImport(int recordCount, int errorCount, bool noErrorMandatory)
        {
            importFileStatus = "Imported";

            if (errorCount == 0 && recordCount > 0)
                importFileStatus = "Processed Successfully";

            if (errorCount > 0 && !noErrorMandatory && recordCount - errorCount > 0)
                importFileStatus = "Processed with errors";

            if (errorCount > 0 && (recordCount <= errorCount || noErrorMandatory))
                importFileStatus = "Import Failed";

            if (errorCount == 0 && recordCount == 0)
                importFileStatus = "No record imported";

            // try to save 
            importFileLog.RecordCnt = recordCount;
            importFileLog.ErrorCnt = errorCount;
            importFileLog.Status = importFileStatus;
            importFileLog.Complete = true;
        }

        private void FillAssetDefinitionAndTypeLists(out Dictionary<string, AssetType> types, out Dictionary<string, AssetDefinition> definitions)
        {
            types = null;
            definitions = null;

            if (!Check("Get FillAssetDefinitionAndTypeLists process")) return;

            types = new Dictionary<string, AssetType>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var item in (from at in db.AssetTypes
                                  where at.IsActive
                                  select at))
            {
                string key = item.Name.Trim();
                if (types.ContainsKey(key)) continue;
                types[key] = item;
            }

            definitions = new Dictionary<string, AssetDefinition>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var item in (from at in db.AssetTypes
                                  join ad in db.AssetDefinitions on at.AssetTypeID equals ad.AssetTypeID
                                  where at.IsActive && ad.IsActive
                                  select ad))
            {
                string key = string.Format("{0}\x0{1}", item.Name.Trim(), item.AssetType.Name.Trim());
                if (definitions.ContainsKey(key)) continue;
                definitions[key] = item;
            }
        }
        private AssetDefinition CreateAssetDefinition(Dictionary<string, AssetType> types, Dictionary<string, AssetDefinition> definitions, string assetType, string assetDefinition, int userId)
        {
            assetType = assetType.Trim();
            string key = string.Format("{0}\x0{1}", assetDefinition.Trim(), assetType);
            AssetDefinition definition;
            if (definitions.TryGetValue(key, out definition)) return definition;
            AssetType type;
            if (!types.TryGetValue(assetType, out type))
            {
                type = new AssetType();
                type.Name = assetType;
                type.Description = assetType;
                type.CreatedBy = userId;
                type.CreatedDate = DateTime.Now;
                type.LastModifiedBy = userId;
                type.LastModifiedDate = DateTime.Now;
                type.IsActive = true;

                types[assetType] = type;
                db.AssetTypes.AddObject(type);
            }

            definition = new AssetDefinition();
            definition.AssetType = type;
            definition.Description = assetDefinition;
            definition.Name = assetDefinition;
            definition.CreatedBy = userId;
            definition.CreatedDate = DateTime.Now;
            definition.LastModifiedBy = userId;
            definition.LastModifiedDate = DateTime.Now;
            definition.IsActive = true;

            definitions[key] = definition;
            db.AssetDefinitions.AddObject(definition);

            return definition;
        }
        
        private AttributeType GetAttributeType(string description)
        {

            if (!Check("Get GetAttributeType process")) return null;
            return (from at in db.AttributeTypes
                    where description.Equals(at.Description, StringComparison.CurrentCultureIgnoreCase)
                    select at).FirstOrDefault();
        }

        //Asset Attributes
        private AssetAttribute CreateAssetAttribute(Asset asset, string attrDescriptiom, string value, int userId)
        {
            AssetAttribute attr = db.AssetAttributes.CreateObject();
            attr.Asset = asset;
            attr.AssetAttributeID = -1;
            attr.AttributeTypeID = assetAttributeTypes[attrDescriptiom];
            attr.CreatedBy = userId;
            attr.CreatedDate = DateTime.Now;
            attr.Value = value;
            attr.LastModifiedBy = userId;
            attr.LastModifiedDate = DateTime.Now;

            db.AssetAttributes.AddObject(attr);

            return attr;
        }
        private AssetAttribute CreateAssetAttribute(Asset asset, AttributeType attribute, int userId)
        {
            AssetAttribute attr = null;

            attr = new AssetAttribute();
            attr.Asset = asset;
            attr.AttributeType = attribute;
            attr.AttributeTypeID = attribute.AttributeTypeID;
            attr.CreatedBy = userId;
            attr.CreatedDate = DateTime.Now;
            attr.LastModifiedBy = userId;
            attr.LastModifiedDate = DateTime.Now;

            db.AssetAttributes.AddObject(attr);

            return attr;
        }
        private AssetAttribute GetAssetAttribute(int assetID, string attrDescriptiom)
        {
            AssetAttribute attr = null;
            int attributeTypeID = assetAttributeTypes[attrDescriptiom];

            if (!Check("Get GetAssetAttribute process")) return null;

            // attribute for existing asset
            if (assetID > 0)
            {

                attr = db.AssetAttributes
                            .Where(aa => aa.AssetID == assetID
                                    && aa.AttributeTypeID == attributeTypeID)
                            .FirstOrDefault();
            }

            return attr;
        }
        private void SetAssetAttribute(Asset asset, string attrName, string value, int userId)
        {
            AssetAttribute assetAttr = GetAssetAttribute(asset.AssetID, attrName);
            value = value.Trim();

            if (assetAttr == null)
            {
                assetAttr = CreateAssetAttribute(asset, attrName, value, userId);
            }

            //RegionID is not to be updated if already exists
            if (attrName.ToUpper() != Global.AssetInspectionAssetAttributeType.RegionID.ToString().ToUpper())
            {
                if (string.Equals(assetAttr.Value, value) == false)
                { assetAttr.Value = value; }

                if (assetAttr.EntityState == EntityState.Modified)
                {
                    assetAttr.LastModifiedBy = userId;
                    assetAttr.LastModifiedDate = DateTime.Now;
                }
            }

        }

        //JobAsset Attributes
        private JobAssetAttribute CreateJobAssetAttribute(JobAsset JobAsset, string attrDescriptiom, string value, int ContactID)
        {
            int AttributeTypeID = assetAttributeTypes[attrDescriptiom];
            string dtype = db.AttributeTypes.Where(aa => aa.AttributeTypeID == AttributeTypeID)
                                                                 .Join(db.AttributeDataTypes, at => at.AttributeDataTypeID, ad => ad.AttributeDataTypeID, (at, ad) => ad)
                                                                 .Select(dt => dt.Name).FirstOrDefault();

            JobAssetAttribute attr = db.JobAssetAttributes.CreateObject();
            attr.JobAsset = JobAsset;
            attr.JobAssetAttributeID = -1;
            attr.AttributeTypeID = AttributeTypeID;
            attr.AttributeValue = value;
            attr.AttributeDescription = value;
            attr.AttributeDataType = dtype;

            db.JobAssetAttributes.AddObject(attr);

            return attr;
        }
        private JobAssetAttribute GetJobAssetAttribute(int JobAssetID, string attrDescriptiom)
        {
            JobAssetAttribute attr = null;
            int attributeTypeID = assetAttributeTypes[attrDescriptiom];

            if (!Check("Get GetJobAssetAttribute process")) return null;

            // attribute for existing asset
            if (JobAssetID > 0)
            {

                attr = db.JobAssetAttributes
                                             .Where(ja => ja.JobAssetID == JobAssetID
                                                       && ja.AttributeTypeID == attributeTypeID)
                                             .FirstOrDefault();
            }

            return attr;
        }
        private void SetJobAssetAttribute(JobAsset JobAsset, string attrName, string value, int ContactID)
        {
            JobAssetAttribute JobAssetAttr = GetJobAssetAttribute(JobAsset.JobAssetID, attrName);
            value = value.Trim();

            if (JobAssetAttr == null)
            {
                JobAssetAttr = CreateJobAssetAttribute(JobAsset, attrName, value, ContactID);
            }

            if (string.Equals(JobAssetAttr.AttributeValue, value) == false)
            { JobAssetAttr.AttributeValue = value; }
        }

        //Job Attributes
        private JobAttribute CreateJobAttribute(Job job, string attrDescriptiom, string value, int userId)
        {
            JobAttribute attr = null;

            attr = new JobAttribute();
            attr.Job = job;
            attr.JobAttributeID = -1;
            attr.AttributeTypeID = jobAttributeTypes[attrDescriptiom];
            attr.CreatedBy = userId;
            attr.CreatedDate = DateTime.Now;
            attr.Value = value;
            attr.LastModifiedBy = userId;
            attr.LastModifiedDate = DateTime.Now;

            db.JobAttributes.AddObject(attr);

            return attr;
        }

        //Task Attributes
        private TaskAttribute CreateTaskAttribute(Task task, string attrDescription, string value, int userId)
        {
            TaskAttribute attr = null;

            attr = new TaskAttribute();
            attr.Task = task;
            attr.TaskAttributeID = -1;
            attr.AttributeTypeID = taskAttributeTypes[attrDescription];
            attr.CreatedBy = userId;
            attr.CreatedDate = DateTime.Now;
            attr.Value = value;
            attr.LastModifiedBy = userId;
            attr.LastModifiedDate = DateTime.Now;

            db.TaskAttributes.AddObject(attr);

            return attr;
        }

        //Regions
        private int GetRegionByName(string value, int OrganisationID)
        {
            return db.Regions
                        .Where(rg => rg.Name.ToUpper() == value.ToUpper()
                                    && rg.OrganisationID == OrganisationID)
                        .Select(rg => rg.RegionID)
                        .FirstOrDefault();
        }
        private int CreateRegion(string value, int OrganisationID, int userId)
        {
            Region rg = db.Regions.CreateObject();
            rg.Description = value;
            rg.Name = value;
            rg.OrganisationID = OrganisationID;
            rg.CreatedBy = userId;
            rg.CreatedDate = DateTime.Now;
            rg.LastModifiedBy = userId;
            rg.LastModifiedDate = DateTime.Now;

            try
            {
                db.Regions.AddObject(rg);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }

            return rg.RegionID;
        }

        private int GetServiceRequestJobCount(int ServiceRequestID)
        {
            int JobCount = 0;

            if (!Check("Get GetServiceRequestJobCount process")) return 0;

            JobCount = (from Job in db.Jobs
                        where Job.ServiceRequestID == ServiceRequestID
                        && Job.JobResolutionID != (int)Global.JobResolution.NewAsset
                        select Job).Count();

            return JobCount;
        }

        
        #endregion Private Functions
    }

    

    public class FileParameters
    {
        private string inputFolder = null;
        private string outputFolder = null;
        private string srcFileName = null;                                                                      

        public FileParameters(
                string inputFolder, 
                string outputFolder,
                string srcFileName
            )
        {
            this.inputFolder = inputFolder;
            this.outputFolder = outputFolder;
            this.srcFileName = srcFileName;
        }

        public string SrcFileName
        {
            get { return srcFileName; }
        }

        public string InputFolder
        {
            get { return inputFolder; }
        }

        public string OutputFolder
        {
            get { return outputFolder; }
        }

    }

    public class GenericAsset
    {
        [CsvColumn(LineNo = true)]
        public int LineNo { get; set; }
        [CsvColumn("Id", true)]
        public string ID { get; set; }
        [CsvColumn("Line/Route Names")]
        public string LineNames { get; set; }
        [CsvColumn(Mandatory = true)]
        public string Type { get; set; }
        [CsvColumn]
        public string Purpose { get; set; }
        [CsvColumn("Track Position")]
        public string TrackPosition { get; set; }
        [CsvColumn("TPW Support")]
        public string TPWSupport { get; set; }
        [CsvColumn("Asset ID", true)]
        public string AssetID { get; set; }
        [CsvColumn("Survey Date")]
        public string SurveyDate { get; set; }
        [CsvColumn]
        public string Easting { get; set; }
        [CsvColumn]
        public string Northing { get; set; }
        [CsvColumn]
        public string Comments { get; set; }
    }

}
