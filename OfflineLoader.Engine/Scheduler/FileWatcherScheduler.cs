﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using OfflineLoader.Core.System;
using OfflineLoader.Engine.Processor;

namespace OfflineLoader.Engine.Scheduler
{
    class FileWatcherScheduler : BaseScheduler
    {
        public FileWatcherScheduler(SystemScheduler scheduler) : base(scheduler) { }
        public Thread WatcherThread;
        protected override void Run()
        {
            using (var system = new XSystem())
            {
                system.ProcessName = "FileProcessorScheduler";
                try
                {
                    system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} started", system.ProcessName));

                    using (var fileWatcher = new FileWatcher(system))
                    {
                        WatcherThread = new Thread(new ThreadStart(fileWatcher.Execute));
                        WatcherThread.IsBackground = true;
                        WatcherThread.Name = "EvoFileWatcher";
                        WatcherThread.Start();
                    }

                }
                catch (Exception ex) { system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message); }
                finally
                {
                    Finished = true; //tell the ControllerService that current cycle has finished, so service can be stopped
                    //system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} stopped", system.ProcessName));
                }
            }
        }

        public void Stop()
        {
            if (WatcherThread != null)
            {
                if (WatcherThread.IsAlive)
                {
                    WatcherThread.Abort();
                }
            }
        }
    }
}
